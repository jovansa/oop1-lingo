package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.PasswordHasher;

class PasswordHasherTest {

	@Test
	void testHashPass() {
		String expected = "2351db315178b8ec6f86114cb3241340bf4237728e0f6511227cb0a9d0d124da739bc0c52625f18d078bf2dc81410bd5605344e63648183cc0f8f20482c89c9a";
		String actual = PasswordHasher.hashPass("password123", "123password");
		assertEquals(expected,actual);
	}

}
