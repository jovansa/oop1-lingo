package testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;

import org.junit.jupiter.api.Test;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.other.Jezik;

class MapGeneratorsTest {

	@Test
	void testGetUceniciPoJeziku() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		HashMap<Jezik, Integer> expected = new HashMap<Jezik, Integer>();
		expected.put(Jezik.ENGLESKI, 13);
		expected.put(Jezik.NEMACKI, 7);
		expected.put(Jezik.FRANCUSKI, 2);
		
		HashMap<Jezik, Integer> actual = dh.getUceniciPoJeziku();
	
		for (Jezik j: Jezik.values()) 
			assertEquals(expected.get(j), actual.get(j));
	}

}
