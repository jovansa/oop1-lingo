package testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import entities.managers.*;
import entities.managers.DataLoad;
import utilities.ReportTextGenerator;

class ReportTextGeneratorTest extends ReportTextGenerator {
	
	
	@Test
	void testGetPredavacTestReport() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("Izvestaj za test: t7004 Engleski Jezik - Zavrsni ispit");
		expected.add("Autor testa: Milica Maric");
		expected.add("Ukupan broj resenja: 3");
		expected.add("Maksimalan broj bodova: 40.0");
		expected.add("Prosecno ostvaren broj bodova: 30.0");
		expected.add("Prosecna ocena: 3.33");
		expected.add("Ucinak po polazniku: ");
		expected.add("Polaznik: Tatjana Ivanovic, broj bodova: 30.0, 3");
		expected.add("Polaznik: Teodosija Radic, broj bodova: 33.0, 4");
		expected.add("Polaznik: Ljubica Milic, broj bodova: 27.0, 3");
		ArrayList<String> actual = ReportTextGenerator.getPredavacTestReport(dh.getTestFromId("t7004"));
		assertEquals(expected, actual);
	}

}
