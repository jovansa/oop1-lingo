package testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import entities.managers.DataHandler;
import entities.managers.DataLoad;

class IDGeneratorsTest extends DataHandler {

	@Test
	void testGenerateTestId() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		String expected = "t0019";
		String actual = dh.generateTestId();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testGeneratePlatnaKarticaId() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		String expected = "p016";
		String actual = dh.generatePlatnaKarticaId();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testGenerateZahtevId() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		String expected = "z008";
		String actual = dh.generateZahtevId();
		
		assertEquals(expected, actual);
	}
}
