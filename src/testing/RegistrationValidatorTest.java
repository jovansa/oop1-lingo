package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.RegistrationValidator;

class RegistrationValidatorTest extends RegistrationValidator {

	@Test
	void testIsValidUsername() {
		boolean expected = true;
		boolean actual = RegistrationValidator.isValidUsername("ispravan_username");
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsValidUsernameTooShort() {
		boolean expected = false;
		boolean actual = RegistrationValidator.isValidUsername("isp");
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsValidUsernameUnsupportedChar() {
		boolean expected = false;
		boolean actual = RegistrationValidator.isValidUsername("#ispravan username");
		assertEquals(expected, actual);
	}

	@Test
	void testIsValidUsernameFail() {
		boolean expected = false;
		boolean actual = RegistrationValidator.isValidUsername("ispravan_username_ali_predugacak");
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsValidName() {
		boolean expected = true;
		boolean actual = RegistrationValidator.isValidName("Marko");
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsValidNameNumber() {
		boolean expected = false;
		boolean actual = RegistrationValidator.isValidUsername("Mark0");
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsValidNameEmpty() {
		boolean expected = false;
		boolean actual = RegistrationValidator.isValidUsername("");
		assertEquals(expected, actual);
	}

	@Test
	void testIsValidDateString() {
		boolean expected = true;
		boolean actual = RegistrationValidator.isValidDateString("25.06.2000.");
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsValidDateStringWrong() {
		boolean expected = false;
		boolean actual = RegistrationValidator.isValidDateString("25.06.2000");
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsValidDateTimeString() {
		boolean expected = true;
		boolean actual = RegistrationValidator.isValidDateTimeString("25.06.2000. 17:00");
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsValidDateTimeStringWrong() {
		boolean expected = false;
		boolean actual = RegistrationValidator.isValidDateTimeString("25.06.2000 17-00");
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsValidPassword() {
		boolean expected = true;
		boolean actual = RegistrationValidator.isValidPassword("password123");
		assertEquals(expected, actual);
		
	}

	@Test
	void testIsValidPasswordTooShort() {
		boolean expected = false;
		boolean actual = RegistrationValidator.isValidPassword("pass");
		assertEquals(expected, actual);
		
	}
	
	@Test
	void testIsValidTelefon() {
		boolean expected = true;
		boolean actual = RegistrationValidator.isValidTelefon("+381601234567");
		assertEquals(expected, actual);
		
	}
	
	@Test
	void testIsValidTelefonWrong() {
		boolean expected = false;
		boolean actual = RegistrationValidator.isValidTelefon("060 1234567");
		assertEquals(expected,actual);
	}
}
