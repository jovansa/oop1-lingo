package testing;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import entities.managers.DataHandler;
import entities.managers.DataLoad;

class DataLoadTest extends DataLoad {

	@Test
	void testLoadNalozi() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    DataLoad.loadNalozi(dh);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}
	
	@Test
	void testLoadKorisnici() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    DataLoad.loadKorisnici(dh);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}
	
	@Test
	void testLoadKursevi() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    DataLoad.loadKursevi(dh);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}
	
	@Test
	void testLoadTestovi() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    DataLoad.loadTestovi(dh);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}
	
	@Test
	void testLoadResenja() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    DataLoad.loadResenja(dh);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}
	
	@Test
	void testLoadPlatneListe() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    DataLoad.loadPlatneListe(dh);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}

	@Test
	void testLoadTipBonusa() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    DataLoad.loadTipBonusa(dh);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}
	
	@Test
	void testLoadIstorijatPlate() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    DataLoad.loadIstorijatPlate(dh);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}

	@Test
	void testLoadCenovnici() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    DataLoad.loadCenovnici(dh);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}

	@Test
	void testLoadZahtevi() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    DataLoad.loadZahtevi(dh);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}
	
	@Test
	void testLoadPlatneKartice() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    DataLoad.loadPlatneKartice(dh);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}
}
