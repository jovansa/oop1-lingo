package testing;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import entities.managers.DataHandler;
import entities.managers.DataLoad;

class FileWritingTest extends DataHandler {

	@Test
	void testWriteKurseviToFile() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    dh.writeKurseviToFile(DataLoad.kurseviPath);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}
	
	@Test
	void testWriteZahteviToFile() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    dh.writeZahteviToFile(DataLoad.zahteviPath);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}
	
	@Test
	void testWriteKarticeToFile() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    dh.writeKarticeToFile(DataLoad.karticePath);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}

	@Test
	void testWriteKorisniciToFile() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    dh.writeKorisniciToFile(DataLoad.korisniciPath);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}

	@Test
	void testWriteResenjaToFile() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    dh.writeResenjaToFile(DataLoad.resenjaPath);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}

	@Test
	void testWriteTestToFile() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		boolean pass = true;
		  try {
			    dh.writeTestToFile(DataLoad.testoviPath);
			  } catch (Exception e) {
			    pass = false;
			  }
		  assertTrue(pass);
	}
}


