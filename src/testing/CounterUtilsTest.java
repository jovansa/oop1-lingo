package testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.users.Ucenik;

class CounterUtilsTest extends DataHandler{

	@Test
	void testGetNumOfTests() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		int expected = 2;
		int actual = dh.getNumOfTests(LocalDateTime.of(2019, 1, 1, 0, 0),
				LocalDateTime.of(2022, 1, 1, 0, 0), "Dejan Osmanovic");
	
		assertEquals(expected, actual);
	}

	@Test
	void testGetNumObradjenih() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		int expected = 2;
		int actual = dh.getNumObradjenih("Slavisa Dragic");
		
		assertEquals(expected, actual);
		
	}

	@Test
	void testGetBrKursevaForUcenik() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		int expected = 3;
		int actual = dh.getBrKursevaForUcenik((Ucenik) dh.getKorisnikFromJmbg("00012"));
		
		assertEquals(expected, actual);
		
	}
	
	@Test
	void testGetBrojEditabilnih(){
	DataHandler dh = new DataHandler();
	DataLoad.loadAll(dh);
	dh.setCurrentlyLogged(dh.getKorisnikFromJmbg("00009"));
	
	int expected = 1;
	int actual = dh.getBrojEditabilnih();
	
	assertEquals(expected, actual);
		
	}

	@Test
	void testGetBrojNeocenjenih() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		dh.setCurrentlyLogged(dh.getKorisnikFromJmbg("00006"));
		
		int expected = 2;
		int actual = dh.getBrojNeocenjenih();
		
		assertEquals(expected, actual);
	}
}
