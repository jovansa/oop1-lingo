package testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.other.Kurs;
import entities.users.Predavac;

class ArrayGeneratorsTest extends DataHandler {

	@Test
	void testGetPredavaciNames() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		Object[] expected = {"Ana Krajinovic", "Milica Maric", "Milan Milovanovic", "Dejan Osmanovic", "Ivana Radmanovic", "Martina Nadj"};
		Object[] actual = dh.getPredavaciNames();
		
		for (int i = 0; i < expected.length; i++)
			assertEquals(expected[i],actual[i]);
		
	}
	
	@Test
	void testGetSekretariNames() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		Object[] expected = {"Slavisa Dragic", "Mirka Stajsic", "Djordje Jovanovic"};
		Object[] actual = dh.getSekretariNames();
		
		for (int i = 0; i < expected.length; i++)
			assertEquals(expected[i],actual[i]);
		
	}
	
	@Test
	void testGetKurseviForPredavac() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		ArrayList<Kurs> expected = new ArrayList<Kurs>();
		expected.add(dh.getKursFromId("0004"));
		expected.add(dh.getKursFromId("0005"));
		
		ArrayList<Kurs> actual = dh.getKurseviForPredavac((Predavac) dh.getKorisnikFromJmbg("00009"));
		
		for (int i = 0; i < expected.size(); i++)
			assertEquals(expected.get(i),actual.get(i));
	}
	
	@Test
	void testGetDostupniKurseviForPredavac() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		ArrayList<Kurs> expected = new ArrayList<Kurs>();
		expected.add(dh.getKursFromId("0003"));
		
		ArrayList<Kurs> actual = dh.getDostupniKurseviForPredavac((Predavac) dh.getKorisnikFromJmbg("00006"));
		
		for (int i = 0; i < expected.size(); i++)
			assertEquals(expected.get(i),actual.get(i));
	}

	@Test
	void testGetUceniciPoGodinama() {
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		Integer[] expected = {7, 3, 5};
		Integer[] actual = dh.getUceniciPoGodinama();
		
		for(int i = 0; i < expected.length; i++)
			assertEquals(expected[i], actual[i]);
	}
}
