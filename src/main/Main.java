package main;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import gui.login.LoginScreen;
import utilities.PasswordHasher;


@SuppressWarnings("unused")
public class Main {

	public static void main(String[] args) {
	
		DataHandler dh = new DataHandler();
		DataLoad.loadAll(dh);
		
		LoginScreen ls = new LoginScreen(dh);	

	}
}
