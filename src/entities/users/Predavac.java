package entities.users;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import entities.other.Jezik;
import entities.other.StrucnaSprema;

public class Predavac extends Korisnik {

	private StrucnaSprema strucnaSprema;
	private ArrayList<Jezik> jezici;
	private int staz;
	private double plata;
	
	
	public Predavac(String jmbg, String ime, String prezime, String telefon, String adresa, Pol pol,
			LocalDate datumRodjena) {
		super(jmbg, ime, prezime, telefon, adresa, pol, datumRodjena);
		
	}
	
	public Predavac(String jmbg, String ime, String prezime, String telefon, String adresa, Pol pol,
			LocalDate datumRodjena, KorisnickiNalog nalog) {
		super(jmbg, ime, prezime, telefon, adresa, pol, datumRodjena, nalog);
		
	}

	public Predavac(String jmbg, String ime, String prezime, String telefon, String adresa, Pol pol,
			LocalDate datumRodjenja, KorisnickiNalog nalog, StrucnaSprema sprema, ArrayList<Jezik> jezici, int staz, double plata) {
		super(jmbg, ime, prezime, telefon, adresa, pol, datumRodjenja, nalog);
		this.strucnaSprema = sprema;
		this.jezici = jezici;
		this.staz = staz;
		this.plata = plata;
	}

	public StrucnaSprema getStrucnaSprema() {
		return strucnaSprema;
	}

	public void setStrucnaSprema(StrucnaSprema strucnaSprema) {
		this.strucnaSprema = strucnaSprema;
	}
	
	public ArrayList<Jezik> getJezici(){
		return this.jezici;
	}
	
	public void setJezici(ArrayList<Jezik> jezici) {
		this.jezici = jezici;
	}
	
	public int getStaz() {
		return staz;
	}

	public void setStaz(int staz) {
		this.staz = staz;
	}

	public double getPlata() {
		return plata;
	}

	public void setPlata(double plata) {
		String str = String.format("%1.2f", plata);
		plata = Double.valueOf(str);
		this.plata = plata;
	}
	
	public String getJeziciString() {
		String retVal = "";
		for (Jezik j: this.getJezici())
			retVal += j.name() + ",";
		if (retVal.length() > 0)
			return retVal.substring(0, retVal.length() - 1);
		return retVal;
	}
	
	@Override
	public String toString() {
		return "p|" + this.getJmbg() + "|" + this.getIme() + "|" + this.getPrezime() + "|" + this.getTelefon() 
				+ "|" + this.getAdresa() + "|" + this.getPol().name() + "|" 
				+ this.getDatumRodjena().format(DateTimeFormatter.ofPattern("dd.MM.yyyy.")) + "|" + this.getNalog().getUser()
				+ "|" + this.getStrucnaSprema().name() + "|" + this.getJeziciString() + "|" + this.getStaz() + "|" + this.getPlata() + "\n";
	}
}
