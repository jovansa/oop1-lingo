package entities.users;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Administrator extends Korisnik{

	public Administrator(String jmbg, String ime, String prezime, String telefon, String adresa, Pol pol,
			LocalDate datumRodjena) {
		super(jmbg, ime, prezime, telefon, adresa, pol, datumRodjena);
	}
	
	public Administrator(String jmbg, String ime, String prezime, String telefon, String adresa, Pol pol,
			LocalDate datumRodjena, KorisnickiNalog nalog) {
		super(jmbg, ime, prezime, telefon, adresa, pol, datumRodjena, nalog);
	}

	@Override
	public String toString() {
		return "a|" + this.getJmbg() + "|" + this.getIme() + "|" + this.getPrezime() + "|" + this.getTelefon() 
				+ "|" + this.getAdresa() + "|" + this.getPol().name() + "|" 
				+ this.getDatumRodjena().format(DateTimeFormatter.ofPattern("dd.MM.yyyy.")) + "|" 
				+ this.getNalog().getUser() +"\n";
	}
}
