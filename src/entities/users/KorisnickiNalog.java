package entities.users;

public class KorisnickiNalog {

	private String user;
	private String pass;
	
	public KorisnickiNalog(String user, String pass) {
		this.user = user;
		this.pass = pass;
	}

	public String getUser() {
		return user;
	}
	
	/*
		Username mora biti velicine od 5 do 20 karaktera
		i mora se sastojati od alfanumerickih znakova i donje crte.
		Prvi karakter mora biti slovo.
	*/
	public void setUser(String user) throws IllegalArgumentException {
	
		if (!user.matches("^[a-zA-Z]+[\\w]{5,20}$"))
			throw new IllegalArgumentException("Prepoznat nepodrzan karakter.");
		else
			this.user = user;
	}
	
	public String getPass(){
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}

	@Override
	public String toString() {
		return this.getUser() + "|" + this.getPass();
	}
}
