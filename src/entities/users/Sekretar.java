package entities.users;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import entities.other.StrucnaSprema;

public class Sekretar extends Korisnik {

	private StrucnaSprema strucnaSprema;
	private int staz;
	private double plata;
	
	
	public Sekretar(String jmbg, String ime, String prezime, String telefon, String adresa, Pol pol,
			LocalDate datumRodjena) {
		super(jmbg, ime, prezime, telefon, adresa, pol, datumRodjena);
		
	}
	
	public Sekretar(String jmbg, String ime, String prezime, String telefon, String adresa, Pol pol,
			LocalDate datumRodjena, KorisnickiNalog nalog) {
		super(jmbg, ime, prezime, telefon, adresa, pol, datumRodjena, nalog);
		
	}

	public Sekretar(String jmbg, String ime, String prezime, String telefon, String adresa, Pol pol,
			LocalDate datumRodjenja, KorisnickiNalog nalog, StrucnaSprema sprema, int staz, double plata) {
		super(jmbg, ime, prezime, telefon, adresa, pol, datumRodjenja, nalog);
		this.strucnaSprema = sprema;
		this.staz = staz;
		this.plata = plata;
	}

	public StrucnaSprema getStrucnaSprema() {
		return strucnaSprema;
	}

	public void setStrucnaSprema(StrucnaSprema strucnaSprema) {
		this.strucnaSprema = strucnaSprema;
	}

	public int getStaz() {
		return staz;
	}

	public void setStaz(int staz) {
		this.staz = staz;
	}

	public double getPlata() {
		return plata;
	}

	public void setPlata(double plata) {
		String str = String.format("%1.2f", plata);
		plata = Double.valueOf(str);
		this.plata = plata;
	}

	@Override
	public String toString() {
		return "s|" + this.getJmbg() + "|" + this.getIme() + "|" + this.getPrezime() + "|" + this.getTelefon() 
				+ "|" + this.getAdresa() + "|" + this.getPol().name() + "|" 
				+ this.getDatumRodjena().format(DateTimeFormatter.ofPattern("dd.MM.yyyy.")) + "|" + this.getNalog().getUser()
				+ "|" + this.getStrucnaSprema().name() + "|" + this.getStaz() + "|" + this.getPlata() + "\n";
	}
}
