package entities.users;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import entities.other.PlatnaKartica;


public class Ucenik extends Korisnik {
	private PlatnaKartica platnaKartica;
	
	public Ucenik(String jmbg, String ime, String prezime, String telefon, String adresa, Pol pol,
			LocalDate datumRodjena) {
		super(jmbg, ime, prezime, telefon, adresa, pol, datumRodjena);
	}
	
	public Ucenik(String jmbg, String ime, String prezime, String telefon, String adresa, Pol pol, 
			LocalDate datumRodjena, KorisnickiNalog nalog, PlatnaKartica platnaKartica) {
		super(jmbg, ime, prezime, telefon, adresa, pol, datumRodjena, nalog);
		this.platnaKartica = platnaKartica;
	}
	
	public PlatnaKartica getPlatnaKartica() {
		return platnaKartica;
	}

	public void setPlatnaKartica(PlatnaKartica platnaKartica) {
		this.platnaKartica = platnaKartica;
	}

	@Override
	public String toString() {
		String user;
		if (this.getNalog() == null)
			user = "";
		else
			user = this.getNalog().getUser();
		
		return "u|" + this.getJmbg() + "|" + this.getIme() + "|" + this.getPrezime() + "|" + this.getTelefon() 
				+ "|" + this.getAdresa() + "|" + this.getPol().name() + "|" 
				+ this.getDatumRodjena().format(DateTimeFormatter.ofPattern("dd.MM.yyyy.")) + "|" 
				+ user + "|" + this.getPlatnaKartica().getId() + "\n";
	}
}
