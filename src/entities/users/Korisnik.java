package entities.users;

import java.time.LocalDate;

public abstract class Korisnik {
	
	private String jmbg;
	private String ime;
	private String prezime;
	private String telefon;
	private String adresa;
	private Pol pol;
	private LocalDate datumRodjena;
	private KorisnickiNalog nalog;
	
	public enum Pol {MUSKO, ZENSKO}

	public Korisnik(String jmbg, String ime, String prezime, String telefon, String adresa, Pol pol,
			LocalDate datumRodjena) {
		this.jmbg = jmbg;
		this.ime = ime;
		this.prezime = prezime;
		this.telefon = telefon;
		this.adresa = adresa;
		this.pol = pol;
		this.datumRodjena = datumRodjena;
		this.nalog = null;
	}
	public Korisnik(String jmbg, String ime, String prezime, String telefon, String adresa,
			Pol pol, LocalDate datumRodjena, KorisnickiNalog nalog) {
		this.jmbg = jmbg;
		this.ime = ime;
		this.prezime = prezime;
		this.telefon = telefon;
		this.adresa = adresa;
		this.pol = pol;
		this.datumRodjena = datumRodjena;
		this.nalog = nalog;
	}
	
	public String getJmbg() {
		return this.jmbg;
	}
	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}
	
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) throws IllegalArgumentException {
		if (!telefon.matches("[+0-9]+")) 
			throw new IllegalArgumentException("Telefon se sastoji samo od cifara i znaka \"+\"");
		else
			this.telefon = telefon;
	}

	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public Pol getPol() {
		return pol;
	}
	public void setPol(Pol pol) {
		this.pol = pol;
	}
	
	public LocalDate getDatumRodjena() {
		return datumRodjena;
	}
	public void setDatumRodjena(LocalDate datumRodjena) {
		this.datumRodjena = datumRodjena;
	};
	
	public KorisnickiNalog getNalog() {
		return nalog;
	}
	public void setNalog(KorisnickiNalog nalog) {
		this.nalog = nalog;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		
		else if ( o == null)
			return false;
		
		Korisnik k = (Korisnik) o;
		return k.getJmbg().equals(this.getJmbg());
	}
}
