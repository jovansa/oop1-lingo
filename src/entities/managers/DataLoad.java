package entities.managers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

import entities.other.*;
import entities.users.*;
import entities.users.Korisnik.Pol;

public class DataLoad {
	
	public static final String naloziPath = "data\\nalozi.txt";
	public static final String korisniciPath = "data\\korisnici.txt";
	public static final String kurseviPath = "data\\kursevi.txt";
	public static final String testoviPath = "data\\testovi.txt";
	public static final String resenjaPath = "data\\resenja.txt";
	public static final String platneListePath = "data\\platna_lista.txt";
	public static final String tipBonusaPath = "data\\tip_bonusa.txt";
	public static final String istorijatPlatePath = "data\\istorijat_plate.txt";
	public static final String cenovniciPath = "data\\cenovnici.txt";
	public static final String zahteviPath = "data\\zahtevi.txt";
	public static final String karticePath = "data\\platne_kartice.txt";
	
	public static void loadNalozi(DataHandler data) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(naloziPath));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parsed = line.split("\\|");
				data.addNalog(new KorisnickiNalog(parsed[0], parsed[1]));
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public static void loadKorisnici(DataHandler data) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(korisniciPath));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parsed = line.split("\\|");
				if (parsed[0].equals("a")){
					data.addKorisnik(new Administrator(parsed[1], parsed[2], parsed[3], parsed[4], parsed[5], Pol.valueOf(parsed[6]),
							LocalDate.parse(parsed[7], DateTimeFormatter.ofPattern("dd.MM.yyyy.")), 
							data.getNalogFromUser(parsed[8])));
				}
				else if (parsed[0].equals("u")) {
					data.addKorisnik(new Ucenik(parsed[1], parsed[2], parsed[3], parsed[4], parsed[5], Pol.valueOf(parsed[6]),
							LocalDate.parse(parsed[7], DateTimeFormatter.ofPattern("dd.MM.yyyy.")), 
							data.getNalogFromUser(parsed[8]), data.getKarticaFromId(parsed[9])));
				}
				else if (parsed[0].equals("s")) {
					KorisnickiNalog nalog;
					if (parsed[8].equals(""))
						nalog = null;
					else
						nalog = data.getNalogFromUser(parsed[8]);
					data.addKorisnik(new Sekretar(parsed[1], parsed[2], parsed[3], parsed[4], parsed[5], Pol.valueOf(parsed[6]),
							LocalDate.parse(parsed[7], DateTimeFormatter.ofPattern("dd.MM.yyyy.")), 
							nalog, StrucnaSprema.valueOf(parsed[9]), 
							Integer.parseInt(parsed[10]), Double.parseDouble(parsed[11])));
				}
				else if (parsed[0].equals("p")) {
					String[] parsedJezici = parsed[10].split(",");
					ArrayList<Jezik> jezici = new ArrayList<Jezik>();
					for (String s: parsedJezici)
						jezici.add(Jezik.valueOf(s));
					
					data.addKorisnik(new Predavac(parsed[1], parsed[2], parsed[3], parsed[4], parsed[5], Pol.valueOf(parsed[6]),
							LocalDate.parse(parsed[7], DateTimeFormatter.ofPattern("dd.MM.yyyy.")), 
							data.getNalogFromUser(parsed[8]), StrucnaSprema.valueOf(parsed[9]), jezici,
							Integer.parseInt(parsed[11]), Double.parseDouble(parsed[12])));
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadKursevi(DataHandler data) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(kurseviPath));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parsed = line.split("\\|");
				String[] parsedPredavaci = parsed[2].split(",");
				String[] parsedUcenici = parsed[3].split(",");
				String[] parsedTestovi = {};
				if (parsed.length > 8)
					parsedTestovi = parsed[8].split(",");
				ArrayList<Predavac> predavaci = new ArrayList<Predavac>();
				for (String jmbg: parsedPredavaci)
					predavaci.add((Predavac) data.getKorisnikFromJmbg(jmbg));
				
				ArrayList<Ucenik> ucenici = new ArrayList<Ucenik>();
				for (String jmbg: parsedUcenici)
					ucenici.add((Ucenik) data.getKorisnikFromJmbg(jmbg));
				
				ArrayList<Test> testovi = new ArrayList<Test>();
				if (parsedTestovi.length > 0) {
					for (String t: parsedTestovi)
						testovi.add(data.getTestFromId(t));
				}
				
				data.addKurs(new Kurs(parsed[0], parsed[1], predavaci, ucenici,
						LocalDate.parse(parsed[4], DateTimeFormatter.ofPattern("dd.MM.yyyy.")),
						LocalDate.parse(parsed[5], DateTimeFormatter.ofPattern("dd.MM.yyyy.")),
						Jezik.valueOf(parsed[6]), NivoKursa.valueOf(parsed[7]), testovi));
			}
			br.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public static void loadTestovi(DataHandler data) {

		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(testoviPath));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parsed = line.split("\\|");
				String[] parsedPitanja = parsed[5].split(";");
				String[] parsedResenjaId = {};
				
				if (parsed.length > 7)
					parsedResenjaId = parsed[7].split(",");
				
				ArrayList<String> pitanja = new ArrayList<String>();
				for(String p: parsedPitanja)
					pitanja.add(p);
				
				ArrayList<ResenjeTesta> resenja = new ArrayList<ResenjeTesta>();
				for(String r: parsedResenjaId) {
					ResenjeTesta resenje = data.getResenjeFromId(r);
					resenja.add(resenje);
				}
				data.addTest(new Test(parsed[0], parsed[1],
						LocalDateTime.parse(parsed[2], DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")),
						LocalDateTime.parse(parsed[3], DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")),
						(Predavac) data.getKorisnikFromJmbg(parsed[4]), pitanja, Double.parseDouble(parsed[6]), resenja));
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public static void loadResenja(DataHandler data) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(resenjaPath));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parsed = line.split("\\|");
				String[] parsedOdgovori = parsed[6].split(";");
				
				ArrayList<String> odgovori = new ArrayList<String>();
				for(String o: parsedOdgovori)
					odgovori.add(o);
				
				data.addResenje(new ResenjeTesta(parsed[0], (Ucenik) data.getKorisnikFromJmbg(parsed[1]), 
						LocalDateTime.parse(parsed[2], DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")),
						Double.parseDouble(parsed[3]), Boolean.parseBoolean(parsed[4]), Boolean.parseBoolean(parsed[5]),
						odgovori, Integer.parseInt(parsed[7])));
			}
			
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public static void loadPlatneListe(DataHandler data) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(platneListePath));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parsed = line.split("\\|");
				PlatnaLista pl = new PlatnaLista(parsed[0], Double.parseDouble(parsed[1]),
						Double.parseDouble(parsed[2]), Double.parseDouble(parsed[3]));
				if (parsed[0].equals("p"))
					data.setPlPredavaci(pl);
				else if(parsed[0].equals("s"))
					data.setPlSekretari(pl);
			}
			br.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void loadTipBonusa(DataHandler data) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(tipBonusaPath));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parsed = line.split("\\|");
				if (parsed[0].equals("s"))
					data.setTipBonusaSekretar(parsed[1]);
				else if(parsed[0].equals("p"))
					data.setTipBonusaPredavac(parsed[1]);
			}
			br.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadIstorijatPlate(DataHandler data) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(istorijatPlatePath));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parsed = line.split("\\|");
				String[] parsedMapa = parsed[2].split(";");
				HashMap<Korisnik, Double> mapa = new HashMap<Korisnik, Double>();
				
				for(String i: parsedMapa) {
					String[] par = i.split(",");
					mapa.put(data.getKorisnikFromJmbg(par[0]), Double.parseDouble(par[1]));
				}
				data.addIstorijatPlate(new IstorijatPlate(LocalDate.parse(parsed[0], DateTimeFormatter.ofPattern("dd.MM.yyyy.")),
										LocalDate.parse(parsed[1], DateTimeFormatter.ofPattern("dd.MM.yyyy.")), mapa));		
			}
			br.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadCenovnici(DataHandler data) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(cenovniciPath));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parsed = line.split("\\|");
				LocalDate vaziOd = LocalDate.parse(parsed[1], DateTimeFormatter.ofPattern("dd.MM.yyyy."));	
				LocalDate vaziDo = LocalDate.parse(parsed[2], DateTimeFormatter.ofPattern("dd.MM.yyyy."));	
			
				HashMap<Kurs, Double> cene = new HashMap<Kurs, Double>();
				HashMap<Kurs, Double> ceneTesta = new HashMap<Kurs,Double>();
				HashMap<Kurs, Double> popusti = new HashMap<Kurs,Double>();
				HashMap<Kurs, Double> povecanje = new HashMap<Kurs, Double>();
				
				String[] cenePairs = parsed[4].split(";");
				for (String s : cenePairs) {
					String[] pair = s.split(",");
					cene.put(data.getKursFromId(pair[0]), Double.parseDouble(pair[1]));
				}
			
				String[] testPairs = parsed[5].split(";");
				for (String s : testPairs) {
					String[] pair = s.split(",");
					ceneTesta.put(data.getKursFromId(pair[0]), Double.parseDouble(pair[1]));
				}
			
				String[] popustPairs = parsed[6].split(";");
				for (String s : popustPairs) {
					String[] pair = s.split(",");
					popusti.put(data.getKursFromId(pair[0]), Double.parseDouble(pair[1]));
				}
				
				String[] povecanjePairs = parsed[7].split(";");
				for (String s : povecanjePairs) {
					String[] pair = s.split(",");
					povecanje.put(data.getKursFromId(pair[0]), Double.parseDouble(pair[1]));
				}
				
				data.addCenovnik(new Cenovnik(parsed[0], vaziOd, vaziDo, Double.parseDouble(parsed[3]), cene, ceneTesta, popusti, povecanje));
			}
			br.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void loadZahtevi(DataHandler data) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(zahteviPath));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parsed = line.split("\\|");
				data.addZahtev(new Zahtev(parsed[0], (Ucenik)data.getKorisnikFromJmbg(parsed[1]),
							   data.getKursFromId(parsed[2]), StanjeZahteva.valueOf(parsed[3]),
							   LocalDateTime.parse(parsed[5], DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")),
							   (Sekretar)data.getKorisnikFromJmbg(parsed[4])));
			}
			br.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadPlatneKartice(DataHandler data) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(karticePath));
			String line;
			while ((line = br.readLine()) != null) {
				String[] parsed = line.split("\\|");
				HashMap<LocalDateTime, Double> mapa = new HashMap<LocalDateTime, Double>();
				
				String[] parsedMap = parsed[2].split(";");
				for (String s : parsedMap) {
					String[] pair = s.split(",");
					mapa.put(LocalDateTime.parse(pair[0], DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")), Double.parseDouble(pair[1]));
				}
				data.addKartica(new PlatnaKartica(parsed[0], Double.parseDouble(parsed[1]), mapa));
			}
			br.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void loadAll(DataHandler dh) {
		DataLoad.loadNalozi(dh);
		DataLoad.loadPlatneKartice(dh);
		DataLoad.loadKorisnici(dh);
		DataLoad.loadPlatneListe(dh);
		DataLoad.loadResenja(dh);
		DataLoad.loadTestovi(dh);
		DataLoad.loadKursevi(dh);
		DataLoad.loadTipBonusa(dh);
		DataLoad.loadIstorijatPlate(dh);
		DataLoad.loadCenovnici(dh);
		DataLoad.loadZahtevi(dh);
	}
}
