package entities.managers;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import entities.other.Cenovnik;
import entities.other.IstorijatPlate;
import entities.other.Jezik;
import entities.other.Kurs;
import entities.other.PlatnaKartica;
import entities.other.PlatnaLista;
import entities.other.ResenjeTesta;
import entities.other.Test;
import entities.other.Zahtev;
import entities.users.KorisnickiNalog;
import entities.users.Korisnik;
import entities.users.Predavac;
import entities.users.Sekretar;
import entities.users.Ucenik;
import utilities.PasswordHasher;

public class DataHandler {
	
	private Korisnik currentlyLogged;
	
	private PlatnaLista plSekretari;
	private PlatnaLista plPredavaci;
	
	private String tipBonusaPredavac;
	private String tipBonusaSekretar;
	
	private ArrayList<KorisnickiNalog> nalozi;
	private ArrayList<Korisnik> korisnici;
	private ArrayList<Kurs> kursevi;
	private ArrayList<Test> testovi;
	private ArrayList<ResenjeTesta> resenja;
	private ArrayList<IstorijatPlate> spisakPlata;
	private ArrayList<Cenovnik> cenovnici;
	private ArrayList<Zahtev> zahtevi;
	private ArrayList<PlatnaKartica> kartice;
	
	public DataHandler() {
		currentlyLogged = null;
		nalozi = null;
		korisnici = null;
		kursevi = null;
		testovi = null;
		resenja = null;
		spisakPlata = null;
		plSekretari = null;
		plPredavaci = null;
		tipBonusaPredavac = null;
		tipBonusaSekretar = null;
		cenovnici = null;
		zahtevi = null;
		kartice = null;
	}
	
	public ArrayList<PlatnaKartica> getKartice(){
		return this.kartice;
	}
	
	public void addKartica(PlatnaKartica kartica) {
		if(this.kartice == null)
			this.kartice = new ArrayList<PlatnaKartica>();
		this.kartice.add(kartica);
	}
	
	public ArrayList<Zahtev> getZahtevi(){
		this.zahtevi.sort(Comparator.comparing(Zahtev::getDatumPrijema));
		return this.zahtevi;
	}
	
	public void addZahtev(Zahtev z) {
		if (this.zahtevi == null)
			this.zahtevi = new ArrayList<Zahtev>();
		this.zahtevi.add(z);
	}
	
	public ArrayList<Cenovnik> getCenovnici(){
		this.cenovnici.sort(Comparator.comparing(Cenovnik::getVaziOd));
		return this.cenovnici;
	}
	
	public void addCenovnik(Cenovnik c) {
		if (this.cenovnici == null)
			this.cenovnici = new ArrayList<Cenovnik>();
		this.cenovnici.add(c);
	}
	
	public Cenovnik getCurrentCenovnik() {
		return this.getCenovnici().get(this.getCenovnici().size() - 1);
	}
	
	public ArrayList<IstorijatPlate> getSpisakPlata(){
		
		this.spisakPlata.sort(Comparator.comparing(IstorijatPlate::getStart));
		return this.spisakPlata;
	}
	
	public void addIstorijatPlate(IstorijatPlate ip) {
		if (this.spisakPlata == null)
			this.spisakPlata = new ArrayList<IstorijatPlate>();
		this.spisakPlata.add(ip);
	}
	
	public String getTipBonusaPredavac() {
		return tipBonusaPredavac;
	}

	public void setTipBonusaPredavac(String tipBonusaPredavac) {
		this.tipBonusaPredavac = tipBonusaPredavac;
	}

	public String getTipBonusaSekretar() {
		return tipBonusaSekretar;
	}

	public void setTipBonusaSekretar(String tipBonusaSekretar) {
		this.tipBonusaSekretar = tipBonusaSekretar;
	}

	public PlatnaLista getPlSekretari() {
		return plSekretari;
	}

	public void setPlSekretari(PlatnaLista plSekretari) {
		this.plSekretari = plSekretari;
	}


	public PlatnaLista getPlPredavaci() {
		return plPredavaci;
	}


	public void setPlPredavaci(PlatnaLista plPredavaci) {
		this.plPredavaci = plPredavaci;
	}
	
	public Korisnik getCurrentlyLogged() {
		return this.currentlyLogged;
	}
	
	public void setCurrentlyLogged(Korisnik k) {
		this.currentlyLogged = k;
	}
	
	public ArrayList<KorisnickiNalog> getNalozi(){
		return nalozi;
	}
	
	public void addNalog(KorisnickiNalog nalog) {
		if (nalozi == null)
			nalozi = new ArrayList<KorisnickiNalog>();
		nalozi.add(nalog);
	}
	
	public ArrayList<Korisnik> getKorisnici(){
		return korisnici;
	}
	
	public void addKorisnik(Korisnik k) {
		if (korisnici == null)
			korisnici = new ArrayList<Korisnik>();
		korisnici.add(k);
	}
	
	public ArrayList<Kurs> getKursevi(){
		return kursevi;
	}
	
	public void addKurs(Kurs k) {
		if (kursevi == null)
			kursevi = new ArrayList<Kurs>();
		kursevi.add(k);
	}
	
	public ArrayList<Test> getTestovi(){
		return testovi;
	}
	
	public void addTest(Test t) {
		if (testovi == null)
			testovi = new ArrayList<Test>();
		testovi.add(t);
	}
	
	public ArrayList<ResenjeTesta> getResenja(){
		return resenja;
	}
	
	public void addResenje(ResenjeTesta r) {
		if (resenja == null)
			resenja = new ArrayList<ResenjeTesta>();
		resenja.add(r);
	}
	
	public boolean nalogExists(String user) {
		for (KorisnickiNalog nalog: this.getNalozi()) {
			if (user.equalsIgnoreCase(nalog.getUser()))
				return true;
		}
		return false;
	}
	
	public boolean jmbgExists(String jmbg) {
		for (Korisnik k: this.getKorisnici()) {
			if (k.getJmbg().equalsIgnoreCase(jmbg))
				return true;
		}
		return false;
	}
	
	public KorisnickiNalog authenticateLogin(String user, String pass) {
		for (KorisnickiNalog nalog: this.getNalozi()) {
			if ((user.equalsIgnoreCase(nalog.getUser())) && PasswordHasher.hashPass(pass, nalog.getUser()).equals(nalog.getPass()))
				return nalog;
		}
		return null;
	}

	public KorisnickiNalog getNalogFromUser(String user) {
		for(KorisnickiNalog nalog: this.getNalozi()) {
			if (nalog.getUser().equals(user))
				return nalog;
		}
		return null;
	}
	
	public Kurs getKursFromId(String id) {
		for(Kurs k: this.getKursevi()) {
			if (k.getId().equals(id))
				return k;
		}
		return null;
	}
	
	public Korisnik getKorisnikFromUser(String user) {
		for(Korisnik k: this.getKorisnici()) {
			if (k.getNalog().getUser().equalsIgnoreCase(user))
				return k;
		}
		return null;
	}
	
	public Korisnik getKorisnikFromJmbg(String jmbg) {
		for(Korisnik k: this.getKorisnici()) {
			if (k.getJmbg().equals(jmbg))
				return k;
		}
		return null;
	}

	public Test getTestFromId(String id) {
		for(Test t: this.getTestovi()) {
			if (t.getId().equals(id))
				return t;
		}
		return null;
	}

	public ResenjeTesta getResenjeFromId(String id) {
		for(ResenjeTesta r: this.getResenja()) {
			if (r.getId().equals(id))
				return r;
		}
		return null;
	}

	public PlatnaKartica getKarticaFromId(String id) {
		for(PlatnaKartica k : this.getKartice()) {
			if(k.getId().equals(id))
				return k;
		}
		return null;
	}
	
	public Zahtev getZahtevFromId(String id) {
		for (Zahtev z : this.getZahtevi()) {
			if(z.getId().equals(id))
				return z;
		}
		return null;
	}
	
	public int getBrojTestova(Predavac p) {
		int retVal = 0;
		for (Test t: this.getTestovi()) {
			if (t.getAutor().getJmbg().equals(p.getJmbg()))
				retVal++;
		}
		return retVal;
	}
	
	public int getBrojKurseva(Predavac p) {
		int retVal = 0;
		for (Kurs k: this.getKursevi()) {
			if (k.getPredavaci().contains(p))
				retVal++;
		}
		return retVal;
	}
	
	public int getBrojUpisanih() {
		int retVal = 0;
		for (Korisnik k: this.getKorisnici()) {
			if (k instanceof Ucenik)
				retVal++;
		}
		return retVal;
	}
	
	public int getBrojNeocenjenih() {
		int retVal = 0;
		for (Test t : this.getTestovi()) {
			for (ResenjeTesta rt : t.getResenja()){
				if (t.getAutor().getJmbg().equals(this.getCurrentlyLogged().getJmbg())
						&& rt.isPregledan() == false)
					retVal++;
			}
		}
		return retVal;
	}
	
	public double getPrihodi(LocalDate start, LocalDate end) {
		double retVal = 0;
		for (PlatnaKartica kartica : this.getKartice()) {
			retVal += kartica.getStanje();
			for (Map.Entry<LocalDateTime, Double> entry : kartica.getPlacanja().entrySet()) {
				if (entry.getKey().toLocalDate().isAfter(start.minusDays(1)) 
						&& entry.getKey().toLocalDate().isBefore(end.plusDays(1)))
					retVal += entry.getValue();
			}
		}
		String str = String.format("%1.2f", retVal);
		retVal = Double.valueOf(str);
		return retVal;
	}
	
	public double getRashodi(LocalDate start, LocalDate end) {
		double retVal = 0;
		long days = ChronoUnit.DAYS.between(start, end);
		int numOfSalaries = (int) Math.ceil(days/31);
		for(Korisnik k : this.getKorisnici()) {
			if(k instanceof Sekretar) {
				Sekretar s = (Sekretar) k;
				retVal += s.getPlata() * numOfSalaries;
			}
			else if(k instanceof Predavac) {
				Predavac p = (Predavac) k;
				retVal += p.getPlata() * numOfSalaries;
			}
		}
		return retVal;
	}
	
	public HashMap<Jezik, Integer> getUceniciPoJeziku(){
		HashMap<Jezik, Integer> mapa = new HashMap<Jezik, Integer>();
		for (Jezik j : Jezik.values())
			mapa.put(j, 0);
		for (Kurs k : this.getKursevi()) {
			int noviBr = mapa.get(k.getJezik()) + k.getPolaznici().size();
			mapa.put(k.getJezik(), noviBr);
		}
		return mapa;
	}
	
	public HashMap<Korisnik, Double> getUpdatedIstorijatMap(){
		HashMap<Korisnik, Double> newMap = new HashMap<Korisnik, Double>();
		for(Korisnik k : this.getKorisnici()) {
			if (k instanceof Sekretar){
				Sekretar s = (Sekretar) k;
				newMap.put(s, s.getPlata());
			}
			else if (k instanceof Predavac){
				Predavac p = (Predavac) k;
				newMap.put(p, p.getPlata());
			}
		}
		return newMap;
	}
	
	public String generatePlatnaKarticaId() {
		return String.format("p%03d", this.getKartice().size() + 1);
	}
	
	public String generateZahtevId() {
		return String.format("z%03d", this.getZahtevi().size() + 1);
	}
	
	public String generateTestId() {
		return String.format("t%04d", this.getTestovi().size() + 1);
	}
	
	public ArrayList<Kurs> getKurseviForPredavac(Predavac p){
		ArrayList<Kurs> retVal = new ArrayList<Kurs>();
		for (Kurs k : this.getKursevi()) {
			if (k.getPredavaci().contains(p) && LocalDate.now().isBefore(k.getKraj()))
				retVal.add(k);
		}
		return retVal;
	}
	
	public int getBrojEditabilnih() {
		int retVal = 0;
		for (Test t: this.getTestovi()) {
			if(t.getAutor().getJmbg().equals(this.getCurrentlyLogged().getJmbg())) {
				if (t.getResenja().isEmpty() || t.getResenja() == null) {
					if(t.getAktivanDo().isAfter(LocalDateTime.now())) 
						retVal++;
				}
			}
		}
		return retVal;
	}
	
	
	public ArrayList<Kurs> getDostupniKurseviForPredavac(Predavac p){
		ArrayList<Kurs> retVal = new ArrayList<Kurs>();
		for(Kurs k : this.getKursevi()) {
			if ((!k.getPredavaci().contains(p)) && p.getJezici().contains(k.getJezik())) {
				if(LocalDate.now().isBefore(k.getKraj()))
					retVal.add(k);
				
			}
		}
		return retVal;
	}
	
	public Integer[] getUceniciPoGodinama() {
		Integer[] retVal = {0, 0, 0};
		for(Korisnik k : this.getKorisnici()) {
			if (k instanceof Ucenik) {
				if(ChronoUnit.YEARS.between(k.getDatumRodjena() , LocalDate.now()) < 20)
					retVal[0]++;
				else if (ChronoUnit.YEARS.between(k.getDatumRodjena(), LocalDate.now()) > 30)
					retVal[2]++;
				else
					retVal[1]++;
			}
		}
		return retVal;
	}
	
	public double[] getPrihodiForYear(){
		double[] retVal = new double[12];
		for (int i = 0; i < 12; i++) {
			LocalDate start = LocalDate.now().minusMonths(i+1);
			LocalDate end = LocalDate.now().minusMonths(i);
			retVal[i]=getPrihodi(start, end);
		}
		return retVal;
	}
	
	public Integer getBrKursevaForUcenik(Ucenik u) {
		Integer retVal = 0;
		for(Kurs k: this.getKursevi()) {
			if (k.getPolaznici().contains(u))
				retVal++;
		}
		return retVal;
	}
	
	public double[] getRashodiForYear(){
		double[] retVal = new double[12];
		for (int i = 0; i < 12; i++) {
			LocalDate start = LocalDate.now().withDayOfMonth(1).minusMonths(i+1);
			LocalDate end = LocalDate.now().minusMonths(i);
			retVal[i]=getRashodi(start, end);
		}
		return retVal;
	}
	
	public void writeEntityToFile(Object k, String path) {
		try {
			PrintWriter out = new PrintWriter(new FileWriter(path, true));
			out.print(k);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeTipBonusaToFile(String s1, String s2, String path) {
		try {
			PrintWriter out = new PrintWriter(new FileWriter(path));
			out.println("s|" + s1);
			out.println("p|" + s2);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeTestToFile(String path) {
		try {
			PrintWriter out = new PrintWriter(new FileWriter(path));
			for (Test t : this.getTestovi())
				out.print(t);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeResenjaToFile(String path) {
		try {
			PrintWriter out = new PrintWriter(new FileWriter(path));
			for (ResenjeTesta rt : this.getResenja())
				out.print(rt);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeIstorijatToFile(String path) {
		try {
			PrintWriter out = new PrintWriter(new FileWriter(path, false));
			for (IstorijatPlate ip : this.getSpisakPlata())
				out.print(ip);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeKorisniciToFile(String path) {
		try {
			PrintWriter out = new PrintWriter(new FileWriter(path, false));
			for (Korisnik o : this.getKorisnici())
				out.print(o);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeKarticeToFile(String path) {
		try {
			PrintWriter out = new PrintWriter(new FileWriter(path, false));
			for (PlatnaKartica pk : this.getKartice())
				out.print(pk);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int getNumObradjenih(String sekretarImePrez) {
		int retVal = 0;
		for(Zahtev z: this.getZahtevi()) {
			try {
				if ((z.getSekretar().getIme() + " " + z.getSekretar().getPrezime()).equals(sekretarImePrez))
					retVal++;
			}catch(Exception e) {
				continue;
			}
		}
		return retVal;
	}
	
	public Object[] getSekretariNames() {
		Object[] retVal;
		ArrayList<Sekretar> sekretari = new ArrayList<Sekretar>();
		
		for (Korisnik k : this.getKorisnici()) {
			if (k instanceof Sekretar)
				sekretari.add((Sekretar)k);
		}
		retVal = new Object[sekretari.size()];
		for (int i = 0; i < retVal.length; i++)
			retVal[i] = sekretari.get(i).getIme() + " " + sekretari.get(i).getPrezime();
		return retVal;
	}
	
	public Object[] getPredavaciNames() {
		Object[] retVal;
		ArrayList<Predavac> predavaci = new ArrayList<Predavac>();
		
		for (Korisnik k : this.getKorisnici()) {
			if (k instanceof Predavac)
				predavaci.add((Predavac)k);
		}
		retVal = new Object[predavaci.size()];
		for (int i = 0; i < retVal.length; i++)
			retVal[i] = predavaci.get(i).getIme() + " " + predavaci.get(i).getPrezime();
		return retVal;
	}
	
	public int getNumOfTests(LocalDateTime start, LocalDateTime end, String p) {
		int retVal = 0;
		for (Test t: this.getTestovi()) {
			if ((t.getAutor().getIme() + " " + t.getAutor().getPrezime()).equals(p)
					&& t.getAktivanOd().isAfter(start) && t.getAktivanOd().isBefore(end))
				retVal++;
		}
		return retVal;
	}
	
	public void writeZahteviToFile(String path) {
		try {
			PrintWriter out = new PrintWriter(new FileWriter(path, false));
			for (Zahtev z: this.getZahtevi())
				out.print(z);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeKurseviToFile(String path) {
		try {
			PrintWriter out = new PrintWriter(new FileWriter(path, false));
			for (Kurs o : this.getKursevi())
				out.print(o);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
