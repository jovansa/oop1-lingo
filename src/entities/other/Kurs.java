package entities.other;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import entities.users.*;

public class Kurs {
	String id;
	String naziv;
	ArrayList<Predavac> predavaci;
	ArrayList<Ucenik> polaznici;
	LocalDate pocetak;
	LocalDate kraj;
	Jezik jezik;
	NivoKursa nivo;
	ArrayList<Test> testovi;
	
	public Kurs(String id, String naziv, ArrayList<Predavac> predavaci, ArrayList<Ucenik> polaznici, LocalDate pocetak,
			LocalDate kraj, Jezik jezik, NivoKursa nivo, ArrayList<Test> testovi) {
		this.id = id;
		this.naziv = naziv;
		this.predavaci = predavaci;
		this.polaznici = polaznici;
		this.pocetak = pocetak;
		this.kraj = kraj;
		this.jezik = jezik;
		this.nivo = nivo;
		this.testovi = testovi;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public ArrayList<Predavac> getPredavaci() {
		return predavaci;
	}
	public void addPredavac(Predavac p) {
		if (p != null)
			this.predavaci.add(p);
	}
	public void addUcenik(Ucenik u) {
		if(u != null)
			this.polaznici.add(u);
	}
	public void setPredavaci(ArrayList<Predavac> predavaci) {
		this.predavaci = predavaci;
	}
	public ArrayList<Ucenik> getPolaznici() {
		return polaznici;
	}
	public void setPolaznici(ArrayList<Ucenik> polaznici) {
		this.polaznici = polaznici;
	}
	public LocalDate getPocetak() {
		return pocetak;
	}
	public void setPocetak(LocalDate pocetak) {
		this.pocetak = pocetak;
	}
	public LocalDate getKraj() {
		return kraj;
	}
	public void setKraj(LocalDate kraj) {
		this.kraj = kraj;
	}
	public Jezik getJezik() {
		return jezik;
	}
	public void setJezik(Jezik jezik) {
		this.jezik = jezik;
	}
	public NivoKursa getNivo() {
		return nivo;
	}
	public void setNivo(NivoKursa nivo) {
		this.nivo = nivo;
	}
	public ArrayList<Test> getTestovi() {
		return testovi;
	}
	public void setTestovi(ArrayList<Test> testovi) {
		this.testovi = testovi;
	}
	
	public String getPredavaciString() {
		String retVal = "";
		if (this.getPredavaci().size() > 0) {
			for (Predavac p: this.getPredavaci())
				retVal += p.getJmbg() + ",";
			return retVal.substring(0, retVal.length() - 1);
		}
		else return retVal;
	}
	
	public String getPolazniciString() {
		String retVal = "";
		if (this.getPolaznici().size() > 0) {
			for (Ucenik u: this.getPolaznici())
				retVal += u.getJmbg() + ",";
			return retVal.substring(0, retVal.length() - 1);
		}
		else return retVal;
	}
	
	public String getTestoviString() {
		String retVal = "";
		if (this.getTestovi().size() > 0) {
			for (Test t: this.getTestovi())
				retVal += t.getId() + ",";
			return retVal.substring(0, retVal.length() - 1);
		}
		else return retVal;
	}
	
	@Override
	public String toString() {
		return this.getId() + "|" + this.getNaziv() + "|" + this.getPredavaciString() + "|" +
			   this.getPolazniciString() + "|" + this.getPocetak().format(DateTimeFormatter.ofPattern("dd.MM.yyyy.")) + "|" +
			   this.getKraj().format(DateTimeFormatter.ofPattern("dd.MM.yyyy.")) + "|" + this.getJezik().toString() + "|" + 
			   this.getNivo().toString() + "|" + this.getTestoviString() + "\n";
	}
}
