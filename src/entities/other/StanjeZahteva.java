package entities.other;

public enum StanjeZahteva {
	KREIRAN,
	U_OBRADI,
	PRIHVACEN,
	ODBIJEN
}
