package entities.other;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import entities.users.Sekretar;
import entities.users.Ucenik;

public class Zahtev {
	
	private String id;
	private Ucenik ucenik;
	private Kurs kurs;
	private StanjeZahteva stanje;
	private LocalDateTime datumPrijema;
	private Sekretar sekretar;
	
	public Zahtev(String id, Ucenik ucenik, Kurs kurs, StanjeZahteva stanje, LocalDateTime datumPrijema, Sekretar sekretar) {
		this.ucenik = ucenik;
		this.stanje = stanje;
		this.kurs = kurs;
		this.datumPrijema = datumPrijema;
		this.id = id;
		this.sekretar = sekretar;
	}
	
	public Sekretar getSekretar() {
		return sekretar;
	}

	public void setSekretar(Sekretar obradio) {
		this.sekretar = obradio;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public Ucenik getUcenik() {
		return ucenik;
	}
	public void setUcenik(Ucenik ucenik) {
		this.ucenik = ucenik;
	}
	
	public Kurs getKurs() {
		return kurs;
	}
	public void setKurs(Kurs k) {
		this.kurs = k;
	}
	
	public StanjeZahteva getStanje() {
		return stanje;
	}
	public void setStanje(StanjeZahteva stanje) {
		this.stanje = stanje;
	}
	public LocalDateTime getDatumPrijema() {
		return datumPrijema;
	}
	public void setDatumPrijema(LocalDateTime datumPrijema) {
		this.datumPrijema = datumPrijema;
	}
	
	@Override
	public String toString() {
		String sekretar;
		if(this.getSekretar() == null)
			sekretar = "";
		else
			sekretar = this.getSekretar().getJmbg();
		return this.getId() + "|" + this.getUcenik().getJmbg() + "|" + this.getKurs().getId() + "|" +
				this.getStanje().name() + "|" + sekretar + "|" +
				this.getDatumPrijema().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm"))  + "\n";
	}
}
