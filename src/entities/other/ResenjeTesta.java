package entities.other;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import entities.users.Ucenik;

public class ResenjeTesta {
	
	String id;
	Ucenik ucenik;
	LocalDateTime vremeResavanja;
	double brojBodova;
	boolean pregledan;
	boolean polozen;
	ArrayList<String> odgovori;
	int ocena;
	
	public ResenjeTesta(String id, Ucenik ucenik, LocalDateTime vremeResavanja, double brojBodova, boolean pregledan,
			boolean polozen, ArrayList<String> odgovori, int ocena) {
		this.id = id;
		this.ucenik = ucenik;
		this.vremeResavanja = vremeResavanja;
		this.brojBodova = brojBodova;
		this.pregledan = pregledan;
		this.polozen = polozen;
		this.odgovori = odgovori;
		this.ocena = ocena;
	}

	public int getOcena() {
		return ocena;
	}
	
	public void setOcena(int ocena) {
		this.ocena = ocena;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Ucenik getUcenik() {
		return ucenik;
	}

	public void setUcenik(Ucenik ucenik) {
		this.ucenik = ucenik;
	}

	public LocalDateTime getVremeResavanja() {
		return vremeResavanja;
	}

	public void setVremeResavanja(LocalDateTime vremeResavanja) {
		this.vremeResavanja = vremeResavanja;
	}

	public double getBrojBodova() {
		return brojBodova;
	}

	public void setBrojBodova(float brojBodova) {
		this.brojBodova = brojBodova;
	}

	public boolean isPregledan() {
		return pregledan;
	}

	public void setPregledan(boolean pregledan) {
		this.pregledan = pregledan;
	}

	public boolean isPolozen() {
		return polozen;
	}

	public void setPolozen(boolean polozen) {
		this.polozen = polozen;
	}

	public ArrayList<String> getOdgovori() {
		return odgovori;
	}

	public void setOdgovori(ArrayList<String> odgovori) {
		this.odgovori = odgovori;
	}
	
	public String getOdgovoriString() {
		String retVal = "";
		if (this.getOdgovori().size() > 0) {
			for (String t: this.getOdgovori())
				retVal += t + ";";
			return retVal.substring(0, retVal.length() - 1);
		}
		else return retVal;
	}
	
	@Override 
	public String toString() {
		return this.getId() + "|" + this.getUcenik().getJmbg() + "|" + 
	           this.getVremeResavanja().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")) + "|" + 
			   this.getBrojBodova() + "|" +this.isPregledan() + "|" + this.isPolozen() + "|" + 
	           this.getOdgovoriString() + "|" + this.getOcena() +"\n";
	}
}
