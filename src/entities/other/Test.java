package entities.other;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import entities.users.*;

public class Test {
	
	String id;
	String naziv;
	LocalDateTime aktivanOd;
	LocalDateTime aktivanDo;
	Predavac autor;
	ArrayList<String> pitanja;
	double maxBrojbodova;
	ArrayList<ResenjeTesta> resenja;
	
	public Test(String id, String naziv, LocalDateTime aktivanOd, LocalDateTime aktivanDo, Predavac autor,
			ArrayList<String> pitanja, double maxBrojbodova, ArrayList<ResenjeTesta> resenja) {
		this.id = id;
		this.naziv = naziv;
		this.aktivanOd = aktivanOd;
		this.aktivanDo = aktivanDo;
		this.autor = autor;
		this.pitanja = pitanja;
		this.maxBrojbodova = maxBrojbodova;
		this.resenja = resenja;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public LocalDateTime getAktivanOd() {
		return aktivanOd;
	}

	public void setAktivanOd(LocalDateTime aktivanOd) {
		this.aktivanOd = aktivanOd;
	}

	public LocalDateTime getAktivanDo() {
		return aktivanDo;
	}

	public void setAktivanDo(LocalDateTime aktivanDo) {
		this.aktivanDo = aktivanDo;
	}

	public Predavac getAutor() {
		return autor;
	}

	public void setAutor(Predavac autor) {
		this.autor = autor;
	}

	public ArrayList<String> getPitanja() {
		return pitanja;
	}

	public void setPitanja(ArrayList<String> pitanja) {
		this.pitanja = pitanja;
	}

	public double getMaxBrojbodova() {
		return maxBrojbodova;
	}

	public void setMaxBrojbodova(double maxBrojbodova) {
		this.maxBrojbodova = maxBrojbodova;
	}

	public ArrayList<ResenjeTesta> getResenja() {
		return resenja;
	}

	public void setResenja(ArrayList<ResenjeTesta> resenja) {
		this.resenja = resenja;
	}

	public String getResenjaString() {
		String retVal = "";
		if (this.getResenja().size() > 0) {
			for (ResenjeTesta t: this.getResenja())
				retVal += t.getId() + ",";
			return retVal.substring(0, retVal.length() - 1);
		}
		else return retVal;
	}
	
	public String getPitanjaString() {
		String retVal = "";
		if (this.getPitanja().size() > 0) {
			for (String t: this.getPitanja())
				retVal += t + ";";
			return retVal.substring(0, retVal.length() - 1);
		}
		else return retVal;
	}
	
	@Override
	public String toString() {
		return this.getId() + "|" + this.getNaziv() + "|" + 
			   this.getAktivanOd().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")) + "|" +
			   this.getAktivanDo().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")) + "|" +
			   this.getAutor().getJmbg() + "|" + this.getPitanjaString() + "|" + this.getMaxBrojbodova() + "|" + 
			   this.getResenjaString() + "\n";
	}
}
