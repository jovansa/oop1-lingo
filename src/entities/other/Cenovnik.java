package entities.other;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class Cenovnik {
	
	String id;
	LocalDate vaziOd;
	LocalDate vaziDo;
	double cenaUpisa;
	HashMap<Kurs, Double> cene;
	HashMap<Kurs, Double> ceneIzlaska;
	HashMap<Kurs, Double> popusti;
	HashMap<Kurs, Double> iznosPovecanja;
	
	public Cenovnik(String id, LocalDate vaziOd, LocalDate vaziDo, double cenaUpisa, HashMap<Kurs, Double> cene,
			HashMap<Kurs, Double> ceneIzlaska, HashMap<Kurs, Double> popusti, HashMap<Kurs, Double> iznosPovecanja) {
		this.id = id;
		this.vaziOd = vaziOd;
		this.vaziDo = vaziDo;
		this.cenaUpisa = cenaUpisa;
		this.cene = cene;
		this.ceneIzlaska = ceneIzlaska;
		this.popusti = popusti;
		this.iznosPovecanja = iznosPovecanja;
	}
	
	public double getCenaUpisa() {
		return cenaUpisa;
	}

	public void setCenaUpisa(double cenaUpisa) {
		this.cenaUpisa = cenaUpisa;
	}

	public HashMap<Kurs, Double> getIznosPovecanja() {
		return iznosPovecanja;
	}

	public void setIznosPovecanja(HashMap<Kurs, Double> iznosPovecanja) {
		this.iznosPovecanja = iznosPovecanja;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public LocalDate getVaziOd() {
		return vaziOd;
	}
	public void setVaziOd(LocalDate vaziOd) {
		this.vaziOd = vaziOd;
	}
	public LocalDate getVaziDo() {
		return vaziDo;
	}
	public void setVaziDo(LocalDate vaziDo) {
		this.vaziDo = vaziDo;
	}
	public HashMap<Kurs, Double> getCene() {
		return cene;
	}
	public void setCene(HashMap<Kurs, Double> cene) {
		this.cene = cene;
	}
	public HashMap<Kurs, Double> getPopusti() {
		return popusti;
	}
	public void setPopusti(HashMap<Kurs, Double> popusti) {
		this.popusti = popusti;
	}

	public HashMap<Kurs, Double> getCeneIzlaska() {
		return ceneIzlaska;
	}

	public void setCeneIzlaska(HashMap<Kurs, Double> ceneIzlaska) {
		this.ceneIzlaska = ceneIzlaska;
	}	
	
	public static String hashMapToString(HashMap<Kurs, Double> mapa) {
		String retVal = "";
		for(Map.Entry<Kurs, Double> entry : mapa.entrySet())
			retVal += entry.getKey().getId() + "," + entry.getValue() + ";";
		return retVal.substring(0, retVal.length() - 1);
	}
	
	@Override
	public String toString() {
		return this.getId() + "|" + this.getVaziOd().format(DateTimeFormatter.ofPattern("dd.MM.yyyy.")) + "|" +
				this.getVaziDo().format(DateTimeFormatter.ofPattern("dd.MM.yyyy.")) + "|" + this.getCenaUpisa() + "|" +
				hashMapToString(this.getCene()) + "|" + hashMapToString(this.getCeneIzlaska()) + "|" + 
				hashMapToString(this.getPopusti()) + "|" + hashMapToString(this.getIznosPovecanja()) + "\n";
		
	}
}
