package entities.other;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class PlatnaKartica {
	String id;
	double stanje;
	HashMap<LocalDateTime, Double> placanja;
	
	public PlatnaKartica(String id, double stanje, HashMap<LocalDateTime, Double> placanja) {
		this.id = id;
		this.stanje = stanje;
		this.placanja = placanja;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getStanje() {
		return stanje;
	}
	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}
	public HashMap<LocalDateTime, Double> getPlacanja() {
		return placanja;
	}
	public void setPlacanja(HashMap<LocalDateTime, Double> placanja) {
		this.placanja = placanja;
	}
	public Double getTotalSpent() {
		Double retVal = 0.0;
		for(Double d : placanja.values()) {
			retVal += d;
		}
		String crop = String.format("%1.2f", retVal);
		return Double.parseDouble(crop);
	}
	public String getPlacanjaString() {
		if (this.placanja.size() == 0)
			return "";
		String retVal = "";
		for(Map.Entry<LocalDateTime, Double> entry : this.getPlacanja().entrySet())
			retVal += entry.getKey().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")) + "," + entry.getValue() + ";";
		return retVal.substring(0, retVal.length() - 1);
	}
	
	@Override
	public String toString() {
		return id + "|" + stanje + "|" + this.getPlacanjaString() + "\n";
	}
}
