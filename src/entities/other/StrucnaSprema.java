package entities.other;

public enum StrucnaSprema {
	SREDNJA_STRUCNA_SPREMA(0),
	VISA_STRUCNA_SPREMA(1),
	VISOKA_STRUCNA_SPREMA(2),
	OSNOVNE_STRUKOVNE_STUDIJE(3),
	OSNOVNE_AKADEMSKE_STUDIJE(5),
	MAGISTAR_NAUKA(7),
	DOKTOR_NAUKA(10);

	public final int stepen;
	
	private StrucnaSprema(int stepen) {
		this.stepen = stepen;
	}
}
