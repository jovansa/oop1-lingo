package entities.other;

public enum NivoKursa {
	OSNOVNI(1),
	SREDNJI(2),
	NAPREDNI(3);
	
	public final int level;
	private NivoKursa(int level) {
		this.level = level;
	}
}
