package entities.other;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import entities.users.Korisnik;

public class IstorijatPlate {
	
	LocalDate start;
	LocalDate end;
	HashMap<Korisnik, Double> spisakPlata;
	
	public IstorijatPlate(LocalDate start, LocalDate end, HashMap<Korisnik, Double> spisakPlata) {
		this.start = start;
		this.end = end;
		this.spisakPlata = spisakPlata;
	}

	public LocalDate getStart() {
		return start;
	}

	public void setStart(LocalDate start) {
		this.start = start;
	}

	public LocalDate getEnd() {
		return end;
	}

	public void setEnd(LocalDate end) {
		this.end = end;
	}

	public HashMap<Korisnik, Double> getSpisakPlata() {
		return spisakPlata;
	}

	public void setSpisakPlata(HashMap<Korisnik, Double> spisakPlata) {
		this.spisakPlata = spisakPlata;
	}

	public String getSpisakPlataString() {
		String retVal = "";
		for (Map.Entry<Korisnik, Double> entry : spisakPlata.entrySet()) {
		    Korisnik key = entry.getKey();
		    Double value = entry.getValue();
		    retVal += key.getJmbg() + "," + value + ";";
		}
		if (retVal.length() > 0)
			return retVal.substring(0, retVal.length() - 1);
		return "";
	}
	
	@Override
	public String toString() {
		return start.format(DateTimeFormatter.ofPattern("dd.MM.yyyy.")) + "|" + 
			   end.format(DateTimeFormatter.ofPattern("dd.MM.yyyy.")) + "|" + this.getSpisakPlataString() + "\n";
	}
}
