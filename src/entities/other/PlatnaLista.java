package entities.other;

import entities.users.Predavac;
import entities.users.Sekretar;

public class PlatnaLista {
	
	String tip;
	double osnova;
	double koeficijentRadnogStaza;
	double koeficijentStrucneSpreme;
	
	public PlatnaLista(String tip, double osnova, double koeficijentRadnogStaza, double koeficijentStrucneSpreme) {
		this.tip = tip;
		this.osnova = osnova;
		this.koeficijentRadnogStaza = koeficijentRadnogStaza;
		this.koeficijentStrucneSpreme = koeficijentStrucneSpreme;
	}
	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}
	public double getOsnova() {
		return osnova;
	}
	public void setOsnova(double osnova) {
		this.osnova = osnova;
	}
	public double getKoeficijentRadnogStaza() {
		return koeficijentRadnogStaza;
	}
	public void setKoeficijentRadnogStaza(double koeficijentRadnogStaza) {
		this.koeficijentRadnogStaza = koeficijentRadnogStaza;
	}
	public double getKoeficijentStrucneSpreme() {
		return koeficijentStrucneSpreme;
	}
	public void setKoeficijentStrucneSpreme(double koeficijentStrucneSpreme) {
		this.koeficijentStrucneSpreme = koeficijentStrucneSpreme;
	}
	
	@Override
	public String toString() {
		return this.getTip() + "|" + this.getOsnova() + "|" + this.getKoeficijentRadnogStaza() + "|"
							 + this.getKoeficijentStrucneSpreme();
	}
	public double obracunajPlatu(Sekretar s) {
		return getOsnova() + s.getStaz() * getKoeficijentRadnogStaza() 
						   + s.getStrucnaSprema().stepen * getKoeficijentStrucneSpreme();
	}
	
	public double obracunajPlatu(Predavac p) {
		return getOsnova() + p.getStaz() * getKoeficijentRadnogStaza() 
				   + p.getStrucnaSprema().stepen * getKoeficijentStrucneSpreme();
	}

	public double obracunajBonus(Sekretar s, String pravilo, int brojUpisanih) {
		if (pravilo.equals("Trinaesta plata"))
			return s.getPlata() / 12;
		else if (pravilo.equals("Broj upisanih polaznika"))
			return brojUpisanih * 500;
		return 0;

	}

	public double obracunajBonus(Predavac p, String pravilo, int pomocniBroj) {
		if (pravilo.equals("Trinaesta plata"))
			return p.getPlata() / 12;
		else if (pravilo.equals("Broj kreiranih testova"))
			return pomocniBroj * 1000;
		else if(pravilo.equals("Broj predavanih kurseva"))
			return pomocniBroj * 5000;
		return 0;

	}
}
