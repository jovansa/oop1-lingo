package gui.admin;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.HashMap;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.other.IstorijatPlate;
import entities.users.*;
import gui.customcomponents.CustomButton;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;


@SuppressWarnings("serial")
public class AdminBonusPanel extends JPanel implements ActionListener{
	
	private DataHandler dh;
	
	private CustomButton obracunajButton;
	private CustomButton odustaniButton;
	
	private JComboBox<String> predavacKombo;
	private JComboBox<String> sekretarKombo;
	
	public AdminBonusPanel(DataHandler dh) {
		this.dh = dh;
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
	
		JLabel naslovLabel = new JLabel("Određivanje plate");
		naslovLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		naslovLabel.setBounds(349, 15, 276, 52);
		add(naslovLabel);
		
		JLabel podnaslovLabel = new JLabel("Način na koji se  mesečna plata računa je sledeći: ");
		podnaslovLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		podnaslovLabel.setBounds(10, 78, 543, 52);
		add(podnaslovLabel);
		
		JLabel formulaLabel = new JLabel("Osnova + Godine staža * x + Koeficijent stručne spreme * y + Bonus");
		formulaLabel.setForeground(Color.DARK_GRAY);
		formulaLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		formulaLabel.setBounds(10, 141, 754, 52);
		add(formulaLabel);
		
		JLabel lblRadnikJePrilikom = new JLabel("Osnova i koeficijenti su navedeni u ugovoru radnika.  Međutim, bonus je fleksibilan.");
		lblRadnikJePrilikom.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblRadnikJePrilikom.setBounds(10, 204, 918, 52);
		add(lblRadnikJePrilikom);
		
		JLabel lblOdaberiteNekiOd = new JLabel("Odaberite neki od načina za obračun bonusa (prikazan je trenutni):");
		lblOdaberiteNekiOd.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblOdaberiteNekiOd.setBounds(10, 268, 754, 52);
		add(lblOdaberiteNekiOd);
		
		JLabel lblZaSekretara = new JLabel("Za sekretare:");
		lblZaSekretara.setForeground(Color.DARK_GRAY);
		lblZaSekretara.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		lblZaSekretara.setBounds(48, 331, 122, 52);
		add(lblZaSekretara);
		
		JLabel lblZaPredavaa = new JLabel("Za predavače:");
		lblZaPredavaa.setForeground(Color.DARK_GRAY);
		lblZaPredavaa.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		lblZaPredavaa.setBounds(41, 375, 129, 52);
		add(lblZaPredavaa);
		
		String[] sekretarOpcije = {"Bez bonusa", "Trinaesta plata", "Broj upisanih polaznika"};
		String[] predavacOpcije = {"Bez bonusa", "Trinaesta plata", "Broj kreiranih testova", "Broj predavanih kurseva"};
		
		sekretarKombo = new JComboBox<String>(sekretarOpcije);
		sekretarKombo.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		sekretarKombo.setBounds(192, 350, 224, 20);
		sekretarKombo.setSelectedItem(dh.getTipBonusaSekretar());
		add(sekretarKombo);
		
		predavacKombo = new JComboBox<String>(predavacOpcije);
		predavacKombo.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		predavacKombo.setBounds(192, 394, 224, 20);
		predavacKombo.setSelectedItem(dh.getTipBonusaPredavac());
		add(predavacKombo);
		
		JLabel poslednjaLabela = new JLabel("Sistem automatski obračunava bonus na osnovu odabrane opcije.");
		poslednjaLabela.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		poslednjaLabela.setBounds(10, 425, 954, 52);
		add(poslednjaLabela);
		
		obracunajButton = new CustomButton("Obračunaj");
		obracunajButton.setBounds(489, 350, 136, 64);
		obracunajButton.addActionListener(this);
		add(obracunajButton);
		
		odustaniButton = new CustomButton("Odustani");
		odustaniButton.setBounds(659, 350, 136, 64);
		odustaniButton.addActionListener(this);
		add(odustaniButton);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == odustaniButton) {
			AdminLoggedFrame topFrame = (AdminLoggedFrame) SwingUtilities.getWindowAncestor(AdminBonusPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
		
		else if(e.getSource() == obracunajButton) {
			String sekretarOpcija = (String) sekretarKombo.getSelectedItem();
			String predavacOpcija = (String) predavacKombo.getSelectedItem();
			dh.setTipBonusaSekretar(sekretarOpcija);
			dh.setTipBonusaPredavac(predavacOpcija);
			dh.writeTipBonusaToFile(sekretarOpcija, predavacOpcija, DataLoad.tipBonusaPath);
			
			for (Korisnik k: dh.getKorisnici()) {
				double plata = 0;
				if (k instanceof Sekretar) {
					Sekretar s = (Sekretar) k;
					plata += dh.getPlSekretari().obracunajPlatu(s);
					plata += dh.getPlSekretari().obracunajBonus(s, sekretarOpcija, dh.getBrojUpisanih());
					s.setPlata(plata);
				}
				else if (k instanceof Predavac) {
					Predavac p = (Predavac) k;
					int pomocniBroj = 0;
					plata += dh.getPlPredavaci().obracunajPlatu(p);
					
					if (predavacOpcija.equals("Broj kreiranih testova"))
						pomocniBroj = dh.getBrojTestova(p);
					else if (predavacOpcija.equals("Broj predavanih kurseva"))
						pomocniBroj = dh.getBrojKurseva(p);
					plata += dh.getPlPredavaci().obracunajBonus(p, predavacOpcija, pomocniBroj);
					p.setPlata(plata);
				
				}
			}
			dh.writeKorisniciToFile(DataLoad.korisniciPath);
			
			IstorijatPlate last = dh.getSpisakPlata().get(dh.getSpisakPlata().size() - 1);
			HashMap<Korisnik, Double> newMap = dh.getUpdatedIstorijatMap();
			
			if (last.getStart().isEqual(LocalDate.now().plusDays(1)))
				last.setSpisakPlata(newMap);
			else {
				last.setEnd(LocalDate.now());
				dh.addIstorijatPlate(new IstorijatPlate(LocalDate.now().plusDays(1), LocalDate.of(9999, 1, 1), newMap));
			}
			
			dh.getSpisakPlata().set(dh.getSpisakPlata().size() - 1, last);
			dh.writeIstorijatToFile(DataLoad.istorijatPlatePath);
			
			JOptionPane.showMessageDialog(null, "Obračun uspešan!", "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			AdminLoggedFrame topFrame = (AdminLoggedFrame) SwingUtilities.getWindowAncestor(AdminBonusPanel.this);
			topFrame.getLogoLabel().setVisible(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	}
}

