package gui.admin;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.JPanel;

import entities.managers.DataHandler;
import gui.customcomponents.CustomButton;
import utilities.RegistrationValidator;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class AdminIncomePanel extends JPanel implements ActionListener{
	
	private DataHandler dh;
	private JTextField pocetniDatumField;
	private JTextField zavrsniDatumField;
	
	private CustomButton prikaziButton;
	private CustomButton odustaniButton;
	
	private JLabel errorLabel;
	private JLabel infoLabel;
	private JLabel rashodInfoLabel;
	private JLabel prihodInfoLabel;
	private JLabel netoInfoLabel;
	private JLabel rashodLabel;
	private JLabel prihodLabel;
	private JLabel netoLabel;
	private JLabel noticeLabel;
	
	public AdminIncomePanel(DataHandler dh) {
		this.dh = dh;
	
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel naslovLabel = new JLabel("Pregled prihoda i rashoda");
		naslovLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		naslovLabel.setBounds(311, 15, 352, 41);
		add(naslovLabel);
		
		JLabel pocetniDatumLabel = new JLabel("Unesite početni datum:");
		pocetniDatumLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		pocetniDatumLabel.setBounds(75, 99, 254, 41);
		add(pocetniDatumLabel);
		
		JLabel zavrsniDatumLabel = new JLabel("Unesite završni datum:");
		zavrsniDatumLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		zavrsniDatumLabel.setBounds(81, 151, 248, 41);
		add(zavrsniDatumLabel);
		
		pocetniDatumField = new JTextField();
		pocetniDatumField.setBounds(339, 106, 182, 28);
		add(pocetniDatumField);
		pocetniDatumField.setColumns(10);
		
		zavrsniDatumField = new JTextField();
		zavrsniDatumField.setColumns(10);
		zavrsniDatumField.setBounds(339, 160, 182, 28);
		add(zavrsniDatumField);
		
		JLabel formatLabel = new JLabel("(dd.mm.yyyy.)");
		formatLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		formatLabel.setBounds(379, 132, 126, 28);
		add(formatLabel);
		
		prikaziButton = new CustomButton("Prikaži");
		prikaziButton.setBounds(560, 106, 144, 82);
		prikaziButton.addActionListener(this);
		add(prikaziButton);
		
		odustaniButton = new CustomButton("Povratak");
		odustaniButton.setBounds(746, 106, 144, 82);
		odustaniButton.addActionListener(this);
		add(odustaniButton);
		
		infoLabel = new JLabel();
		infoLabel.setForeground(Color.DARK_GRAY);
		infoLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		infoLabel.setBounds(223, 223, 529, 41);
		add(infoLabel);
		
		rashodInfoLabel = new JLabel("Ukupno novca potrošeno na plate:");
		rashodInfoLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		rashodInfoLabel.setBounds(91, 285, 316, 41);
		rashodInfoLabel.setVisible(false);
		add(rashodInfoLabel);
		
		prihodInfoLabel = new JLabel("Ukupno novca zarađeno od učenika:");
		prihodInfoLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		prihodInfoLabel.setBounds(75, 339, 332, 41);
		prihodInfoLabel.setVisible(false);
		add(prihodInfoLabel);
		
		netoInfoLabel = new JLabel("Neto dohodak:");
		netoInfoLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		netoInfoLabel.setBounds(270, 391, 137, 41);
		netoInfoLabel.setVisible(false);
		add(netoInfoLabel);
		
		rashodLabel = new JLabel();
		rashodLabel.setForeground(new Color(204, 0, 51));
		rashodLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		rashodLabel.setBounds(417, 285, 316, 41);
		add(rashodLabel);
		
		prihodLabel = new JLabel();
		prihodLabel.setForeground(new Color(0, 153, 51));
		prihodLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		prihodLabel.setBounds(417, 339, 316, 41);
		add(prihodLabel);
		
		netoLabel = new JLabel();
		netoLabel.setForeground(new Color(0, 153, 51));
		netoLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		netoLabel.setBounds(417, 391, 316, 41);
		add(netoLabel);
		
		noticeLabel = new JLabel("<html><u>** Kompletan iznos plate se obračunava svakom radniku za svaki započet mesec **</u></html>");
		noticeLabel.setForeground(Color.DARK_GRAY);
		noticeLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 18));
		noticeLabel.setBounds(148, 443, 679, 41);
		noticeLabel.setVisible(false);
		add(noticeLabel);
		
		errorLabel = new JLabel();
		errorLabel.setForeground(Color.RED);
		errorLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		errorLabel.setBounds(560, 199, 330, 20);
		add(errorLabel);
	
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == odustaniButton) {
			AdminLoggedFrame topFrame = (AdminLoggedFrame) SwingUtilities.getWindowAncestor(AdminIncomePanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
		
		else if (e.getSource() == prikaziButton) {
			errorLabel.setText("");
			String pocetniDatumString = pocetniDatumField.getText();
			if (!RegistrationValidator.isValidDateString(pocetniDatumString)) {
				errorLabel.setText("Neispravan format početnog datuma!");
				return;
			}
			
			String zavrsniDatumString = zavrsniDatumField.getText();
			if (!RegistrationValidator.isValidDateString(zavrsniDatumString)) {
				errorLabel.setText("Neispravan format završnog datuma!");
				return;
			}
			
			LocalDate pocetniDatum = LocalDate.parse(pocetniDatumString, DateTimeFormatter.ofPattern("dd.MM.yyyy."));
			LocalDate zavrsniDatum = LocalDate.parse(zavrsniDatumString, DateTimeFormatter.ofPattern("dd.MM.yyyy."));
			
			if (zavrsniDatum.isBefore(pocetniDatum) && !zavrsniDatum.isEqual(pocetniDatum)) {
				errorLabel.setText("Početni datum mora biti pre završnog! ");
				return;
			}
			
			infoLabel.setText("<html><u>Prihodi i rashodi između " + pocetniDatumString + " i " 
								+ zavrsniDatumString + "</u></html>");
			rashodInfoLabel.setVisible(true);
			prihodInfoLabel.setVisible(true);
			netoInfoLabel.setVisible(true);
			noticeLabel.setVisible(true);
			
			double rashodi = dh.getRashodi(pocetniDatum, zavrsniDatum);
			double prihodi = dh.getPrihodi(pocetniDatum, zavrsniDatum);
			double neto = prihodi - rashodi;
			
			if (rashodi > prihodi)
				netoLabel.setForeground(new Color(204, 0, 51));
			else
				netoLabel.setForeground(new Color(0, 153, 51));
			
			rashodLabel.setText("RSD " + rashodi);
			prihodLabel.setText("RSD " + prihodi);
			netoLabel.setText("RSD " + neto);
			
		}
		
	}
}
