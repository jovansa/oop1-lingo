package gui.admin;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import entities.managers.DataHandler;
import gui.customcomponents.CustomButton;
import gui.customcomponents.LoggedFrame;
import gui.izvestaji.PrihodiProteklaGodina;
import gui.izvestaji.PrihodiRashodiLine;
import gui.izvestaji.RashodiProteklaGodina;
import gui.izvestaji.UceniciPoJeziku;
import utilities.RegistrationValidator;

@SuppressWarnings("serial")
public class AdminLoggedFrame extends LoggedFrame implements ActionListener{
	JPanel buttonPanel;
	CustomButton plateButton;
	CustomButton uvidButton;
	CustomButton registracijaButton;
	CustomButton entitetiButton;
	
	JMenu chartMenu;
	JMenu izvestajiMenu;
	
	JMenuItem prihodi;
	JMenuItem rashodi;
	JMenuItem uceniciPoJeziku;
	JMenuItem prihodiRashodi;
	
	JMenuItem testoviPredavaca;
	JMenuItem obradeSekretara;
	
	public AdminLoggedFrame(DataHandler dh) {
		super(dh);
		
		chartMenu = new JMenu("Grafički izveštaji");
		this.menuBar.add(chartMenu);
		
		izvestajiMenu = new JMenu("Obični izveštaji");
		this.menuBar.add(izvestajiMenu);
		
		testoviPredavaca = new JMenuItem("Broj organizovanih testova po predavaču");
		testoviPredavaca.setIcon(scroll);
		izvestajiMenu.add(testoviPredavaca);
		testoviPredavaca.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
            	Object[] options = dh.getPredavaciNames();
            	String predavac = (String) JOptionPane.showInputDialog(null, "Odaberite predavača: ", 
            			"Izveštaj", JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
            		if (predavac != null) {
            			int br = AdminLoggedFrame.this.getTestNumForPeriod(predavac);
            			if (br != -1)
            				JOptionPane.showMessageDialog(null, "<html>Broj testova koje je organizovao predavač " +
                        			predavac + " je: " + br + "</html>");
            		}
            }
            }));
		
		
		obradeSekretara = new JMenuItem("Broj obrađenih zahteva po sekretaru");
		obradeSekretara.setIcon(scroll);
		izvestajiMenu.add(obradeSekretara);
		obradeSekretara.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
            	Object[] options = dh.getSekretariNames();
            	String sekretar = (String) JOptionPane.showInputDialog(null, "Odaberite sekretara: ", 
            			"Izveštaj", JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
            	if (sekretar != null)	
            		JOptionPane.showMessageDialog(null, "<html>Broj zahteva koje je regulisao sekretar " +
            			sekretar + " je: " + dh.getNumObradjenih(sekretar) + "</html>");
            }
            }));
		
		prihodi = new JMenuItem("Prikaz prihoda za tekuću godinu");
		prihodi.setIcon(pieChartIcon);
		prihodi.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
    			Thread t = new Thread(new Runnable() {
    				@Override
    				public void run() {
    	            	PrihodiProteklaGodina izv = new PrihodiProteklaGodina(AdminLoggedFrame.this.getData());
    	    			izv.prihodiLastYear(AdminLoggedFrame.this.getData());
    				}
    			});
    			t.start();
            }
            }));

		rashodi = new JMenuItem("Prikaz rashoda za tekuću godinu");
		rashodi.setIcon(pieChartIcon);
		rashodi.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
    			Thread t = new Thread(new Runnable() {
    				@Override
    				public void run() {
    	            	RashodiProteklaGodina izv = new RashodiProteklaGodina(AdminLoggedFrame.this.getData());
    	    			izv.rashodiLastYear(AdminLoggedFrame.this.getData());
    				}
    			});
    			t.start();
            }
            }));
		
		uceniciPoJeziku = new JMenuItem("Prikaz odnosa broja učenika po jeziku");
		uceniciPoJeziku.setIcon(pieChartIcon);
		uceniciPoJeziku.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
    			Thread t = new Thread(new Runnable() {
    				@Override
    				public void run() {
    	            	UceniciPoJeziku izv = new UceniciPoJeziku(AdminLoggedFrame.this.getData());
    	    			izv.uceniciIzvestaj(AdminLoggedFrame.this.getData());
    				}
    			});
    			t.start();
            }
            }));
		
		prihodiRashodi = new JMenuItem("Odnos prihoda i rashoda u proteklih 12 meseci");
		prihodiRashodi.setIcon(lineChartIcon);
		prihodiRashodi.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
    			Thread t = new Thread(new Runnable() {
    				@Override
    				public void run() {
    	            	PrihodiRashodiLine izv = new PrihodiRashodiLine(AdminLoggedFrame.this.getData());
    	    			izv.prihodiRashodiIzvestaj(AdminLoggedFrame.this.getData());
    				}
    			});
    			t.start();
            }
            }));
		
		chartMenu.add(prihodi);
		chartMenu.add(rashodi);
		chartMenu.add(uceniciPoJeziku);
		chartMenu.add(prihodiRashodi);
		
		buttonPanel = new JPanel();
		buttonPanel.setBounds(10, 527, 974, 112);
		buttonPanel.setLayout(null);
		buttonPanel.setBackground(new Color(255, 168, 168));
		getContentPane().add(buttonPanel);
		
		plateButton = new CustomButton("Određivanje plate");
		plateButton.setBounds(41, 25, 183, 63);
		plateButton.addActionListener(this);
		buttonPanel.add(plateButton);
		
		uvidButton = new CustomButton("<html>Prihodi i rashodi</html>");
		uvidButton.setBounds(278, 25, 183, 63);
		uvidButton.addActionListener(this);
		buttonPanel.add(uvidButton);
		
		registracijaButton = new CustomButton("<html>Registracija<br />zaposlenih</html>");
		registracijaButton.setBounds(514, 25, 183, 63);
		registracijaButton.addActionListener(this);
		buttonPanel.add(registracijaButton);
		
		entitetiButton = new CustomButton("<html>Upravljanje<br />entitetima</html>");
		entitetiButton.setBounds(750, 25, 183, 63);
		entitetiButton.addActionListener(this);
		buttonPanel.add(entitetiButton);
		
		buttonPanel.setVisible(true);
		buttonPanel.revalidate();
		buttonPanel.repaint();
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == registracijaButton) {
			AdminLoggedFrame.this.makeButtonsVisible(false);
			AdminLoggedFrame.this.getContentPane().add(new AdminRegistrationPanel(this.getData()));
			AdminLoggedFrame.this.setLabelVisibility(false);
			AdminLoggedFrame.this.revalidate();
			AdminLoggedFrame.this.repaint();
		}
		
		else if (e.getSource() == plateButton) {
			AdminLoggedFrame.this.makeButtonsVisible(false);
			AdminLoggedFrame.this.getContentPane().add(new AdminBonusPanel(this.getData()));
			AdminLoggedFrame.this.setLabelVisibility(false);
			AdminLoggedFrame.this.revalidate();
			AdminLoggedFrame.this.repaint();
		}
	
		else if (e.getSource() == uvidButton) {
			AdminLoggedFrame.this.makeButtonsVisible(false);
			AdminLoggedFrame.this.getContentPane().add(new AdminIncomePanel(this.getData()));
			AdminLoggedFrame.this.setLabelVisibility(false);
			AdminLoggedFrame.this.revalidate();
			AdminLoggedFrame.this.repaint();
		}
	}
	
	public void makeButtonsVisible(boolean state) {
		AdminLoggedFrame.this.entitetiButton.setVisible(state);
		AdminLoggedFrame.this.registracijaButton.setVisible(state);
		AdminLoggedFrame.this.plateButton.setVisible(state);
		AdminLoggedFrame.this.uvidButton.setVisible(state);
		
	}

	public int getTestNumForPeriod(String p) { 
		String startDate;
		String endDate;
		while (true) {
			startDate =(String) JOptionPane.showInputDialog(null, "Unesite početni datum u formatu dd.mm.yyyy.",
					"Izveštaј", JOptionPane.QUESTION_MESSAGE, null, null, null);
			if(!RegistrationValidator.isValidDateString(startDate) && startDate != null) 
				JOptionPane.showMessageDialog(null, "Neispravan format datuma!",
						"Greška", JOptionPane.ERROR_MESSAGE);
			else if (startDate == null) return -1;
			else break;
		
		}
		while (true) {
			endDate =(String) JOptionPane.showInputDialog(null, "Unesite krajnji datum u formatu dd.mm.yyyy.",
					"Izveštaј", JOptionPane.QUESTION_MESSAGE, null, null, null);
			if(!RegistrationValidator.isValidDateString(endDate) && endDate != null) {
				JOptionPane.showMessageDialog(null, "Neispravan format datuma!",
						"Greška", JOptionPane.ERROR_MESSAGE);
				continue;
			}
			if (endDate == null)
				return -1;
			if(LocalDate.parse(endDate, DateTimeFormatter.ofPattern("dd.MM.yyyy.")).isBefore(
					LocalDate.parse(startDate, DateTimeFormatter.ofPattern("dd.MM.yyyy."))))
				JOptionPane.showMessageDialog(null, "Neispravan raspored datuma!",
						"Greška", JOptionPane.ERROR_MESSAGE);
			else break;
		}
		if(startDate == null || endDate == null)
			return -1;
		LocalDateTime start = LocalDate.parse(startDate, DateTimeFormatter.ofPattern("dd.MM.yyyy.")).atStartOfDay();
		LocalDateTime end = LocalDate.parse(endDate, DateTimeFormatter.ofPattern("dd.MM.yyyy.")).atStartOfDay();
		return this.getData().getNumOfTests(start, end, p);
	}
}
