package gui.admin;

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;

import entities.users.*;
import entities.users.Korisnik.Pol;
import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.other.Jezik;
import entities.other.StrucnaSprema;
import gui.customcomponents.CustomButton;
import utilities.PasswordHasher;
import utilities.RegistrationValidator;
import javax.swing.JCheckBox;


@SuppressWarnings("serial")
public class AdminRegistrationPanel extends JPanel implements ActionListener {
	
	private DataHandler dh;
	
	private JTextField dobField;
	private JTextField telefonField;
	private JTextField adresaField;
	private JTextField jmbgField;
	private JTextField prezimeField;
	private JTextField imeField;
	private JTextField usernameField;
	
	private JLabel errorLabel;
	
	private CustomButton registerButton;
	private CustomButton odustaniButton;
	
	private JRadioButton predavacButton;
	private JRadioButton sekretarButton;
	
	private JRadioButton muskaracButton;
	private JRadioButton zenaButton;
	
	private JComboBox<String> comboBox;
	private JPasswordField passwordField;
	
	private JCheckBox nemackiCheck;
	private JCheckBox engleskiCheck;
	private JCheckBox francuskiCheck;
	
	public AdminRegistrationPanel(DataHandler dh) {
		this.dh = dh;
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel lblNewLabel = new JLabel("Registracija zaposlenog");
		lblNewLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		lblNewLabel.setBounds(325, 15, 323, 52);
		add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Ime:");
		lblNewLabel_1.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_1.setBounds(229, 112, 50, 29);
		add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Prezime:");
		lblNewLabel_2.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_2.setBounds(185, 152, 94, 29);
		add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("JMBG:");
		lblNewLabel_3.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_3.setBounds(209, 192, 70, 29);
		add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Adresa:");
		lblNewLabel_4.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_4.setBounds(198, 232, 81, 29);
		add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Broj Telefona:");
		lblNewLabel_5.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_5.setBounds(128, 272, 151, 32);
		add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Datum Rođenja:");
		lblNewLabel_6.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_6.setBounds(102, 315, 177, 29);
		add(lblNewLabel_6);
		
		dobField = new JTextField();
		dobField.setBounds(290, 315, 177, 29);
		add(dobField);
		dobField.setColumns(10);
		
		telefonField = new JTextField();
		telefonField.setBounds(290, 272, 177, 30);
		add(telefonField);
		telefonField.setColumns(10);
		
		adresaField = new JTextField();
		adresaField.setBounds(289, 232, 178, 29);
		add(adresaField);
		adresaField.setColumns(10);
		
		jmbgField = new JTextField();
		jmbgField.setBounds(289, 192, 178, 29);
		add(jmbgField);
		jmbgField.setColumns(10);
		
		prezimeField = new JTextField();
		prezimeField.setColumns(10);
		prezimeField.setBounds(289, 152, 178, 29);
		add(prezimeField);
		
		imeField = new JTextField();
		imeField.setColumns(10);
		imeField.setBounds(289, 112, 178, 29);
		add(imeField);
		
		ButtonGroup polGroup = new ButtonGroup();
		
		muskaracButton = new JRadioButton("Muškarac");
		muskaracButton.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		muskaracButton.setBounds(597, 112, 131, 23);
		muskaracButton.setBackground(new Color(255, 168, 168));
		muskaracButton.setSelected(true);
		polGroup.add(muskaracButton);
		add(muskaracButton);
		
		zenaButton = new JRadioButton("Žena");
		zenaButton.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		zenaButton.setBounds(597, 152, 81, 23);
		zenaButton.setBackground(new Color(255, 168, 168));
		polGroup.add(zenaButton);
		add(zenaButton);
		
		ButtonGroup ulogaGroup = new ButtonGroup();
		
		sekretarButton = new JRadioButton("Sekretar");
		sekretarButton.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		sekretarButton.setFocusable(false);
		sekretarButton.setBounds(746, 112, 109, 23);
		sekretarButton.setBackground(new Color(255, 168, 168));
		sekretarButton.setSelected(true);
		sekretarButton.addActionListener(this);
		ulogaGroup.add(sekretarButton);
		add(sekretarButton);
		
		predavacButton = new JRadioButton("Predavač");
		predavacButton.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		predavacButton.setFocusable(false);
		predavacButton.setBounds(746, 152, 109, 23);
		predavacButton.setBackground(new Color(255, 168, 168));
		predavacButton.addActionListener(this);
		ulogaGroup.add(predavacButton);
		add(predavacButton);
		
		JLabel lblKorisnikoIme = new JLabel("Korisničko ime:");
		lblKorisnikoIme.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblKorisnikoIme.setBounds(112, 355, 165, 32);
		add(lblKorisnikoIme);
		
		JLabel lblLozinka = new JLabel("Lozinka:");
		lblLozinka.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblLozinka.setBounds(190, 398, 89, 32);
		add(lblLozinka);
		
		usernameField = new JTextField();
		usernameField.setColumns(10);
		usernameField.setBounds(290, 355, 177, 29);
		add(usernameField);
		
		registerButton = new CustomButton("Registruj");
		registerButton.setBounds(596, 296, 259, 72);
		registerButton.addActionListener(this);
		add(registerButton);
		
		JLabel formatLabel = new JLabel("(dd.mm.yyyy.)");
		formatLabel.setFont(new Font("Segoe UI Semibold", Font.ITALIC, 18));
		formatLabel.setBounds(477, 315, 177, 29);
		add(formatLabel);
		
		errorLabel = new JLabel("");
		errorLabel.setForeground(Color.RED);
		errorLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		errorLabel.setBounds(597, 441, 329, 64);
		add(errorLabel);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(290, 398, 177, 29);
		add(passwordField);
		
		odustaniButton = new CustomButton("Odustani");
		odustaniButton.setBounds(597, 381, 259, 72);
		odustaniButton.addActionListener(this);
		add(odustaniButton);
		
		engleskiCheck = new JCheckBox("Engleski");
		engleskiCheck.setVisible(false);
		engleskiCheck.setFocusable(false);
		engleskiCheck.setBackground(new Color(255, 168, 168));
		engleskiCheck.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		engleskiCheck.setBounds(597, 227, 101, 29);
		add(engleskiCheck);
		
		nemackiCheck = new JCheckBox("Nemački");
		nemackiCheck.setVisible(false);
		nemackiCheck.setFocusable(false);
		nemackiCheck.setBackground(new Color(255, 168, 168));
		nemackiCheck.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		nemackiCheck.setBounds(746, 227, 123, 29);
		add(nemackiCheck);
		
		francuskiCheck = new JCheckBox("Francuski");
		francuskiCheck.setVisible(false);
		francuskiCheck.setFocusable(false);
		francuskiCheck.setBackground(new Color(255, 168, 168));
		francuskiCheck.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		francuskiCheck.setBounds(597, 260, 131, 29);
		add(francuskiCheck);
		
		String[] spreme = {"SREDNJA_STRUCNA_SPREMA", "VISA_STRUCNA_SPREMA", "VISOKA_STRUCNA_SPREMA",
				"OSNOVNE_STRUKOVNE_STUDIJE", "OSNOVNE_AKADEMSKE_STUDIJE", "MAGISTAR_NAUKA", "DOKTOR_NAUKA"};
		
		comboBox = new JComboBox<String>(spreme);
		comboBox.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		comboBox.setBounds(597, 192, 253, 23);
		comboBox.setEnabled(true);
		add(comboBox);		
	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == odustaniButton) {
			AdminLoggedFrame topFrame = (AdminLoggedFrame) SwingUtilities.getWindowAncestor(AdminRegistrationPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
		
		else if(e.getSource() == predavacButton) {
			engleskiCheck.setVisible(true);
			francuskiCheck.setVisible(true);
			nemackiCheck.setVisible(true);
		}
		
		else if(e.getSource() == sekretarButton) {
			engleskiCheck.setVisible(false);
			francuskiCheck.setVisible(false);
			nemackiCheck.setVisible(false);
		}
		
		else if(e.getSource() == registerButton) {
			boolean ready = true;
			errorLabel.setText("");
			
			String username = usernameField.getText();
			
			if (!RegistrationValidator.isValidUsername(username)) {
				errorLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
				errorLabel.setText("<html>Neispravno korisničko ime!<br />"
						+ "Pravila: dužina [6, 20], slova, brojevi i donja crta! </html>");
				ready = false;
			}
			else if(dh.nalogExists(username)) {
				errorLabel.setText("Korisničko ime već postoji!");
				ready = false;
			}
			
			String password = passwordField.getText();
			
			if (!RegistrationValidator.isValidPassword(password)) {
				errorLabel.setText("Lozinka mora sadržati bar 6 karaktera!");
				ready = false;
			}
				
			String ime = imeField.getText();
			if (!RegistrationValidator.isValidName(ime)) {
				errorLabel.setText("Neispravno ime! Podržana su samo slova!");
				ready = false;
			}
			
			String prezime = prezimeField.getText();
			if (!RegistrationValidator.isValidName(prezime)) {
				errorLabel.setText("Neispravno prezime! Podržana su samo slova!");
				ready = false;
			}
			
			String jmbg = jmbgField.getText();
			if (dh.jmbgExists(jmbg)) {
				errorLabel.setText("JMBG već postoji!");
				ready = false;
			}
				
			String adresa = adresaField.getText();
			String telefon = telefonField.getText();
			if(!RegistrationValidator.isValidTelefon(telefon)) {
				errorLabel.setText("Telefon mora imati samo cifre i '+'");
				ready = false;
			}
			
			
			LocalDate datumRodjenja = null;
			if (!RegistrationValidator.isValidDateString(dobField.getText())) {
				errorLabel.setText("Neispravan format datuma!");
				ready = false;
			}
			else
				datumRodjenja = LocalDate.parse(dobField.getText(), DateTimeFormatter.ofPattern("dd.MM.yyyy."));
			
			ArrayList<Jezik> jezici = new ArrayList<Jezik>();
			if (!sekretarButton.isSelected() && !engleskiCheck.isSelected() 
					&& !francuskiCheck.isSelected() && !nemackiCheck.isSelected()) {
				ready = false;
				errorLabel.setText("<html>Morate odabrati bar jedan jezik<br /> za predavača!</html>");
			}
			else {
				if (engleskiCheck.isSelected())
					jezici.add(Jezik.ENGLESKI);
				if (nemackiCheck.isSelected())
					jezici.add(Jezik.NEMACKI);
				if (francuskiCheck.isSelected())
					jezici.add(Jezik.FRANCUSKI);
			}
			
			
			Pol pol;
			if(muskaracButton.isSelected()) 
				pol = Pol.MUSKO;
			else
				pol = Pol.ZENSKO;
			
			KorisnickiNalog nalog = new KorisnickiNalog(username, PasswordHasher.hashPass(password, username));
			
	
			Korisnik novi;
			if (ready) {
				if (predavacButton.isEnabled()) {
					novi = new Predavac(jmbg, ime, prezime, telefon, adresa, pol, datumRodjenja, nalog,
						StrucnaSprema.valueOf((String)comboBox.getSelectedItem()), jezici, 0, 0);
					Predavac p = (Predavac) novi;
					p.setPlata(dh.getPlPredavaci().obracunajPlatu(p));
					}
				else {
					novi = new Sekretar(jmbg, ime, prezime, telefon, adresa, pol, datumRodjenja, nalog,
							StrucnaSprema.valueOf((String)comboBox.getSelectedItem()), 0, 0);
					Sekretar s = (Sekretar) novi;
					s.setPlata(dh.getPlPredavaci().obracunajPlatu(s));
				}
				
			dh.addKorisnik(novi);
			dh.writeEntityToFile(novi, DataLoad.korisniciPath);
			dh.addNalog(nalog);
			dh.writeEntityToFile(nalog, DataLoad.naloziPath);
			
			JOptionPane.showMessageDialog(null, "Registracija uspešna!", "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			AdminLoggedFrame topFrame = (AdminLoggedFrame) SwingUtilities.getWindowAncestor(AdminRegistrationPanel.this);
			topFrame.getLogoLabel().setVisible(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
			}
		}
	}
}
