package gui.customcomponents;

import java.util.HashMap;
import java.util.Map;

import org.knowm.xchart.PieChart;
import org.knowm.xchart.PieChartBuilder;
import org.knowm.xchart.demo.charts.ExampleChart;

public class PieChartSample implements ExampleChart<PieChart>{

	int width;
	int heigth;
	String title;
	HashMap<String, Double> data;
	
	public PieChartSample(int width, int height, String title, HashMap<String, Double> data) {
		this.width = width;
		this.heigth = height;
		this.title = title;
		this.data = data;
	}
	
	@Override
	public PieChart getChart() {
		PieChart chart = new PieChartBuilder().width(this.width).height(this.heigth).title(title).build();
		
		for(Map.Entry<String, Double> entry : this.data.entrySet())
			chart.addSeries(entry.getKey(), entry.getValue());
		
		return chart;
	}

	@Override
	public String getExampleChartName() {
		return this.title;
	}
	

}
