package gui.customcomponents;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import entities.managers.DataHandler;
import gui.login.LoginScreen;
import java.awt.Font;

@SuppressWarnings("serial")
public class LoggedFrame extends JFrame implements ActionListener {
	
	ImageIcon icon = new ImageIcon("img\\L.png");
	ImageIcon logout = new ImageIcon("img\\logout.png");
	ImageIcon exit = new ImageIcon("img\\exit.png");
	
	protected ImageIcon scroll = new ImageIcon("img\\scroll.png");
	protected ImageIcon barChartIcon = new ImageIcon("img\\barchart.png");
	protected ImageIcon pieChartIcon = new ImageIcon("img\\piechart.png");
	protected ImageIcon lineChartIcon = new ImageIcon("img\\linechart.png");
	
	protected JLabel logoLabel;
	
	public JMenuBar menuBar;
	JMenu odjavaMenu;
	JMenuItem odjava;
	JMenuItem ugasi;
	
	private DataHandler data;
	protected JLabel lblUlogovan;
	protected JLabel lblUloga;
	protected JLabel ulogovanLabel;
	protected JLabel ulogaLabel;
	
	public LoggedFrame(DataHandler data){
		
		this.data = data;
		
		logoLabel = new JLabel();
		logoLabel.setForeground(Color.LIGHT_GRAY);
		logoLabel.setBounds(220, 92, 544, 350);
		logoLabel.setIcon(new ImageIcon(icon.getImage().getScaledInstance(350, 350, Image.SCALE_SMOOTH)));
		logoLabel.setVerticalAlignment(JLabel.CENTER);
		logoLabel.setHorizontalAlignment(JLabel.CENTER);
		getContentPane().add(logoLabel);
		
		menuBar = new JMenuBar();
		
		odjavaMenu = new JMenu("Odjava");
		menuBar.add(odjavaMenu);
		
		odjava = new JMenuItem("Odjavi se");
		odjava.setIcon(logout);
		odjava.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
    			new LoginScreen(getData());
    			LoggedFrame.this.dispose();
            	}
            }));
		
		ugasi = new JMenuItem("Ugasi program");
		ugasi.setIcon(exit);
		ugasi.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
    				System.exit(0);
            	}
            }));
		
		odjavaMenu.add(odjava);
		odjavaMenu.add(ugasi);
		
		this.setJMenuBar(menuBar);
		
		this.setSize(new Dimension(1000, 700));
		this.setLocationRelativeTo(null);
		this.setTitle("Lingo");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setIconImage(icon.getImage());
		this.getContentPane().setBackground(new Color(255, 209, 209));
		getContentPane().setLayout(null);
		
		lblUlogovan = new JLabel("Trenutno ulogovan:");
		lblUlogovan.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		lblUlogovan.setBounds(23, 37, 143, 22);
		getContentPane().add(lblUlogovan);
		
		lblUloga = new JLabel("Uloga:");
		lblUloga.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		lblUloga.setBounds(119, 56, 47, 22);
		getContentPane().add(lblUloga);
		
		ulogovanLabel = new JLabel("");
		ulogovanLabel.setForeground(Color.DARK_GRAY);
		ulogovanLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		ulogovanLabel.setBounds(176, 37, 143, 22);
		getContentPane().add(ulogovanLabel);
		
		ulogaLabel = new JLabel("");
		ulogaLabel.setForeground(Color.DARK_GRAY);
		ulogaLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		ulogaLabel.setBounds(176, 56, 143, 22);
		getContentPane().add(ulogaLabel);
		
		ulogovanLabel.setText(data.getCurrentlyLogged().getIme() + " " + data.getCurrentlyLogged().getPrezime());
		ulogaLabel.setText(data.getCurrentlyLogged().getClass().getSimpleName());
		this.setVisible(true);
	}
	
	public JLabel getLogoLabel() {
		return this.logoLabel;
	}
	
	public DataHandler getData() {
		return this.data;
	}
	
	public void setLabelVisibility(boolean state) {
		logoLabel.setVisible(state);
		lblUlogovan.setVisible(state);
		lblUloga.setVisible(state);
		ulogaLabel.setVisible(state);
		ulogovanLabel.setVisible(state);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

	}
}