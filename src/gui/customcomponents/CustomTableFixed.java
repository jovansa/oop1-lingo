package gui.customcomponents;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;


@SuppressWarnings("serial")
public class CustomTableFixed extends JTable {

	public CustomTableFixed() {
		this.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 15));
		this.getTableHeader().setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		this.getTableHeader().setBackground(Color.PINK);
		this.setBackground(new Color(255, 209, 209));
		this.setFocusable(false);
		this.setRowSelectionAllowed(false);
		this.setRowHeight(20);
		this.setSelectionBackground(new Color(255, 168, 168));
		this.getTableHeader().setReorderingAllowed(false);
	
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		this.setDefaultRenderer(String.class, centerRenderer);
	}
}
