package gui.customcomponents;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class CustomButton extends JButton {
	
	ImageIcon buttonIcon = new ImageIcon("img\\button.png");
	
	public CustomButton(String text) {
		this.setText(text);
		this.setIcon(new ImageIcon(buttonIcon.getImage().getScaledInstance(415, 102, Image.SCALE_SMOOTH)));
		this.setContentAreaFilled(false);
		this.setOpaque(false);
		this.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		this.setForeground(Color.WHITE);
		this.setFocusable(false);
		this.setHorizontalTextPosition(SwingConstants.CENTER);
		this.setVisible(true);
	}
}