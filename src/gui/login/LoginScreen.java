package gui.login;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import entities.managers.DataHandler;
import entities.users.*;
import gui.admin.AdminLoggedFrame;
import gui.predavac.PredavacLoggedFrame;
import gui.sekretar.SekretarLoggedFrame;
import gui.ucenik.UcenikLoggedFrame;

@SuppressWarnings("serial")
public class LoginScreen extends JFrame implements ActionListener, KeyListener {
	
	ImageIcon icon = new ImageIcon("img\\L.png");
	ImageIcon buttonIcon = new ImageIcon("img\\button.png");
	private JLabel logoLabel;
	private JLabel usernameLabel;
	private JLabel passwordLabel;
	private JLabel headerLabel;
	private JButton prijavaButton;
	private JTextField usernameField;
	private JPasswordField passwordField;
	private JLabel errorLabel;
	
	private DataHandler data;
	
	public LoginScreen(DataHandler data){
		
		this.data = data;
		this.data.setCurrentlyLogged(null);
		
		this.setSize(new Dimension(550, 300));
		this.setLocationRelativeTo(null);
		this.setTitle("Lingo - Prijava");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setIconImage(icon.getImage());
		this.getContentPane().setBackground(Color.PINK);
		getContentPane().setLayout(null);
		
		logoLabel = new JLabel();
		logoLabel.setForeground(Color.LIGHT_GRAY);
		logoLabel.setBounds(0, 32, 544, 239);
		logoLabel.setIcon(new ImageIcon(icon.getImage().getScaledInstance(150, 150, Image.SCALE_SMOOTH)));
		logoLabel.setVerticalAlignment(JLabel.CENTER);
		getContentPane().add(logoLabel);
		
		headerLabel = new JLabel("Lingo - Prijava");
		headerLabel.setBounds(0, 0, 544, 32);
		headerLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		headerLabel.setVerticalAlignment(JLabel.TOP);
		headerLabel.setHorizontalAlignment(JLabel.CENTER);
		getContentPane().add(headerLabel);
		
		usernameField = new JTextField();
		usernameField.setBounds(301, 89, 171, 20);
		getContentPane().add(usernameField);
		usernameField.setColumns(10);
		
		usernameLabel = new JLabel("Korisničko ime:");
		usernameLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		usernameLabel.setBounds(168, 89, 117, 14);
		getContentPane().add(usernameLabel);
		
		passwordLabel = new JLabel("Lozinka:");
		passwordLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		passwordLabel.setBounds(222, 134, 117, 14);
		getContentPane().add(passwordLabel);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(301, 134, 171, 20);
		passwordField.addKeyListener(this);
		getContentPane().add(passwordField);
		
		prijavaButton = new JButton("Prijava");
		prijavaButton.setIcon(new ImageIcon(buttonIcon.getImage().getScaledInstance(415, 102, Image.SCALE_SMOOTH)));
		prijavaButton.setContentAreaFilled(false);
		prijavaButton.setOpaque(false);
		prijavaButton.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		prijavaButton.setForeground(Color.WHITE);
		prijavaButton.setBounds(168, 186, 304, 44);
		prijavaButton.setFocusable(false);
		prijavaButton.setHorizontalTextPosition(SwingConstants.CENTER);
		prijavaButton.addActionListener(this);
		getContentPane().add(prijavaButton);
		
		errorLabel = new JLabel("");
		errorLabel.setForeground(Color.RED);
		errorLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 13));
		errorLabel.setBounds(168, 161, 324, 16);
		getContentPane().add(errorLabel);
		
		this.setVisible(true);
		
	}

	@SuppressWarnings("deprecation")
	public void loginAuthentication() {
		String user = usernameField.getText();
		String pass = passwordField.getText();
		if (!data.nalogExists(user)) 
			errorLabel.setText("Korisnik sa zadatim korisničkim imenom ne postoji!");
		else if (data.authenticateLogin(user, pass) == null) 
			errorLabel.setText("Neispravna lozinka!");
		else {
			data.setCurrentlyLogged(data.getKorisnikFromUser(user));
			
			if(data.getCurrentlyLogged() instanceof Administrator)
				new AdminLoggedFrame(data);	
			else if (data.getCurrentlyLogged() instanceof Predavac)
				new PredavacLoggedFrame(data);
			else if (data.getCurrentlyLogged() instanceof Sekretar)
				new SekretarLoggedFrame(data);
			else if (data.getCurrentlyLogged() instanceof Ucenik)
				new UcenikLoggedFrame(data);
			this.dispose();
		}
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == prijavaButton) {
			loginAuthentication();
		}
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER)
			loginAuthentication();
	}
	
	@Override
	public void keyTyped(KeyEvent e) {		
	}
	@Override
	public void keyReleased(KeyEvent e) {
	}
}
