package gui.ucenik;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import entities.managers.DataHandler;
import entities.users.Ucenik;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;

@SuppressWarnings("serial")
public class UcenikFinansijskaPanel extends JPanel implements ActionListener{
	
	DataHandler dh;
	
	JLabel amountLabel;
	
	CustomTableFixed table;
	CustomButton povratakButton;
	
	public UcenikFinansijskaPanel(DataHandler dh) {
		this.dh = dh;		
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Finansijska Kartica");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		titleLabel.setBounds(362, 15, 251, 52);
		add(titleLabel);
		
		JLabel stanjeLabel = new JLabel("Trenutno stanje na kartici:");
		stanjeLabel.setForeground(Color.DARK_GRAY);
		stanjeLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		stanjeLabel.setBounds(10, 89, 296, 32);
		add(stanjeLabel);
		
		amountLabel = new JLabel("RSD " + ((Ucenik)dh.getCurrentlyLogged()).getPlatnaKartica().getStanje());
		if(((Ucenik)dh.getCurrentlyLogged()).getPlatnaKartica().getStanje() > 0)
			amountLabel.setForeground(new Color(0, 153, 51));
		else
			amountLabel.setForeground(Color.DARK_GRAY);
		
		amountLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		amountLabel.setBounds(301, 89, 296, 32);
		add(amountLabel);
	
		table = new CustomTableFixed();
		table.setModel(new MyTableModel());
		table.setRowHeight(50);
		
		JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBounds(10, 132, 954, 285);
		scrollPane.getViewport().setBackground(new Color(255, 209, 209));
		this.add(scrollPane);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(281, 430, 405, 64);
		povratakButton.addActionListener(this);
		add(povratakButton);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			UcenikLoggedFrame topFrame = (UcenikLoggedFrame) SwingUtilities.getWindowAncestor(UcenikFinansijskaPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	}
	
	public Object[][] getTableContent() {
		int numOfRow = 0;
		Object[][] retVal;
		
		HashMap<LocalDateTime, Double> mapa = ((Ucenik)dh.getCurrentlyLogged()).getPlatnaKartica().getPlacanja();
		
		Set<Entry<LocalDateTime, Double>> entrySet = mapa.entrySet();
		
		ArrayList<Entry<LocalDateTime, Double>> lista = new ArrayList<Entry<LocalDateTime, Double>>(entrySet);
		lista.sort(Comparator.comparing(Entry::getKey));
		Collections.reverse(lista);
		
		retVal = new Object[lista.size()][3];
		for (Entry<LocalDateTime, Double> e : lista) {
			retVal[numOfRow][0] = e.getKey().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm"));
			retVal[numOfRow][1] = e.getValue().toString();
			
			if(e.getValue() > 50000)
				retVal[numOfRow][2] = "Upis na novi kurs";
			else if (e.getValue() < 9000)
				retVal[numOfRow][2] = "Izlazak na test";
			else
				retVal[numOfRow][2] = "Otvaranje kartice";
			numOfRow++;			
		}
		
		return retVal;
	}
	
	class MyTableModel extends AbstractTableModel {
	    private String[] columnNames = {"Vreme Transakcije", "Cena", "Opis"};
	    private Object[][] data = UcenikFinansijskaPanel.this.getTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}
}
