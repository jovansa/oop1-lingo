package gui.ucenik;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import entities.managers.DataHandler;
import entities.other.Jezik;
import entities.other.Kurs;
import entities.other.NivoKursa;
import gui.customcomponents.CustomButton;
import gui.customcomponents.LoggedFrame;

@SuppressWarnings("serial")
public class UcenikLoggedFrame extends LoggedFrame implements ActionListener {

	JPanel buttonPanel;
	CustomButton kurseviButton;
	CustomButton testoviButton;
	CustomButton zahtevButton;
	CustomButton finansijskaButton;
	CustomButton prijavaButton;
	
	public UcenikLoggedFrame(DataHandler data) {
		super(data);
		
		buttonPanel = new JPanel();
		buttonPanel.setBounds(10, 527, 974, 112);
		buttonPanel.setLayout(null);
		buttonPanel.setBackground(new Color(255, 168, 168));
		getContentPane().add(buttonPanel);
		
		kurseviButton = new CustomButton("Pregled kurseva");
		kurseviButton.setBounds(15, 25, 183, 63);
		kurseviButton.addActionListener(this);
		buttonPanel.add(kurseviButton);
	
		testoviButton = new CustomButton("<html><center>Uvid u<br />testove</center></html>");
		testoviButton.setBounds(204, 25, 183, 63);
		testoviButton.addActionListener(this);
		buttonPanel.add(testoviButton);
		
		zahtevButton = new CustomButton("<html><center>Kreiraj zahtev<br />za kurs</center></html>");
		zahtevButton.setBounds(393, 25, 183, 63);
		zahtevButton.addActionListener(this);
		buttonPanel.add(zahtevButton);
		
		finansijskaButton = new CustomButton("<html>Finansijska kartica</html>");
		finansijskaButton.setBounds(582, 25, 183, 63);
		finansijskaButton.addActionListener(this);
		buttonPanel.add(finansijskaButton);
		
		prijavaButton = new CustomButton("<html><center>Prijava na test</center></html>");
		prijavaButton.setBounds(771, 25, 183, 63);
		prijavaButton.addActionListener(this);
		buttonPanel.add(prijavaButton);
		
		buttonPanel.setVisible(true);
		buttonPanel.revalidate();
		buttonPanel.repaint();
		this.setVisible(true);
		
		this.predlogZaNoviKurs();
	
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == kurseviButton) {
			UcenikLoggedFrame.this.makeButtonsVisible(false);
			UcenikLoggedFrame.this.getContentPane().add(new UcenikKurseviPanel(this.getData()));
			UcenikLoggedFrame.this.setLabelVisibility(false);
			UcenikLoggedFrame.this.revalidate();
			UcenikLoggedFrame.this.repaint();
		}
		
		else if (e.getSource() == testoviButton) {
			UcenikLoggedFrame.this.makeButtonsVisible(false);
			UcenikLoggedFrame.this.getContentPane().add(new UcenikTestoviPanel(this.getData()));
			UcenikLoggedFrame.this.setLabelVisibility(false);
			UcenikLoggedFrame.this.revalidate();
			UcenikLoggedFrame.this.repaint();
		}
	
		else if (e.getSource() == zahtevButton) {
			UcenikLoggedFrame.this.makeButtonsVisible(false);
			UcenikLoggedFrame.this.getContentPane().add(new UcenikZahtevPanel(this.getData()));
			UcenikLoggedFrame.this.setLabelVisibility(false);
			UcenikLoggedFrame.this.revalidate();
			UcenikLoggedFrame.this.repaint();
		}
	
		else if(e.getSource() == finansijskaButton) {
			UcenikLoggedFrame.this.makeButtonsVisible(false);
			UcenikLoggedFrame.this.getContentPane().add(new UcenikFinansijskaPanel(this.getData()));
			UcenikLoggedFrame.this.setLabelVisibility(false);
			UcenikLoggedFrame.this.revalidate();
			UcenikLoggedFrame.this.repaint();
		}
	
		else if (e.getSource() == prijavaButton) {
			UcenikLoggedFrame.this.makeButtonsVisible(false);
			UcenikLoggedFrame.this.getContentPane().add(new UcenikPrijavaPanel(this.getData()));
			UcenikLoggedFrame.this.setLabelVisibility(false);
			UcenikLoggedFrame.this.revalidate();
			UcenikLoggedFrame.this.repaint();
		}
	}
	
	public void predlogZaNoviKurs() {
		ArrayList<Kurs> kandidati = new ArrayList<Kurs>();
		for (Kurs k : this.getData().getKursevi()) {
			if (k.getPolaznici().contains(this.getData().getCurrentlyLogged())
					&& k.getKraj().isBefore(LocalDate.now())) {
				NivoKursa nivo = k.getNivo();
				Jezik jezik = k.getJezik();
				for (Kurs nk: this.getData().getKursevi()) {
					if(nk.getJezik().equals(jezik) && nivo.level < nk.getNivo().level && nk.getKraj().isAfter(LocalDate.now())) {
						if (!nk.getPolaznici().contains(this.getData().getCurrentlyLogged()))
							kandidati.add(nk);
					}
				}
			}
		}
		
		for (Kurs k : kandidati) {
			String message = "<html><center>Na osnovu tvog prethodnog uspeha, predlažemo ti da upišeš kurs: "
					+ k.getNaziv() + "</center></html>";
			String[] options = {"Podnesi zahtev za kurs", "Odustani"};
			int response = JOptionPane.showOptionDialog(null, message, "Obaveštenje", JOptionPane.DEFAULT_OPTION,
					JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
		
			if(response == 0) {
				UcenikLoggedFrame.this.makeButtonsVisible(false);
				UcenikLoggedFrame.this.getContentPane().add(new UcenikZahtevPanel(this.getData()));
				UcenikLoggedFrame.this.setLabelVisibility(false);
				UcenikLoggedFrame.this.revalidate();
				UcenikLoggedFrame.this.repaint();
				return;
			}
			else {
				return;
			}
		}
		
	}
	
	public void makeButtonsVisible(boolean state) {
		UcenikLoggedFrame.this.kurseviButton.setVisible(state);
		UcenikLoggedFrame.this.testoviButton.setVisible(state);
		UcenikLoggedFrame.this.zahtevButton.setVisible(state);	
		UcenikLoggedFrame.this.finansijskaButton.setVisible(state);
		UcenikLoggedFrame.this.prijavaButton.setVisible(state);
	}
	
}
