package gui.ucenik;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.other.Kurs;
import entities.other.StanjeZahteva;
import entities.other.Zahtev;
import entities.users.Ucenik;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import gui.customcomponents.MultiLineTableCellRenderer;

@SuppressWarnings("serial")
public class UcenikZahtevPanel extends JPanel implements ActionListener {

	DataHandler dh;
	
	CustomButton posaljiButton;
	CustomButton povratakButton;
	
	CustomTableFixed kursTable;
	
	Kurs odabranKurs;
	
	public UcenikZahtevPanel(DataHandler data) {
		this.dh = data;
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Kreiranje zahteva za upis na kurs");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 30));
		titleLabel.setBounds(264, 15, 446, 50);
		add(titleLabel);
	
		odabranKurs = null;
		
		kursTable = new CustomTableFixed();
		kursTable.setRowSelectionAllowed(true);
		kursTable.setRowHeight(30);
		kursTable.setModel(new MyTableModel());
		kursTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		MultiLineTableCellRenderer renderer = new MultiLineTableCellRenderer();
		kursTable.getColumnModel().getColumn(4).setCellRenderer(renderer);
		MultiLineTableCellRenderer.resizeColumnHeight(kursTable);
		MultiLineTableCellRenderer.resizeColumnWidth(kursTable);
		kursTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	odabranKurs = dh.getKursFromId((String) kursTable.getValueAt(kursTable.getSelectedRow(), 0));
	        }
	    });
		
		JScrollPane scrollPane = new JScrollPane(kursTable);
		scrollPane.setBounds(10, 65, 954, 331);
		scrollPane.getViewport().setBackground(new Color(255, 209, 209));
		this.add(scrollPane);
		
		posaljiButton = new CustomButton("Pošalji zahtev");
		posaljiButton.setBounds(74, 407, 339, 87);
		posaljiButton.addActionListener(this);
		add(posaljiButton);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(563, 407, 339, 87);
		povratakButton.addActionListener(this);
		add(povratakButton);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			UcenikLoggedFrame topFrame = (UcenikLoggedFrame) SwingUtilities.getWindowAncestor(UcenikZahtevPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
		
		else if (e.getSource() == posaljiButton) {
			if (odabranKurs == null) {
				JOptionPane.showMessageDialog(null, "Niste odabrali kurs!", "Greška", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			Zahtev novi = new Zahtev(dh.generateZahtevId(), (Ucenik) dh.getCurrentlyLogged(), odabranKurs,
					StanjeZahteva.U_OBRADI, LocalDateTime.now(), null);
			
			dh.addZahtev(novi);
			dh.writeEntityToFile(novi, DataLoad.zahteviPath);
			
			JOptionPane.showMessageDialog(null, "Zahtev uspešno poslat!", "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			UcenikLoggedFrame topFrame = (UcenikLoggedFrame) SwingUtilities.getWindowAncestor(UcenikZahtevPanel.this);
			topFrame.getLogoLabel().setVisible(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	
	}

	public Object[][] getTableContent() {
		int numOfRow = 0;
		Object[][] retVal;
		
		ArrayList<Kurs> kursevi = new ArrayList<Kurs>();
		for (Kurs k : dh.getKursevi()) {
			if (k.getKraj().isAfter(LocalDate.now()) && !k.getPolaznici().contains(dh.getCurrentlyLogged()))
				kursevi.add(k);
		}
		
		retVal = new Object[kursevi.size()][5];
		
		for (Kurs k : kursevi) {
			retVal[numOfRow][0] = k.getId();
			retVal[numOfRow][1] = k.getNaziv();
			retVal[numOfRow][2] = k.getPocetak().format(DateTimeFormatter.ofPattern("dd.MM.yyyy."));
			retVal[numOfRow][3] = k.getKraj().format(DateTimeFormatter.ofPattern("dd.MM.yyyy."));
			
			String[] predavaci = new String[k.getPredavaci().size()];
			for (int i = 0; i < k.getPredavaci().size(); i++) 
				predavaci[i] = k.getPredavaci().get(i).getIme() + " " + k.getPredavaci().get(i).getPrezime();
			retVal[numOfRow][4] = predavaci;
			numOfRow++;
		}
		return retVal;
	}
	
	class MyTableModel extends AbstractTableModel {
	    private String[] columnNames = {"ID Kursa", "Naziv Kursa", "Datum Početka", "Datum Završetka", "Predavači"};
	    private Object[][] data = UcenikZahtevPanel.this.getTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}
}
