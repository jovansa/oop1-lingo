package gui.ucenik;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;


import entities.managers.DataHandler;
import entities.other.*;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import gui.customcomponents.MultiLineTableCellRenderer;

@SuppressWarnings("serial")
public class UcenikTestoviPanel extends JPanel implements ActionListener {

	private DataHandler dh;
	
	CustomButton povratakButton;
	
	CustomTableFixed table;
	
	public UcenikTestoviPanel(DataHandler dh) {
		this.dh = dh;
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Pregled Testova");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		titleLabel.setBounds(378, 15, 220, 52);
		add(titleLabel);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(282, 407, 405, 64);
		povratakButton.addActionListener(this);
		add(povratakButton);
		
		table = new CustomTableFixed();
		table.setModel(new MyTableModel());
		
		MultiLineTableCellRenderer renderer = new MultiLineTableCellRenderer();
		table.getColumnModel().getColumn(2).setCellRenderer(renderer);
		table.getColumnModel().getColumn(3).setCellRenderer(renderer);
		
		MultiLineTableCellRenderer.resizeColumnHeight(table);
		MultiLineTableCellRenderer.resizeColumnWidth(table);
		
		
		JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBounds(10, 65, 954, 331);
		scrollPane.getViewport().setBackground(new Color(255, 209, 209));
		this.add(scrollPane);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			UcenikLoggedFrame topFrame = (UcenikLoggedFrame) SwingUtilities.getWindowAncestor(UcenikTestoviPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	}
	
	public Object[][] getTableContent(){
		int numOfRow = 0;
		Object[][] retVal;
	
		ArrayList<Test> filtriraniTestovi = new ArrayList<Test>();
		for(Test t: dh.getTestovi()) {
			for(ResenjeTesta rt: t.getResenja()) {
				if (rt.getUcenik().equals(dh.getCurrentlyLogged()))
					filtriraniTestovi.add(t);
			}
		}
		filtriraniTestovi.sort(Comparator.comparing(Test::getAktivanOd));
		Collections.reverse(filtriraniTestovi);
		retVal = new Object[filtriraniTestovi.size()][6];

		for(Test t: filtriraniTestovi) {
			retVal[numOfRow][0] = t.getNaziv();
			
			ArrayList<String> pitanja = t.getPitanja();
			String[] pitanjastr = new String [t.getPitanja().size()];
			for(int i = 0; i < t.getPitanja().size(); i++)
				pitanjastr[i] = pitanja.get(i);
			
			
			retVal[numOfRow][2] = pitanjastr;
			
			for(ResenjeTesta rt : t.getResenja()) {
				if (rt.getUcenik().equals(dh.getCurrentlyLogged())) {
					retVal[numOfRow][1] = rt.getVremeResavanja().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm"));
				
					ArrayList<String> odg = rt.getOdgovori();
					String[] odgovori = new String [odg.size()];
					for(int i = 0; i < odg.size(); i++)
						odgovori[i] = odg.get(i);
					
					retVal[numOfRow][3] = odgovori;
					if(rt.isPregledan()) {
						retVal[numOfRow][4] = ((Double)rt.getBrojBodova()).toString();
						retVal[numOfRow][5] = ((Integer)rt.getOcena()).toString();
					}
					else {
						retVal[numOfRow][4] = "NIJE PREGLEDAN";
						retVal[numOfRow][5] = "NIJE PREGLEDAN";
					}
					numOfRow++;
					break;
				}
			}
		}
		return retVal;
	}
	
	class MyTableModel extends AbstractTableModel {
	    private String[] columnNames = {"Naziv Testa", "Datum Izrade", "Pitanja", "Odgovori", "Broj Bodova", "Ocena"};
	    private Object[][] data = UcenikTestoviPanel.this.getTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}
}
