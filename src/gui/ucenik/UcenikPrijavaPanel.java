package gui.ucenik;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import entities.managers.DataHandler;
import entities.other.Kurs;
import entities.other.ResenjeTesta;
import entities.other.Test;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import gui.customcomponents.MultiLineTableCellRenderer;

@SuppressWarnings("serial")
public class UcenikPrijavaPanel extends JPanel implements ActionListener{

	DataHandler dh;
	
	CustomButton posaljiButton;
	CustomButton povratakButton;
	
	CustomTableFixed testTable;
	
	public UcenikPrijavaPanel(DataHandler data) {
		this.dh = data;
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Prijavljivanje na test");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 30));
		titleLabel.setBounds(350, 15, 274, 50);
		add(titleLabel);
		
		testTable = new CustomTableFixed();
		testTable.setRowSelectionAllowed(true);
		testTable.setRowHeight(30);
		testTable.setModel(new MyTableModel());
		testTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		MultiLineTableCellRenderer.resizeColumnHeight(testTable);
		MultiLineTableCellRenderer.resizeColumnWidth(testTable);

		
		JScrollPane scrollPane = new JScrollPane(testTable);
		scrollPane.setBounds(10, 65, 954, 331);
		scrollPane.getViewport().setBackground(new Color(255, 209, 209));
		this.add(scrollPane);
		
		posaljiButton = new CustomButton("Pošalji zahtev");
		posaljiButton.setBounds(74, 407, 339, 87);
		posaljiButton.addActionListener(this);
		add(posaljiButton);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(563, 407, 339, 87);
		povratakButton.addActionListener(this);
		add(povratakButton);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			UcenikLoggedFrame topFrame = (UcenikLoggedFrame) SwingUtilities.getWindowAncestor(UcenikPrijavaPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
		
		else if (e.getSource() == posaljiButton) {
			JOptionPane.showMessageDialog(null,
					"<html></center>Zahtev uspešno poslat!<br /> O tačnom terminu dolaska bićete obavešteni putem maila!</html></center>", "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			UcenikLoggedFrame topFrame = (UcenikLoggedFrame) SwingUtilities.getWindowAncestor(UcenikPrijavaPanel.this);
			topFrame.getLogoLabel().setVisible(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	}
	
	public Object[][] getTableContent() {
		int numOfRow = 0;
		Object[][] retVal;
		
		ArrayList<Kurs> kursevi = new ArrayList<Kurs>();
		for (Kurs k : dh.getKursevi()) {
			if (k.getKraj().isAfter(LocalDate.now()) && k.getPolaznici().contains(dh.getCurrentlyLogged()))
				kursevi.add(k);
		}
		
		
		ArrayList<Test> nepolaganiTestovi = new ArrayList<Test>();
		for (Kurs k: kursevi) {
			for (Test t: k.getTestovi()) {
				if (t.getResenja().size() > 0)
				for (ResenjeTesta rt: t.getResenja()) {
					if (t.getAktivanDo().isAfter(LocalDateTime.now()) &&
							!rt.getUcenik().equals(dh.getCurrentlyLogged()))
						nepolaganiTestovi.add(t);
						break;
				}
				else if (t.getAktivanDo().isAfter(LocalDateTime.now()))
					nepolaganiTestovi.add(t);
			}
		}
		
		retVal = new Object[nepolaganiTestovi.size()][4];
		
		for (Kurs k : kursevi) {
			for(Test t: nepolaganiTestovi) {
				if (k.getTestovi().contains(t)) {
					retVal[numOfRow][0] = k.getNaziv();
					retVal[numOfRow][1] = t.getNaziv();
					retVal[numOfRow][2] = t.getAktivanOd().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm"));
					retVal[numOfRow][3] = t.getAktivanDo().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm"));		
					numOfRow++;
				}
			}
		}
		return retVal;
	}
	
	class MyTableModel extends AbstractTableModel {
	    private String[] columnNames = {"Naziv Kursa", "Naziv Testa", "Vreme Početka", "Vreme Završetka"};
	    private Object[][] data = UcenikPrijavaPanel.this.getTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}
}
