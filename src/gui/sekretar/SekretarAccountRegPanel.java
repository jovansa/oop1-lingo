package gui.sekretar;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.users.KorisnickiNalog;
import entities.users.Korisnik;
import entities.users.Ucenik;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import utilities.PasswordHasher;
import utilities.RegistrationValidator;

import javax.swing.table.AbstractTableModel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class SekretarAccountRegPanel extends JPanel implements ActionListener {

	private DataHandler dh;
	private CustomTableFixed table;
	
	private JTextField jmbgField;
	private JTextField usernameField;
	private JPasswordField passwordField;
	
	private CustomButton dodeliButton;
	private CustomButton povratakButton;
	private JLabel errorLabel;
	
	public SekretarAccountRegPanel(DataHandler dh) {
		this.dh = dh;
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Dodela naloga polaznicima");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		titleLabel.setBounds(300, 15, 373, 52);
		add(titleLabel);
		
		table = new CustomTableFixed();
		table.setModel(new MyTableModel());
		
		JScrollPane pane =  new JScrollPane(table);
		pane.setBounds(50, 130, 476, 294);
		pane.getViewport().setBackground(new Color(255, 209, 209));
		this.add(pane);
		
		JLabel subtitleLabel = new JLabel("Polaznici koji trenutno nemaju nalog");
		subtitleLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		subtitleLabel.setBounds(134, 94, 307, 25);
		add(subtitleLabel);
		
		JLabel jmbgLabel = new JLabel("JMBG polaznika: ");
		jmbgLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		jmbgLabel.setBounds(558, 156, 140, 25);
		add(jmbgLabel);
		
		JLabel usernameLabel = new JLabel("Korisničko ime:");
		usernameLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		usernameLabel.setBounds(568, 192, 126, 25);
		add(usernameLabel);
		
		JLabel passwordLabel = new JLabel("Lozinka:");
		passwordLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		passwordLabel.setBounds(628, 228, 66, 25);
		add(passwordLabel);
		
		jmbgField = new JTextField();
		jmbgField.setBounds(698, 156, 171, 25);
		add(jmbgField);
		jmbgField.setColumns(10);
		
		usernameField = new JTextField();
		usernameField.setColumns(10);
		usernameField.setBounds(698, 192, 171, 25);
		add(usernameField);
		
		passwordField = new JPasswordField();
		passwordField.setColumns(10);
		passwordField.setBounds(698, 228, 171, 25);
		add(passwordField);
		
		dodeliButton = new CustomButton("Dodeli nalog");
		dodeliButton.setBounds(558, 298, 311, 52);
		dodeliButton.addActionListener(this);
		add(dodeliButton);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(558, 372, 311, 52);
		povratakButton.addActionListener(this);
		add(povratakButton);
		
		errorLabel = new JLabel("");
		errorLabel.setForeground(Color.RED);
		errorLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		errorLabel.setBounds(558, 252, 311, 42);
		add(errorLabel);
	}
	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			SekretarLoggedFrame topFrame = (SekretarLoggedFrame) SwingUtilities.getWindowAncestor(SekretarAccountRegPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	
		else if (e.getSource() == dodeliButton) {
			errorLabel.setText("");
			String jmbg = jmbgField.getText();
			String user = usernameField.getText();
			@SuppressWarnings("deprecation")
			String pass = passwordField.getText();
		
			boolean proceed = false;
			for (int i = 0; i < table.getModel().getRowCount(); i++) {
				if (table.getModel().getValueAt(i, 0).toString().equals(jmbg))
					proceed = true;
			}
			
			if (!proceed) {
				errorLabel.setText("Niste odabrali korisnika iz liste!");
				return;
			}
		
			else if (!RegistrationValidator.isValidUsername(user)) {
				errorLabel.setText("<html>Neispravno korisničko ime!<br />"
						+ "Pravila: dužina [6, 20], slova, brojevi i donja crta! </html>");
			}
		
			else if(dh.nalogExists(user)) {
				errorLabel.setText("Korisničko ime već postoji!");
				return;
			}
		
			else if(!RegistrationValidator.isValidPassword(pass)) {
				errorLabel.setText("Lozinka mora sadržati bar 6 karaktera!");
				return;
			}
		
			KorisnickiNalog nalog = new KorisnickiNalog(user, PasswordHasher.hashPass(pass, user));
			
			dh.addNalog(nalog);
			dh.writeEntityToFile(nalog, DataLoad.naloziPath);
			
			dh.getKorisnikFromJmbg(jmbg).setNalog(nalog);
			dh.writeKorisniciToFile(DataLoad.korisniciPath);
			
			JOptionPane.showMessageDialog(null, "Registracija uspešna!", "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			SekretarLoggedFrame topFrame = (SekretarLoggedFrame) SwingUtilities.getWindowAncestor(SekretarAccountRegPanel.this);
			topFrame.getLogoLabel().setVisible(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	}

	public String[][] getTableContent(){
		int numOfRow = 0;
		String [][] retVal;
		
		ArrayList<Ucenik> lista = new ArrayList<Ucenik>();
		
		for (Korisnik k : dh.getKorisnici()) {
			if (k instanceof Ucenik && k.getNalog() == null) 
				lista.add((Ucenik) k);
		}
	
		retVal = new String[lista.size()][4];
		for (Ucenik u : lista) {
				retVal[numOfRow][0] = u.getJmbg();
				retVal[numOfRow][1] = u.getIme();
				retVal[numOfRow][2] = u.getPrezime();
				retVal[numOfRow][3] = u.getDatumRodjena().format(DateTimeFormatter.ofPattern("dd.MM.yyyy."));
				numOfRow++;
		}
		return retVal;
	}
	
	class MyTableModel extends AbstractTableModel {
	    private String[] columnNames = {"JMBG", "Ime", "Prezime", "Datum Rođenja"};
	    private Object[][] data = SekretarAccountRegPanel.this.getTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}
}
