package gui.sekretar;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import entities.managers.DataHandler;
import gui.customcomponents.CustomButton;
import gui.customcomponents.LoggedFrame;

@SuppressWarnings("serial")
public class SekretarLoggedFrame extends LoggedFrame implements ActionListener{
	
	JPanel buttonPanel;
	CustomButton zahteviButton;
	CustomButton postaviButton;
	CustomButton registrujButton;
	CustomButton dodelaNalogaButton;
	
	JMenu izvestajiMenu;
	
	JMenuItem podaciUcenik;
	JMenuItem podaciZahtevi;
	public SekretarLoggedFrame(DataHandler dh) {
		super(dh);
		
		JMenu izvestajiMenu = new JMenu("Izveštaji");
		this.menuBar.add(izvestajiMenu);
		
		podaciUcenik = new JMenuItem("Podaci o učeniku");
		podaciUcenik.setIcon(scroll);
		izvestajiMenu.add(podaciUcenik);
		podaciUcenik.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
            	SekretarLoggedFrame.this.makeButtonsVisible(false);
    			SekretarLoggedFrame.this.getContentPane().add(new SekretarIzvestajUcenikPanel(SekretarLoggedFrame.this.getData()));
    			SekretarLoggedFrame.this.setLabelVisibility(false);
    			SekretarLoggedFrame.this.revalidate();
    			SekretarLoggedFrame.this.repaint();
            }
        }));
		
		podaciZahtevi = new JMenuItem("Podaci o obrađenim zahtevima");
		podaciZahtevi.setIcon(scroll);
		izvestajiMenu.add(podaciZahtevi);
		podaciZahtevi.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
            	SekretarLoggedFrame.this.makeButtonsVisible(false);
    			SekretarLoggedFrame.this.getContentPane().add(new SekretarIzvestajZahteviPanel(SekretarLoggedFrame.this.getData()));
    			SekretarLoggedFrame.this.setLabelVisibility(false);
    			SekretarLoggedFrame.this.revalidate();
    			SekretarLoggedFrame.this.repaint();
            }
        }));
		
		buttonPanel = new JPanel();
		buttonPanel.setBounds(10, 527, 974, 112);
		buttonPanel.setLayout(null);
		buttonPanel.setBackground(new Color(255, 168, 168));
		getContentPane().add(buttonPanel);
		
		zahteviButton = new CustomButton("Zahtevi za kurseve");
		zahteviButton.setBounds(41, 25, 183, 63);
		zahteviButton.addActionListener(this);
		buttonPanel.add(zahteviButton);
	
		postaviButton = new CustomButton("<html>Postavljanje<br />predavača</html>");
		postaviButton.setBounds(278, 25, 183, 63);
		postaviButton.addActionListener(this);
		buttonPanel.add(postaviButton);
		
		registrujButton = new CustomButton("<html>Registracija<br />polaznika</html>");
		registrujButton.setBounds(514, 25, 183, 63);
		registrujButton.addActionListener(this);
		buttonPanel.add(registrujButton);
		
		dodelaNalogaButton = new CustomButton("<html>Dodela naloga<br />polaznicima</html>");
		dodelaNalogaButton.setBounds(750, 25, 183, 63);
		dodelaNalogaButton.addActionListener(this);
		buttonPanel.add(dodelaNalogaButton);
		
		buttonPanel.setVisible(true);
		buttonPanel.revalidate();
		buttonPanel.repaint();
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == registrujButton) {
			SekretarLoggedFrame.this.makeButtonsVisible(false);
			SekretarLoggedFrame.this.getContentPane().add(new SekretarRegistrationPanel(this.getData()));
			SekretarLoggedFrame.this.setLabelVisibility(false);
			SekretarLoggedFrame.this.revalidate();
			SekretarLoggedFrame.this.repaint();
		}
		
		else if (e.getSource() == zahteviButton) {
			SekretarLoggedFrame.this.makeButtonsVisible(false);
			SekretarLoggedFrame.this.getContentPane().add(new SekretarObradaZahtevaPanel(this.getData()));
			SekretarLoggedFrame.this.setLabelVisibility(false);
			SekretarLoggedFrame.this.revalidate();
			SekretarLoggedFrame.this.repaint();
		}
	
		else if (e.getSource() == postaviButton) {
			SekretarLoggedFrame.this.makeButtonsVisible(false);
			SekretarLoggedFrame.this.getContentPane().add(new SekretarSetKursPanel(this.getData()));
			SekretarLoggedFrame.this.setLabelVisibility(false);
			SekretarLoggedFrame.this.revalidate();
			SekretarLoggedFrame.this.repaint();
		}
	
		else if(e.getSource() == dodelaNalogaButton) {
			SekretarLoggedFrame.this.makeButtonsVisible(false);
			SekretarLoggedFrame.this.getContentPane().add(new SekretarAccountRegPanel(this.getData()));
			SekretarLoggedFrame.this.setLabelVisibility(false);
			SekretarLoggedFrame.this.revalidate();
			SekretarLoggedFrame.this.repaint();
		}
	}
		
	public void makeButtonsVisible(boolean state) {
		SekretarLoggedFrame.this.registrujButton.setVisible(state);
		SekretarLoggedFrame.this.postaviButton.setVisible(state);
		SekretarLoggedFrame.this.zahteviButton.setVisible(state);	
		SekretarLoggedFrame.this.dodelaNalogaButton.setVisible(state);
	}
}
