package gui.sekretar;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.other.PlatnaKartica;
import entities.users.Korisnik.Pol;
import entities.users.Ucenik;
import gui.customcomponents.CustomButton;
import utilities.RegistrationValidator;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class SekretarRegistrationPanel extends JPanel implements ActionListener {
	
	private DataHandler dh;
	
	private JTextField dobField;
	private JTextField telefonField;
	private JTextField adresaField;
	private JTextField jmbgField;
	private JTextField prezimeField;
	private JTextField imeField;

	private JLabel errorLabel;

	private CustomButton odustaniButton;
	private CustomButton registerButton;

	private JRadioButton muskaracButton;
	private JRadioButton zenaButton;
	
	public SekretarRegistrationPanel(DataHandler dh) {
		
		this.dh = dh;
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel lblNewLabel = new JLabel("Registracija polaznika");
		lblNewLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		lblNewLabel.setBounds(325, 15, 323, 52);
		add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Ime:");
		lblNewLabel_1.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_1.setBounds(229, 112, 50, 29);
		add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Prezime:");
		lblNewLabel_2.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_2.setBounds(185, 152, 94, 29);
		add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("JMBG:");
		lblNewLabel_3.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_3.setBounds(209, 192, 70, 29);
		add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Adresa:");
		lblNewLabel_4.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_4.setBounds(198, 232, 81, 29);
		add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Broj Telefona:");
		lblNewLabel_5.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_5.setBounds(128, 272, 151, 32);
		add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Datum Rođenja:");
		lblNewLabel_6.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblNewLabel_6.setBounds(102, 315, 177, 29);
		add(lblNewLabel_6);
		
		dobField = new JTextField();
		dobField.setBounds(290, 315, 177, 29);
		add(dobField);
		dobField.setColumns(10);
		
		telefonField = new JTextField();
		telefonField.setBounds(290, 272, 177, 30);
		add(telefonField);
		telefonField.setColumns(10);
		
		adresaField = new JTextField();
		adresaField.setBounds(289, 232, 178, 29);
		add(adresaField);
		adresaField.setColumns(10);
		
		jmbgField = new JTextField();
		jmbgField.setBounds(289, 192, 178, 29);
		add(jmbgField);
		jmbgField.setColumns(10);
		
		prezimeField = new JTextField();
		prezimeField.setColumns(10);
		prezimeField.setBounds(289, 152, 178, 29);
		add(prezimeField);
		
		imeField = new JTextField();
		imeField.setColumns(10);
		imeField.setBounds(289, 112, 178, 29);
		add(imeField);
		
		JLabel formatLabel = new JLabel("(dd.mm.yyyy.)");
		formatLabel.setFont(new Font("Segoe UI Semibold", Font.ITALIC, 18));
		formatLabel.setBounds(323, 345, 112, 29);
		add(formatLabel);
		
		errorLabel = new JLabel("");
		errorLabel.setForeground(Color.RED);
		errorLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		errorLabel.setBounds(541, 301, 329, 64);
		add(errorLabel);
	
		odustaniButton = new CustomButton("Odustani");
		odustaniButton.setBounds(541, 410, 259, 72);
		odustaniButton.addActionListener(this);
		add(odustaniButton);
	
		registerButton = new CustomButton("Registruj");
		registerButton.setBounds(174, 410, 259, 72);
		registerButton.addActionListener(this);
		add(registerButton);
		
		ButtonGroup polGroup = new ButtonGroup();
		
		muskaracButton = new JRadioButton("Muškarac");
		muskaracButton.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		muskaracButton.setBounds(597, 112, 131, 23);
		muskaracButton.setBackground(new Color(255, 168, 168));
		muskaracButton.setFocusable(false);
		muskaracButton.setSelected(true);
		polGroup.add(muskaracButton);
		add(muskaracButton);
		
		zenaButton = new JRadioButton("Žena");
		zenaButton.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		zenaButton.setBounds(597, 152, 81, 23);
		zenaButton.setBackground(new Color(255, 168, 168));
		zenaButton.setFocusable(false);
		polGroup.add(zenaButton);
		add(zenaButton);
	}
	
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == odustaniButton) {
			SekretarLoggedFrame topFrame = (SekretarLoggedFrame) SwingUtilities.getWindowAncestor(SekretarRegistrationPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	
		else if (e.getSource() == registerButton) {
			errorLabel.setText("");
			
			String ime = imeField.getText();
			if (!RegistrationValidator.isValidName(ime)) {
				errorLabel.setText("Neispravno ime! Podržana su samo slova!");
				return;
			}
			
			String prezime = prezimeField.getText();
			if (!RegistrationValidator.isValidName(prezime)) {
				errorLabel.setText("Neispravno prezime! Podržana su samo slova!");
				return;
			}
			
			String jmbg = jmbgField.getText();
			if (dh.jmbgExists(jmbg)) {
				errorLabel.setText("JMBG već postoji!");
				return;
			}
				
			String adresa = adresaField.getText();
			
			String telefon = telefonField.getText();
			if(!RegistrationValidator.isValidTelefon(telefon)) {
				errorLabel.setText("Telefon mora imati samo cifre i '+'");
				return;
			}
			
			LocalDate datumRodjenja = null;
			if (!RegistrationValidator.isValidDateString(dobField.getText())) {
				errorLabel.setText("Neispravan format datuma!");
				return;
			}
			else
				datumRodjenja = LocalDate.parse(dobField.getText(), DateTimeFormatter.ofPattern("dd.MM.yyyy."));
			
			Pol pol;
			if(muskaracButton.isSelected()) 
				pol = Pol.MUSKO;
			else
				pol = Pol.ZENSKO;
			
			HashMap<LocalDateTime, Double> mapa = new HashMap<LocalDateTime, Double>();
			mapa.put(LocalDateTime.now(), dh.getCurrentCenovnik().getCenaUpisa());
			
			PlatnaKartica pk = new PlatnaKartica(dh.generatePlatnaKarticaId(), 0.0, mapa);
			
			Ucenik novi = new Ucenik(jmbg, ime, prezime, telefon, adresa, pol, datumRodjenja, null, pk);
			
			dh.addKartica(pk);
			dh.addKorisnik(novi);
			dh.writeEntityToFile(novi, DataLoad.korisniciPath);
			dh.writeEntityToFile(pk, DataLoad.karticePath);
			
			JOptionPane.showMessageDialog(null, "Registracija uspešna!", "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			SekretarLoggedFrame topFrame = (SekretarLoggedFrame) SwingUtilities.getWindowAncestor(SekretarRegistrationPanel.this);
			topFrame.getLogoLabel().setVisible(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	}
}