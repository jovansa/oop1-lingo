package gui.sekretar;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import entities.managers.DataHandler;
import entities.other.Jezik;
import entities.other.StanjeZahteva;
import entities.other.Zahtev;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import gui.customcomponents.MultiLineTableCellRenderer;
import utilities.RegistrationValidator;

@SuppressWarnings("serial")
public class SekretarIzvestajZahteviPanel extends JPanel implements ActionListener {
	
	DataHandler dh;
	private JTextField startField;
	private JTextField endField;
	
	JLabel errorLabel;
	
	CustomButton povratakButton;
	CustomButton prikaziButton;
	
	CustomTableFixed izvestajTable;
	
	JScrollPane scrollPane;
	
	LocalDateTime start;
	LocalDateTime end;
	boolean[] flags;
	
	JCheckBox engleskiBox;
	JCheckBox nemackiBox;
	JCheckBox francuskiBox;
	public SekretarIzvestajZahteviPanel(DataHandler dh) {
		this.dh = dh;
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Izveštaj o zahtevima");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		titleLabel.setBounds(349, 15, 276, 52);
		add(titleLabel);
		
		JLabel pocetniLabel = new JLabel("Unesite početni datum:");
		pocetniLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		pocetniLabel.setBounds(68, 112, 254, 40);
		add(pocetniLabel);
		
		JLabel krajnjiLabel = new JLabel("Unesite krajnji datum:");
		krajnjiLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		krajnjiLabel.setBounds(82, 149, 240, 40);
		add(krajnjiLabel);
		
		startField = new JTextField();
		startField.setBounds(332, 119, 182, 30);
		add(startField);
		startField.setColumns(10);
		
		endField = new JTextField();
		endField.setColumns(10);
		endField.setBounds(332, 157, 182, 30);
		add(endField);
		
		JLabel instrLabel = new JLabel("(dd.mm.yyyy.)");
		instrLabel.setFont(new Font("Segoe UI Semibold", Font.ITALIC, 18));
		instrLabel.setBounds(359, 78, 112, 40);
		add(instrLabel);
		
		errorLabel = new JLabel("");
		errorLabel.setForeground(Color.RED);
		errorLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		errorLabel.setBounds(246, 189, 302, 30);
		add(errorLabel);
		
		engleskiBox = new JCheckBox("Engleski");
		engleskiBox.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		engleskiBox.setBackground(new Color(255, 168, 168));
		engleskiBox.setBounds(554, 112, 112, 40);
		engleskiBox.setSelected(true);
		add(engleskiBox);
		
		nemackiBox = new JCheckBox("Nemački");
		nemackiBox.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		nemackiBox.setBackground(new Color(255, 168, 168));
		nemackiBox.setBounds(554, 149, 112, 23);
		nemackiBox.setSelected(true);
		add(nemackiBox);
		
		francuskiBox = new JCheckBox("Francuski");
		francuskiBox.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		francuskiBox.setBackground(new Color(255, 168, 168));
		francuskiBox.setBounds(554, 176, 112, 23);
		francuskiBox.setSelected(true);
		add(francuskiBox);
		
		izvestajTable = new CustomTableFixed();
		
		
		
		scrollPane = new JScrollPane(izvestajTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBounds(10, 270, 954, 204);
		scrollPane.getViewport().setBackground(new Color(255, 209, 209));
		add(scrollPane);
		
		prikaziButton = new CustomButton("Prikaži");
		prikaziButton.setBounds(703, 118, 230, 40);
		prikaziButton.addActionListener(this);
		add(prikaziButton);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(703, 164, 230, 40);
		povratakButton.addActionListener(this);
		add(povratakButton);
	
		
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			SekretarLoggedFrame topFrame = (SekretarLoggedFrame) SwingUtilities.getWindowAncestor(SekretarIzvestajZahteviPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	
		else if (e.getSource() == prikaziButton) {
			errorLabel.setText("");
			String startString = startField.getText();
			String endString = endField.getText();
			flags = new boolean[3];
			flags[0] = engleskiBox.isSelected();
			flags[1] = nemackiBox.isSelected();
			flags[2] = francuskiBox.isSelected();
			
			if(!RegistrationValidator.isValidDateString(startString)) {
				errorLabel.setText("Neispravan format početnog datuma");
				return;
			}
			
			else if(!RegistrationValidator.isValidDateString(endString)) {
				errorLabel.setText("Neispravan format krajnjeg datuma");
				return;
			}
			
			start = (LocalDate.parse(startString, DateTimeFormatter.ofPattern("dd.MM.yyyy."))).atStartOfDay();
			end = (LocalDate.parse(endString, DateTimeFormatter.ofPattern("dd.MM.yyyy."))).atStartOfDay();
			
			if (end.isBefore(start)) {
				errorLabel.setText("Neispravan unos datuma!");
				return;
			}
			izvestajTable.setModel(new MyTableModel());
    		MultiLineTableCellRenderer.resizeColumnWidth(izvestajTable);
    		MultiLineTableCellRenderer.resizeColumnHeight(izvestajTable);
        	scrollPane.revalidate();
        	scrollPane.repaint();
        	SekretarIzvestajZahteviPanel.this.revalidate();
        	SekretarIzvestajZahteviPanel.this.repaint();	
		}
		
	}	
	
	public Object[][] getTableContent(){
		int numOfRow = 0;
		Object[][] retVal;
		
		ArrayList<Zahtev> zahtevi = new ArrayList<Zahtev>();
		for (Zahtev z: dh.getZahtevi()) {
			if (z.getDatumPrijema().isAfter(start) && z.getDatumPrijema().isBefore(end) && z.getStanje()!=StanjeZahteva.KREIRAN) {
				if(flags[0] == flags[1] && flags[0] == flags[2])
					zahtevi.add(z);
				else if (flags[0] && !flags[1] && !flags[2]) {
					if (z.getKurs().getJezik() == Jezik.ENGLESKI)
						zahtevi.add(z);
				}
				else if (flags[1] && !flags[0] && !flags[2]) {
					if (z.getKurs().getJezik() == Jezik.NEMACKI)
						zahtevi.add(z);
				}
				else if (flags[2] && !flags[0] && !flags[1]) {
					if (z.getKurs().getJezik() == Jezik.FRANCUSKI)
						zahtevi.add(z);
				}
				else if (flags[1] && flags[0] && !flags[2]) {
					if (z.getKurs().getJezik() != Jezik.FRANCUSKI)
						zahtevi.add(z);
				}
				else if (!flags[1] && flags[0] && flags[2]) {
					if (z.getKurs().getJezik() != Jezik.NEMACKI)
						zahtevi.add(z);
				}
				else if (flags[1] && !flags[0] && flags[2]) {
					if (z.getKurs().getJezik() != Jezik.ENGLESKI)
						zahtevi.add(z);
				}
			}
		
		}
		
		retVal = new Object[zahtevi.size()][4];
		for(Zahtev z: zahtevi) {
			retVal[numOfRow][0] = z.getUcenik().getIme() + " " + z.getUcenik().getPrezime();
			retVal[numOfRow][1] = z.getKurs().getNaziv();
			retVal[numOfRow][2] = z.getDatumPrijema().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm"));
			retVal[numOfRow][3] = z.getStanje().name();
			numOfRow++;
		}
		if(zahtevi.size() == 0)
			JOptionPane.showMessageDialog(null, "Nije pronađen zahtev koji ispunjava parametre", "Greška", JOptionPane.ERROR_MESSAGE);
		return retVal;
	}
	
	class MyTableModel extends AbstractTableModel {
	    private String[] columnNames = {"Učenik", "Kurs", "Datum prispeća", "Status"};
	    private Object[][] data = SekretarIzvestajZahteviPanel.this.getTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}
}
