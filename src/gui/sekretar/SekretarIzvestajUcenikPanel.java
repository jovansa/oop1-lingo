package gui.sekretar;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import entities.managers.DataHandler;
import entities.users.Korisnik;
import entities.users.Ucenik;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import gui.customcomponents.MultiLineTableCellRenderer;

@SuppressWarnings("serial")
public class SekretarIzvestajUcenikPanel extends JPanel implements ActionListener {
	
	
	private DataHandler dh;
	
	JScrollPane testoviPanel;
	
	CustomTableFixed uceniciTable;
	
	CustomButton povratakButton;

	private JLabel brKursLabel;
	private JLabel iznosLabel;
	private JLabel adrLabel;
	private JLabel datumLabel;
	private JLabel imeprezLabel;
	
	public SekretarIzvestajUcenikPanel(DataHandler dh) {
		this.dh = dh;
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Izveštaj o učeniku");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		titleLabel.setBounds(365, 15, 244, 52);
		add(titleLabel);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(568, 374, 338, 77);
		povratakButton.addActionListener(this);
		add(povratakButton);
		
		uceniciTable = new CustomTableFixed();
		uceniciTable.setModel(new MyTableModel());
		uceniciTable.setRowSelectionAllowed(true);
		uceniciTable.setRowHeight(40);
		uceniciTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		MultiLineTableCellRenderer.resizeColumnWidth(uceniciTable);
		MultiLineTableCellRenderer.resizeColumnHeight(uceniciTable);
		uceniciTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	Ucenik u =  (Ucenik)dh.getKorisnikFromJmbg((String)uceniciTable.getValueAt(uceniciTable.getSelectedRow(), 0));
	        	imeprezLabel.setText(u.getIme() + " " + u.getPrezime());
	        	datumLabel.setText(u.getDatumRodjena().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
	        	adrLabel.setText(u.getAdresa());
	        	brKursLabel.setText(dh.getBrKursevaForUcenik(u).toString());
	        	iznosLabel.setText(u.getPlatnaKartica().getTotalSpent().toString());
	        }
	    });
		
		testoviPanel = new JScrollPane(uceniciTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		testoviPanel.getViewport().setBackground(new Color(255, 209, 209));
		testoviPanel.setBounds(10, 96, 449, 355);
		this.add(testoviPanel);
		
		JLabel fullnameLabel = new JLabel("Ime i prezime:");
		fullnameLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		fullnameLabel.setBounds(575, 120, 130, 34);
		add(fullnameLabel);
		
		JLabel dobLabel = new JLabel("Datum rođenja:");
		dobLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		dobLabel.setBounds(564, 165, 141, 34);
		add(dobLabel);
		
		JLabel adresaLabel = new JLabel("Adresa stanovanja:");
		adresaLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		adresaLabel.setBounds(533, 210, 172, 34);
		add(adresaLabel);
		
		JLabel kursNumLabel = new JLabel("Broj pohađanih kurseva:");
		kursNumLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		kursNumLabel.setBounds(485, 255, 220, 34);
		add(kursNumLabel);
		
		JLabel financeLabel = new JLabel("Ukupan uplaćen iznos:");
		financeLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		financeLabel.setBounds(499, 300, 206, 34);
		add(financeLabel);
		
		imeprezLabel = new JLabel("");
		imeprezLabel.setForeground(Color.DARK_GRAY);
		imeprezLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		imeprezLabel.setBounds(715, 120, 249, 34);
		add(imeprezLabel);
		
		datumLabel = new JLabel("");
		datumLabel.setForeground(Color.DARK_GRAY);
		datumLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		datumLabel.setBounds(715, 165, 249, 34);
		add(datumLabel);
		
		adrLabel = new JLabel("");
		adrLabel.setForeground(Color.DARK_GRAY);
		adrLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		adrLabel.setBounds(715, 210, 249, 34);
		add(adrLabel);
		
		brKursLabel = new JLabel("");
		brKursLabel.setForeground(Color.DARK_GRAY);
		brKursLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		brKursLabel.setBounds(715, 255, 249, 34);
		add(brKursLabel);
		
		iznosLabel = new JLabel("");
		iznosLabel.setForeground(Color.DARK_GRAY);
		iznosLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		iznosLabel.setBounds(715, 300, 249, 34);
		add(iznosLabel);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			SekretarLoggedFrame topFrame = (SekretarLoggedFrame) SwingUtilities.getWindowAncestor(SekretarIzvestajUcenikPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	}
	
	public Object[][] getTableContent(){
		int numOfRow = 0;
		Object[][] retVal;
		
		ArrayList<Ucenik> ucenici = new ArrayList<Ucenik>();
		for (Korisnik k: dh.getKorisnici()) {
			if (k instanceof Ucenik)
				ucenici.add((Ucenik) k);
		}
		
		retVal = new Object[ucenici.size()][2];
		for(Ucenik u : ucenici){
			retVal[numOfRow][0] = u.getJmbg();
			retVal[numOfRow][1] = u.getIme() + " " +u.getPrezime();
			numOfRow++;
		}
		return retVal;
		
	}
	
	class MyTableModel extends AbstractTableModel {
	    private String[] columnNames = {"JMBG","Ime i prezime"};
	    private Object[][] data = SekretarIzvestajUcenikPanel.this.getTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	    
	    
	}
	
}