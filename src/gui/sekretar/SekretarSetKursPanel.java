package gui.sekretar;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.other.Kurs;
import entities.users.Korisnik;
import entities.users.Predavac;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import gui.customcomponents.MultiLineTableCellRenderer;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;


@SuppressWarnings("serial")
public class SekretarSetKursPanel extends JPanel implements ActionListener {

	private DataHandler dh;
	private JTextField dodajKursField;
	
	CustomButton povratakButton;
	CustomButton dodajKursButton;
	
	CustomTableFixed table;
	
	JLabel errorLabel;
	
	public SekretarSetKursPanel(DataHandler dh) {
		this.dh = dh;
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Postavljanje predavača na kurseve");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		titleLabel.setBounds(253, 15, 468, 52);
		add(titleLabel);
		
		
		table = new CustomTableFixed();
		table.setRowSelectionAllowed(true);
		table.setRowHeight(60);
		table.setModel(new MyTableModel());
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setSelectionBackground(new Color(255, 168, 168));
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	if (errorLabel.getText().equals("Niste odabrali predavača!"))
	        		errorLabel.setText("");
	        	
	        	if (((String[])table.getValueAt(table.getSelectedRow(), 5)).length == 0)
	        		errorLabel.setText("<html>Odabran predavač ne može da predaje ni na jednom aktuelnom kursu!</html>");
	        	else
	        		errorLabel.setText("");
	        }
	    });
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		MultiLineTableCellRenderer renderer = new MultiLineTableCellRenderer();
		
		table.getColumnModel().getColumn(3).setCellRenderer(renderer);
		table.getColumnModel().getColumn(4).setCellRenderer(renderer);
		table.getColumnModel().getColumn(5).setCellRenderer(renderer);
		MultiLineTableCellRenderer.resizeColumnWidth(table);
		MultiLineTableCellRenderer.resizeColumnHeight(table);
		
		JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBounds(59, 80, 662, 377);
		scrollPane.getViewport().setBackground(new Color(255, 209, 209));
		this.add(scrollPane);
		
		
		JLabel dodajKursLabel = new JLabel("<html>Odaberite jednog predavača<br /> i unesite ID dostupnog kursa<br /> na koji želite da ga postavite:</html>");
		dodajKursLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		dodajKursLabel.setBounds(731, 105, 219, 82);
		add(dodajKursLabel);
		
		dodajKursField = new JTextField();
		dodajKursField.setBounds(731, 198, 219, 27);
		add(dodajKursField);
		dodajKursField.setColumns(10);
		
		dodajKursButton = new CustomButton("Potvrdi");
		dodajKursButton.setBounds(731, 312, 219, 60);
		dodajKursButton.addActionListener(this);
		add(dodajKursButton);
		
		errorLabel = new JLabel("");
		errorLabel.setForeground(Color.RED);
		errorLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		errorLabel.setBounds(731, 236, 219, 65);
		add(errorLabel);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(731, 397, 219, 60);
		povratakButton.addActionListener(this);
		add(povratakButton);
	
	
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			SekretarLoggedFrame topFrame = (SekretarLoggedFrame) SwingUtilities.getWindowAncestor(SekretarSetKursPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
		
		else if (e.getSource() == dodajKursButton) {
			errorLabel.setText("");
			if (table.getSelectionModel().isSelectionEmpty()) {
				errorLabel.setText("Niste odabrali predavača!");
				return;
			}
			
			String odabranKurs = dodajKursField.getText();
			String jmbgPred = (String) table.getValueAt(table.getSelectedRow(), 0);
			
			Predavac predavac = (Predavac) dh.getKorisnikFromJmbg(jmbgPred);
		
			String[] raspoloziviKursevi = (String[]) table.getValueAt(table.getSelectedRow(), 5);
			
			Kurs kurs = null;
			for (String k: raspoloziviKursevi) {
				if (k.substring(0, 4).contains(odabranKurs) && odabranKurs.length() == 4) {
					kurs = dh.getKursFromId(odabranKurs);
				}
			}
			if (kurs == null) {
				errorLabel.setText("Niste uneli ispravan ID kursa!");
				return;
			}
			
			kurs.addPredavac(predavac);
			dh.writeKurseviToFile(DataLoad.kurseviPath);
			
			JOptionPane.showMessageDialog(null, "Dodela kursa uspešna!", "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			SekretarLoggedFrame topFrame = (SekretarLoggedFrame) SwingUtilities.getWindowAncestor(SekretarSetKursPanel.this);
			topFrame.getLogoLabel().setVisible(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
			
		}
	}

	public Object[][] getTableContent(){
		int numOfRow = 0;
		Object[][] retVal;
		
		ArrayList<Predavac> lista = new ArrayList<Predavac>();
		for (Korisnik k : dh.getKorisnici()) {
			if (k instanceof Predavac)
				lista.add((Predavac) k);
		}
		
		retVal = new Object[lista.size()][6];
		
		for (Predavac p : lista) {
			retVal[numOfRow][0] = p.getJmbg();
			retVal[numOfRow][1] = p.getIme();
			retVal[numOfRow][2] = p.getPrezime();
			
			String[] jezici = new String[p.getJezici().size()];
			jezici = p.getJeziciString().split(",");
			retVal[numOfRow][3] = jezici;
			
			ArrayList<Kurs> kurseviList = dh.getKurseviForPredavac(p);
			String[] kursevi = new String[kurseviList.size()];
			for (int i = 0; i < kurseviList.size(); i++)
				kursevi[i] = kurseviList.get(i).getId()+ " - " + kurseviList.get(i).getNaziv();
		
			ArrayList<Kurs> dostupniList = dh.getDostupniKurseviForPredavac(p);
			String[] dostupni = new String[dostupniList.size()];
			for (int i = 0; i < dostupniList.size(); i++)
				dostupni[i] = dostupniList.get(i).getId() + " - " + dostupniList.get(i).getNaziv();
		
			retVal[numOfRow][4] = kursevi;
			retVal[numOfRow][5] = dostupni;
			numOfRow++;
		}
		return retVal;
	}
	
	class MyTableModel extends AbstractTableModel {
	    private String[] columnNames = {"JMBG", "Ime", "Prezime", "Jezici", "Predaje na", "Može da predaje"};
	    private Object[][] data = SekretarSetKursPanel.this.getTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}
}







