package gui.sekretar;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JPanel;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.other.Kurs;
import entities.other.StanjeZahteva;
import entities.other.Zahtev;
import entities.users.Korisnik;
import entities.users.Sekretar;
import entities.users.Ucenik;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import gui.customcomponents.MultiLineTableCellRenderer;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.JRadioButton;

@SuppressWarnings("serial")
public class SekretarObradaZahtevaPanel extends JPanel implements ActionListener {
	
	DataHandler dh;
	private JTextField jmbgField;
	
	CustomButton povratakButton;
	CustomButton potvrdiButton;
	
	JLabel errorLabel;
	
	JTable zahtevTable;
	JTable kursTable;
	
	JRadioButton obradaButton;
	JRadioButton prihvatiButton;
	JRadioButton odbijButton;
	
	public SekretarObradaZahtevaPanel(DataHandler dh) {
		this.dh = dh;
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Obrada zahteva za upis na kurs");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 30));
		titleLabel.setBounds(274, 15, 426, 50);
		add(titleLabel);
		
		JLabel subtitleLabel = new JLabel("Pregled zahteva");
		subtitleLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		subtitleLabel.setBounds(156, 76, 175, 48);
		add(subtitleLabel);
		
		JLabel jmbgLabel = new JLabel("JMBG polaznika:");
		jmbgLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		jmbgLabel.setBounds(66, 432, 154, 28);
		add(jmbgLabel);
		
		jmbgField = new JTextField();
		jmbgField.setBounds(230, 432, 201, 28);
		add(jmbgField);
		jmbgField.setColumns(10);
		
		JLabel lblPregledKurseva = new JLabel("Pregled kurseva");
		lblPregledKurseva.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		lblPregledKurseva.setBounds(643, 76, 175, 48);
		add(lblPregledKurseva);
		
		JLabel instructionLabel = new JLabel("<html>Odaberite sve kurseve za koje želite da kreirate zahtev za unetog polaznika iz tabele kurseva. Označavanjem zahteva, podaci će biti automatski uneti.</html>");
		instructionLabel.setForeground(Color.DARK_GRAY);
		instructionLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
		instructionLabel.setBounds(66, 317, 855, 81);
		add(instructionLabel);
		
		potvrdiButton = new CustomButton("Potvrdi");
		potvrdiButton.setBounds(643, 395, 222, 41);
		potvrdiButton.addActionListener(this);
		add(potvrdiButton);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(643, 442, 222, 41);
		povratakButton.addActionListener(this);
		add(povratakButton);
		
		errorLabel = new JLabel("");
		errorLabel.setForeground(Color.RED);
		errorLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		errorLabel.setBounds(643, 362, 246, 28);
		add(errorLabel);
	
		zahtevTable = new CustomTableFixed();
		zahtevTable.setRowSelectionAllowed(true);
		zahtevTable.setRowHeight(30);
		zahtevTable.setModel(new MyZahtevTableModel());
		zahtevTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		zahtevTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		MultiLineTableCellRenderer.resizeColumnWidth(zahtevTable);
		zahtevTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	jmbgField.setText((String)zahtevTable.getValueAt(zahtevTable.getSelectedRow(), 1));
	        	if (zahtevTable.getValueAt(zahtevTable.getSelectedRow(), 5).equals("U OBRADI")) {
	        		obradaButton.setEnabled(false);
	        	}
	        	else if (zahtevTable.getValueAt(zahtevTable.getSelectedRow(), 5).equals("KREIRAN")) {
	        		obradaButton.setEnabled(true);
	        	}
	        	zahtevTable.setSelectionBackground(new Color(255, 168, 168));
	        	kursTable.clearSelection();
	        	kursTable.setSelectionBackground(new Color(255, 209, 209));
	        	odbijButton.setEnabled(true);
	        	prihvatiButton.setText("Prihvati");
	        }
	    });
		
		
		JScrollPane zahtevScrollPane = new JScrollPane(zahtevTable);
		zahtevScrollPane.setBounds(23, 130, 441, 167);
		zahtevScrollPane.getViewport().setBackground(new Color(255, 209, 209));
		add(zahtevScrollPane);
		
		kursTable = new CustomTableFixed(){
		    @Override
		    public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
		        super.changeSelection(rowIndex, columnIndex, true, extend);
		    }
		};;
		kursTable.setRowSelectionAllowed(true);
		kursTable.setRowHeight(30);
		kursTable.setModel(new MyKursTableModel());
		kursTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		kursTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		MultiLineTableCellRenderer.resizeColumnWidth(kursTable);
		kursTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	zahtevTable.setSelectionBackground(new Color(255, 209, 209));
	        	kursTable.setSelectionBackground(new Color(255, 168, 168));
	        	odbijButton.setEnabled(false);
	        	obradaButton.setEnabled(true);
	        	jmbgField.setText("");
	        	prihvatiButton.setText("Dodaj na kurs");
	        }
	    });
		
		MultiLineTableCellRenderer renderer = new MultiLineTableCellRenderer();
		kursTable.getColumnModel().getColumn(3).setCellRenderer(renderer);
		MultiLineTableCellRenderer.resizeColumnHeight(kursTable);
		MultiLineTableCellRenderer.resizeColumnWidth(kursTable);
		
		JScrollPane kursScrollPane = new JScrollPane(kursTable);
		kursScrollPane.setBounds(510, 130, 441, 167);
		kursScrollPane.getViewport().setBackground(new Color(255, 209, 209));
		add(kursScrollPane);
		
		
		
		obradaButton = new JRadioButton("Stavi u obradu");
		obradaButton.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		obradaButton.setBounds(492, 405, 117, 23);
		obradaButton.setBackground(new Color(255, 168, 168));
		add(obradaButton);
		
		prihvatiButton = new JRadioButton("Prihvati");
		prihvatiButton.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		prihvatiButton.setBackground(new Color(255, 168, 168));
		prihvatiButton.setBounds(492, 433, 117, 23);
		prihvatiButton.setSelected(true);
		add(prihvatiButton);
		
		odbijButton = new JRadioButton("Odbij");
		odbijButton.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		odbijButton.setBounds(492, 461, 117, 23);
		odbijButton.setBackground(new Color(255, 168, 168));
		add(odbijButton);
	
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(obradaButton);
		buttonGroup.add(prihvatiButton);
		buttonGroup.add(odbijButton);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			SekretarLoggedFrame topFrame = (SekretarLoggedFrame) SwingUtilities.getWindowAncestor(SekretarObradaZahtevaPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
		
		else if (e.getSource() == potvrdiButton) {
			errorLabel.setText("");
			boolean flag = false;
			for (Korisnik k : dh.getKorisnici()) {
				if ((dh.getKorisnikFromJmbg(jmbgField.getText()) instanceof Ucenik) 
						&& jmbgField.getText().equals(k.getJmbg())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				errorLabel.setText("Uneli ste neispravan JMBG");
				return;
			}
				
			if (zahtevTable.getSelectionBackground().equals(new Color(255, 168, 168))) {
				Zahtev odabranZahtev = dh.getZahtevFromId((String) zahtevTable.getValueAt(zahtevTable.getSelectedRow(), 0));
				odabranZahtev.setSekretar((Sekretar) dh.getCurrentlyLogged());
				dh.getZahtevi().remove(odabranZahtev);
				if(obradaButton.isSelected())
					odabranZahtev.setStanje(StanjeZahteva.U_OBRADI);
				else if (prihvatiButton.isSelected())
					odabranZahtev.setStanje(StanjeZahteva.PRIHVACEN);
				else if (odbijButton.isSelected())
					odabranZahtev.setStanje(StanjeZahteva.ODBIJEN);
				
				if(odabranZahtev.getStanje() == StanjeZahteva.PRIHVACEN) {
					odabranZahtev.getKurs().addUcenik(odabranZahtev.getUcenik());
					odabranZahtev.getUcenik().getPlatnaKartica().getPlacanja().put(
							LocalDateTime.now(), dh.getCurrentCenovnik().getCene().get(odabranZahtev.getKurs()));
					dh.getZahtevi().add(odabranZahtev);
					dh.writeKurseviToFile(DataLoad.kurseviPath);
					dh.writeKarticeToFile(DataLoad.karticePath);
					dh.writeZahteviToFile(DataLoad.zahteviPath);
				}
				
				else {
					if(odabranZahtev.getStanje() == StanjeZahteva.U_OBRADI)
						odabranZahtev.setSekretar(null);
					dh.getZahtevi().add(odabranZahtev);
					dh.writeZahteviToFile(DataLoad.zahteviPath);
				}
			}
			else if (kursTable.getSelectionBackground().equals(new Color(255, 168, 168))) {
				String jmbg = jmbgField.getText();
				int[] selection = kursTable.getSelectedRows();
				if (selection.length == 0) {
					errorLabel.setText("Niste odabrali kurs!");
					return;
				}
			
				ArrayList<Kurs> kursevi = new ArrayList<Kurs>();
				for (int i : selection) {
					Kurs kurs = dh.getKursFromId((String) kursTable.getValueAt(i, 0));
					
					if (kurs.getPolaznici().contains(dh.getKorisnikFromJmbg(jmbg))) {
						errorLabel.setText("Polaznik već pohađa kurs: " + kurs.getId());
						return;
					}
					else
						kursevi.add(kurs);
				}
				
				if (obradaButton.isSelected()) {
					for (Kurs kur: kursevi) {
						Zahtev zahtev = new Zahtev(dh.generateZahtevId(), (Ucenik)dh.getKorisnikFromJmbg(jmbg),
								kur, StanjeZahteva.U_OBRADI, LocalDateTime.now(), null);
						dh.getZahtevi().add(zahtev);
					}
					dh.writeZahteviToFile(DataLoad.zahteviPath);
				}
				
				else if(prihvatiButton.isSelected()) {
					for (Kurs kur: kursevi) {
						Zahtev zahtev = new Zahtev(dh.generateZahtevId(), (Ucenik)dh.getKorisnikFromJmbg(jmbg),
								kur, StanjeZahteva.PRIHVACEN, LocalDateTime.now(), (Sekretar)dh.getCurrentlyLogged());
						dh.getZahtevi().add(zahtev);
						dh.writeZahteviToFile(DataLoad.zahteviPath);
						kur.addUcenik((Ucenik) dh.getKorisnikFromJmbg(jmbg));
						Ucenik uc = (Ucenik)dh.getKorisnikFromJmbg(jmbg);
						uc.getPlatnaKartica().getPlacanja().put(LocalDateTime.now().plusMinutes(kursevi.indexOf(kur) * 2),
								dh.getCurrentCenovnik().getCene().get(kur));
						
					}
					dh.writeZahteviToFile(DataLoad.zahteviPath);
					dh.writeKurseviToFile(DataLoad.kurseviPath);
					dh.writeKarticeToFile(DataLoad.karticePath);
				}					
				}
			JOptionPane.showMessageDialog(null, "Obrada zahteva uspešna!", "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			SekretarLoggedFrame topFrame = (SekretarLoggedFrame) SwingUtilities.getWindowAncestor(SekretarObradaZahtevaPanel.this);
			topFrame.getLogoLabel().setVisible(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);		
		}
	}
	
	public Object[][] getZahtevTableContent() {
		int numOfRow = 0;
		Object[][] retVal;
		
		ArrayList<Zahtev> lista = new ArrayList<Zahtev>();
		for (Zahtev z : dh.getZahtevi()) {
			if (z.getStanje() == StanjeZahteva.KREIRAN || z.getStanje() == StanjeZahteva.U_OBRADI)
				lista.add(z);
		}
		
		retVal = new Object[lista.size()][6];
		
		for (Zahtev z : lista) {
			retVal[numOfRow][0] = z.getId();
			retVal[numOfRow][2] = z.getUcenik().getIme() + " " + z.getUcenik().getPrezime();
			retVal[numOfRow][1] = z.getUcenik().getJmbg();
			retVal[numOfRow][3] = z.getKurs().getId() + " - " + z.getKurs().getNaziv();
			retVal[numOfRow][4] = z.getDatumPrijema().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm"));
			retVal[numOfRow][5] = z.getStanje().name().replace("_", " ");
			numOfRow++;
		}
		return retVal;
	}
	
	class MyZahtevTableModel extends AbstractTableModel {
	    private String[] columnNames = {"ID Zahteva", "JMBG", "Ime i prezime", "Kurs", "Datum prispeća", "Status"};
	    private Object[][] data = SekretarObradaZahtevaPanel.this.getZahtevTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}
		
	public Object[][] getKursTableContent() {
		int numOfRow = 0;
		Object[][] retVal;
		
		ArrayList<Kurs> lista = new ArrayList<Kurs>();
		for (Kurs k : dh.getKursevi()) {
			if (k.getKraj().isAfter(LocalDate.now()))
				lista.add(k);
		}
		
		retVal = new Object[lista.size()][4];
		
		for (Kurs k : lista) {
			String[] predavaci = new String[k.getPredavaci().size()];
			for (int i = 0; i < k.getPredavaci().size(); i++)
				predavaci[i] = k.getPredavaci().get(i).getIme() + " " + k.getPredavaci().get(i).getPrezime();
			
			retVal[numOfRow][0] = k.getId();
			retVal[numOfRow][1] = k.getNaziv();
			retVal[numOfRow][2] = ((Integer)k.getPolaznici().size()).toString();
			retVal[numOfRow][3] = predavaci;
			numOfRow++;
		}
		return retVal;
	}
	
	class MyKursTableModel extends AbstractTableModel {
	    private String[] columnNames = {"ID Kursa", "Naziv", "Broj polaznika", "Predavači"};
	    private Object[][] data = SekretarObradaZahtevaPanel.this.getKursTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}

}
