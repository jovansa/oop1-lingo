package gui.izvestaji;

import javax.swing.JFrame;

import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.demo.charts.ExampleChart;
import org.knowm.xchart.style.markers.SeriesMarkers;

import entities.managers.DataHandler;

public class PrihodiRashodiLine implements ExampleChart<XYChart> {
	DataHandler dh;

	public PrihodiRashodiLine(DataHandler data) {
		this.dh = data;
	}
	
	public void prihodiRashodiIzvestaj(DataHandler dh) {
	    ExampleChart<XYChart> exampleChart = new PrihodiRashodiLine(dh);
	    XYChart chart = exampleChart.getChart();
	    new SwingWrapper<XYChart>(chart).displayChart().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	@Override
	public XYChart getChart() {
		XYChart chart =  new XYChartBuilder().width(800).height(600).build();
	    chart.setTitle("Prihodi i rashodi po datumima");
	    chart.setXAxisTitle("Mesec");
	    chart.setYAxisTitle("Novac");
	    chart.getStyler().setYAxisDecimalPattern("RSD #,###,###.##");
	    double[] prihodiData = dh.getPrihodiForYear();
	    double[] rashodiData = dh.getRashodiForYear();
	    XYSeries prihodi = 	chart.addSeries("Prihodi", prihodiData);
	    XYSeries rashodi = chart.addSeries("Rashodi", rashodiData);
	    prihodi.setMarker(SeriesMarkers.CIRCLE);
	    rashodi.setMarker(SeriesMarkers.CIRCLE);
	    return chart;
	}

	@Override
	public String getExampleChartName() {
		// TODO Auto-generated method stub
		return null;
	}
}