package gui.izvestaji;

import java.util.HashMap;

import javax.swing.JFrame;

import org.knowm.xchart.PieChart;
import org.knowm.xchart.PieChartBuilder;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.demo.charts.ExampleChart;
import org.knowm.xchart.style.PieStyler.AnnotationType;

import entities.managers.DataHandler;
import entities.other.Jezik;

public class UceniciPoJeziku implements ExampleChart<PieChart> {
	DataHandler dh;

	public UceniciPoJeziku(DataHandler data) {
		this.dh = data;
	}
	
	public void uceniciIzvestaj(DataHandler dh) {
		ExampleChart<PieChart> exampleChart = new UceniciPoJeziku(dh);
	    PieChart chart = exampleChart.getChart();
	    new SwingWrapper<PieChart>(chart).displayChart().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);;
	}
	public void uceniciIzvestaj(DataHandler dh, boolean num) {
		ExampleChart<PieChart> exampleChart = new UceniciPoJeziku(dh);
		PieChart chart = exampleChart.getChart();
		if(num)
			chart.getStyler().setAnnotationType(AnnotationType.Value);
	    new SwingWrapper<PieChart>(chart).displayChart().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);;
	}
	  @Override
	  public PieChart getChart() {
	 
	    PieChart chart = new PieChartBuilder().width(800).height(600).title(getClass().getSimpleName()).build();
	    
	    HashMap<Jezik, Integer> mapa = dh.getUceniciPoJeziku();

	    for (Jezik j : mapa.keySet())
	    	chart.addSeries(j.name(), mapa.get(j));
	    
	 
	    return chart;
	  }
	  
	@Override
	public String getExampleChartName() {
		
		return null;
	}
}