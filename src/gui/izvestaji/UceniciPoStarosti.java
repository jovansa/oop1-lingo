package gui.izvestaji;

import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;

import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.demo.charts.ExampleChart;
import org.knowm.xchart.style.Styler.LegendPosition;

import entities.managers.DataHandler;

public class UceniciPoStarosti implements ExampleChart<CategoryChart> {
	DataHandler dh;
	
	public UceniciPoStarosti(DataHandler dh) {
		this.dh = dh;
	}
	
	public void uceniciIzvestaj(DataHandler dh) {
	    ExampleChart<CategoryChart> exampleChart = new UceniciPoStarosti(dh);
	    CategoryChart chart = exampleChart.getChart();
	    new SwingWrapper<CategoryChart>(chart).displayChart().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);;
	}
	
	@Override
	  public CategoryChart getChart() {
	    CategoryChart chart = new CategoryChartBuilder().width(800).height(600).title("Učenici po starosti").xAxisTitle("Rang godina").yAxisTitle("Broj učenika").build();
	 
	    chart.getStyler().setLegendPosition(LegendPosition.InsideNE);
	    chart.getStyler().setHasAnnotations(false);
	 
	    Integer[] brPoGod = dh.getUceniciPoGodinama();
	    List<Integer> list = Arrays.asList(brPoGod);
	    chart.addSeries("Broj učenika", Arrays.asList(new String[] {"<20", "20-30", ">30"}), list);
	    chart.getStyler().setYAxisTickMarkSpacingHint(100);
	    return chart;
	  }

	@Override
	public String getExampleChartName() {
		// TODO Auto-generated method stub
		return null;
	}
}
