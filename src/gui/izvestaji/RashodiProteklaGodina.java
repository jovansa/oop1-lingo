package gui.izvestaji;

import java.time.LocalDate;

import javax.swing.JFrame;

import org.knowm.xchart.PieChart;
import org.knowm.xchart.PieChartBuilder;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.demo.charts.ExampleChart;

import entities.managers.DataHandler;

public class RashodiProteklaGodina implements ExampleChart<PieChart> {
	DataHandler dh;

	public RashodiProteklaGodina(DataHandler data) {
		this.dh = data;
	}
	
	public void rashodiLastYear(DataHandler dh) {
		ExampleChart<PieChart> exampleChart = new RashodiProteklaGodina(dh);
	    PieChart chart = exampleChart.getChart();
	    new SwingWrapper<PieChart>(chart).displayChart().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);;
	}
	
	  @Override
	  public PieChart getChart() {
	 
	    PieChart chart = new PieChartBuilder().width(800).height(600).title(getClass().getSimpleName()).build();
	 
	    double[] rashodi = dh.getRashodiForYear();
	    for (int i = 0; i < 12; i++)
	    	chart.addSeries(LocalDate.now().minusMonths(i).getMonth().name(), rashodi[i]);
	    
	 
	    return chart;
	  }
	  
	@Override
	public String getExampleChartName() {
		
		return null;
	}
}
