package gui.predavac;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.other.Test;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import gui.customcomponents.MultiLineTableCellRenderer;
import utilities.RegistrationValidator;


@SuppressWarnings("serial")
public class PredavacEditPanel extends JPanel implements ActionListener{
	DataHandler dh;
	
	JScrollPane izborPanel;
	JScrollPane editPanel;
	
	CustomTableFixed izborTable;
	CustomTableFixed editTable;
	
	CustomButton potvrdiButton;
	CustomButton povratakButton;
	
	Test selectedTest;
	private JLabel infoLabel;
	public PredavacEditPanel(DataHandler dh) {
		this.dh = dh;

		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Izmena testa");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		titleLabel.setBounds(400, 15, 175, 52);
		add(titleLabel);
		
		izborTable = new CustomTableFixed();
		izborTable.setModel(new MyIzborTableModel());
		izborTable.setRowSelectionAllowed(true);
		izborTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		izborTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		MultiLineTableCellRenderer.resizeColumnWidth(izborTable);
		MultiLineTableCellRenderer.resizeColumnHeight(izborTable);
		izborTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	selectedTest = dh.getTestFromId((String) izborTable.getValueAt(izborTable.getSelectedRow(), 0));
	    		editTable.setModel(new MyEditTableModel());
	    		editTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	    		MultiLineTableCellRenderer.resizeColumnWidth(editTable);
	    		MultiLineTableCellRenderer.resizeColumnHeight(editTable);
	        	editPanel.revalidate();
	        	editPanel.repaint();
	        	PredavacEditPanel.this.revalidate();
	        	PredavacEditPanel.this.repaint();
	        }
	    });	
		
		editTable = new CustomTableFixed();
		editTable.setRowSelectionAllowed(true);
		
		potvrdiButton = new CustomButton("Potvrdi");
		potvrdiButton.setBounds(10, 374, 219, 77);
		potvrdiButton.addActionListener(this);
		add(potvrdiButton);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(240, 374, 219, 77);
		povratakButton.addActionListener(this);
		add(povratakButton);
		
		izborPanel = new JScrollPane(izborTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		izborPanel.getViewport().setBackground(new Color(255, 209, 209));
		izborPanel.setBounds(10, 96, 449, 267);
		add(izborPanel);
		
		editPanel = new JScrollPane(editTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		editPanel.setBounds(515, 96, 449, 267);
		editPanel.getViewport().setBackground(new Color(255, 209, 209));
		add(editPanel);
		
		infoLabel = new JLabel("Zasebna pitanja odvajaju se znakom \";\"");
		infoLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		infoLabel.setForeground(Color.DARK_GRAY);
		infoLabel.setBounds(515, 374, 449, 28);
		add(infoLabel);
	
	
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			PredavacLoggedFrame topFrame = (PredavacLoggedFrame) SwingUtilities.getWindowAncestor(PredavacEditPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
		
		else if (e.getSource() == potvrdiButton) {
			selectedTest.setNaziv((String) editTable.getValueAt(0, 0));
			selectedTest.setAktivanOd(LocalDateTime.parse((String)editTable.getValueAt(0, 1), DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")));
			selectedTest.setAktivanDo(LocalDateTime.parse((String)editTable.getValueAt(0, 2), DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")));
			selectedTest.setMaxBrojbodova((Double) editTable.getValueAt(0, 3));
			
			ArrayList<String> pitanja = new ArrayList<String>();
			for (String s : ((String)editTable.getValueAt(0, 4)).split(";"))
				pitanja.add(s);
			selectedTest.setPitanja(pitanja);
		
			dh.writeTestToFile(DataLoad.testoviPath);
			JOptionPane.showMessageDialog(null, "Test izmenjen uspešno!", "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			PredavacLoggedFrame topFrame = (PredavacLoggedFrame) SwingUtilities.getWindowAncestor(PredavacEditPanel.this);
			topFrame.getLogoLabel().setVisible(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
		
	}
	
	public Object[][] getIzborTableContent(){
		int numOfRow = 0;
		ArrayList<Test> podobniTestovi = new ArrayList<Test>();
		
		for (Test t: dh.getTestovi()) {
			if(t.getAutor().getJmbg().equals(dh.getCurrentlyLogged().getJmbg())) {
				if (t.getResenja().isEmpty() || t.getResenja() == null) {
					if(t.getAktivanDo().isAfter(LocalDateTime.now())) 
						podobniTestovi.add(t);
				}
			}
		}
		
		Object[][] retVal = new Object[dh.getTestovi().size()][3];
		for(Test t:podobniTestovi) {
			retVal[numOfRow][0] = t.getId();
			retVal[numOfRow][1] = t.getNaziv();
			retVal[numOfRow][2] = t.getAktivanOd().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm"));
			numOfRow++;
		}
		return retVal;
		
	}
	
	public Object[][] getEditTableContent(){
		Object[][] retVal = new Object[1][5];
		retVal[0][0] = selectedTest.getNaziv();
		retVal[0][1] = selectedTest.getAktivanOd().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. hh:MM"));
		retVal[0][2] = selectedTest.getAktivanDo().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. hh:MM"));;
		retVal[0][3] = selectedTest.getMaxBrojbodova();
		retVal[0][4] = selectedTest.getPitanjaString();
		return retVal;
		
	}
	
	class MyIzborTableModel extends AbstractTableModel {
	    private String[] columnNames = {"ID Testa" ,"Naziv Testa", "Datum Početka"};
	    private Object[][] data = PredavacEditPanel.this.getIzborTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}

	class MyEditTableModel extends AbstractTableModel {
	    private String[] columnNames = {"Naziv Testa" ,"Početak", "Kraj", "Max Bodova", "Pitanja"};
	    private Object[][] data = PredavacEditPanel.this.getEditTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    
	    public void setValueAt(Object value, int row, int col) {
	        if (col == 0) {
	        	if (((String)value).equals(""))
	        		return;
	        }
	        else if(col == 1 || col == 2) {
	        	if (!RegistrationValidator.isValidDateTimeString((String)value)) {
	        		JOptionPane.showMessageDialog(null, "Neispravan Unos!", "Greška", JOptionPane.ERROR_MESSAGE);
	        		return;
	        	}
	        }
	        else if (col == 3) {
	        	if (!RegistrationValidator.isValidDouble((Double)value) || (Double)value < 1) {
	        		JOptionPane.showMessageDialog(null, "Neispravan Unos!", "Greška", JOptionPane.ERROR_MESSAGE);
	        		return;
	        	}
	        }
	        else if (col == 4) {
	        	if (((String)value).equals("")) {
	        		return;
	        	}
	        }
	    	data[row][col]=value;
	        fireTableCellUpdated(row, col);
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return true;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}
}