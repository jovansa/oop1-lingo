package gui.predavac;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import entities.managers.DataHandler;
import entities.other.Kurs;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import gui.customcomponents.MultiLineTableCellRenderer;

@SuppressWarnings("serial")
public class PredavacPregledPanel extends JPanel implements ActionListener {

	private DataHandler dh;
	
	CustomButton povratakButton;
	
	CustomTableFixed table;
	
	public PredavacPregledPanel(DataHandler dh) {
		this.dh = dh;
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Pregled Kurseva");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		titleLabel.setBounds(378, 15, 220, 52);
		add(titleLabel);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(282, 407, 405, 64);
		povratakButton.addActionListener(this);
		add(povratakButton);
		
		table = new CustomTableFixed();
		table.setModel(new MyTableModel());
		
		MultiLineTableCellRenderer renderer = new MultiLineTableCellRenderer();
		table.getColumnModel().getColumn(4).setCellRenderer(renderer);
		table.getColumnModel().getColumn(5).setCellRenderer(renderer);
		MultiLineTableCellRenderer.resizeColumnHeight(table);
		MultiLineTableCellRenderer.resizeColumnWidth(table);
		
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 65, 954, 331);
		scrollPane.getViewport().setBackground(new Color(255, 209, 209));
		this.add(scrollPane);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			PredavacLoggedFrame topFrame = (PredavacLoggedFrame) SwingUtilities.getWindowAncestor(PredavacPregledPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	}

	public Object[][] getTableContent(){
		int numOfRow = 0;
		Object [][] retVal;
		
		ArrayList<Kurs> filtriraniKursevi = new ArrayList<Kurs>();
		for(Kurs k: dh.getKursevi()) {
			if(k.getPredavaci().contains(dh.getCurrentlyLogged()))
				filtriraniKursevi.add(k);
		}
		retVal = new Object[filtriraniKursevi.size()][6];
		Collections.reverse(filtriraniKursevi);
		for(Kurs k: filtriraniKursevi) {
			retVal[numOfRow][0] = k.getId();
			retVal[numOfRow][1] = k.getNaziv();
			retVal[numOfRow][2] = k.getPocetak().format(DateTimeFormatter.ofPattern("dd.MM.yyyy."));
			retVal[numOfRow][3] = k.getKraj().format(DateTimeFormatter.ofPattern("dd.MM.yyyy."));
			
			String[] predavaci = new String[k.getPredavaci().size()];
			for(int i = 0; i < k.getPredavaci().size(); i++)
				predavaci[i] = k.getPredavaci().get(i).getIme() + " " + k.getPredavaci().get(i).getPrezime();
		
			String[] polaznici = new String[k.getPolaznici().size()];
			for(int i = 0; i < k.getPolaznici().size(); i++)
				polaznici[i] = k.getPolaznici().get(i).getIme() + " " + k.getPolaznici().get(i).getPrezime();
			
			retVal[numOfRow][4] = predavaci;
			retVal[numOfRow][5] = polaznici;
			numOfRow++;
		}
		
		return retVal;
		
		
	}
	
	class MyTableModel extends AbstractTableModel {
	    private String[] columnNames = {"ID Kursa", "Naziv", "Datum Početka", "Datum Završetka", "Predavači", "Polaznici"};
	    private Object[][] data = PredavacPregledPanel.this.getTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}
}
