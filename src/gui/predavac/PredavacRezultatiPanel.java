package gui.predavac;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.knowm.xchart.PieChart;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.demo.charts.ExampleChart;
import org.knowm.xchart.style.PieStyler.AnnotationType;

import entities.managers.DataHandler;
import entities.other.ResenjeTesta;
import entities.other.Test;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import gui.customcomponents.MultiLineTableCellRenderer;
import gui.customcomponents.PieChartSample;
import utilities.ReportTextGenerator;


@SuppressWarnings("serial")
public class PredavacRezultatiPanel extends JPanel implements ActionListener  {
	
	private DataHandler dh;
	ArrayList<Test> testovi;
	Test odabranTest;
	
	CustomTableFixed table;
	JLabel errorLabel;
	@SuppressWarnings("rawtypes")
	XChartPanel chartPanel;
	
	CustomButton povratakButton;
	CustomButton printButton;
	
	private JLabel lblNa;
	
	public PredavacRezultatiPanel(DataHandler dh) {
		this.dh = dh;
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Pregled rezultata");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		titleLabel.setBounds(370, 15, 234, 52);
		add(titleLabel);
		
		table = new CustomTableFixed();
		table.setModel(new MyTableModel());
		table.setRowSelectionAllowed(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		MultiLineTableCellRenderer.resizeColumnWidth(table);
		MultiLineTableCellRenderer.resizeColumnHeight(table);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        @SuppressWarnings({ "rawtypes", "unchecked" })
			public void valueChanged(ListSelectionEvent event) {
	        	 	odabranTest = dh.getTestFromId((String) table.getValueAt(table.getSelectedRow(), 0));
	        	 	errorLabel.setText("");
	        	 	List<java.awt.Component> componentList = Arrays.asList(PredavacRezultatiPanel.this.getComponents());
	        	 	if (componentList.contains(chartPanel)) {
	        	 		PredavacRezultatiPanel.this.remove(chartPanel);
	        	 	}
	        	 	try {
	        			ExampleChart<PieChart> exampleChart = new PieChartSample(400, 300, odabranTest.getNaziv(),getChartData(odabranTest));
	        			PieChart chart = exampleChart.getChart();
		        	    chart.getStyler().setChartBackgroundColor(new Color(255, 168, 168));
		        	    chart.getStyler().setAnnotationType(AnnotationType.Value);
		        	    chartPanel = new XChartPanel(chart);
		        		chartPanel.setBounds(520, 132, 420, 271);
		        		chartPanel.setVisible(true);
		        		chartPanel.setBackground(new Color(255, 168, 168));
		        		chartPanel.revalidate();
		        		chartPanel.repaint();
		        		PredavacRezultatiPanel.this.add(chartPanel);
		        		PredavacRezultatiPanel.this.revalidate();
		        		PredavacRezultatiPanel.this.repaint();
		        	    if (getChartData(odabranTest).size() < 1) repaintChartPanel();
	        	 	} catch(Exception e) {
		        	 	repaintChartPanel();
	               }
	        }
	    });
		
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(34, 132, 420, 271);
		scrollPane.getViewport().setBackground(new Color(255, 209, 209));
		add(scrollPane);
		
		errorLabel = new JLabel("");
		errorLabel.setForeground(Color.RED);
		errorLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		errorLabel.setBounds(510, 132, 442, 271);
		add(errorLabel);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(253, 425, 201, 52);
		povratakButton.addActionListener(this);
		add(povratakButton);
		
		lblNa = new JLabel("Na grafiku će biti prikazan broj bodova za svakog učenika");
		lblNa.setForeground(Color.DARK_GRAY);
		lblNa.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		lblNa.setBounds(521, 100, 420, 32);
		add(lblNa);
		
		printButton = new CustomButton("Povratak");
		printButton.setText("Štampaj");
		printButton.setBounds(34, 425, 201, 52);
		printButton.addActionListener(this);
		add(printButton);
	}
	
	public void repaintChartPanel() {
		List<java.awt.Component> componentList = Arrays.asList(PredavacRezultatiPanel.this.getComponents());
		if (componentList.contains(chartPanel)) {
	 		PredavacRezultatiPanel.this.remove(chartPanel);
    		PredavacRezultatiPanel.this.revalidate();
    		PredavacRezultatiPanel.this.repaint();
	 	}
	   errorLabel.setText("<html><center>Nema rezultata za odabrani test!</center></html>");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			PredavacLoggedFrame topFrame = (PredavacLoggedFrame) SwingUtilities.getWindowAncestor(PredavacRezultatiPanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	
		else if (e.getSource() == printButton) {
			PDDocument doc = new PDDocument();
			
			PDPage page = new PDPage();
			doc.addPage(page);
			
			try {
				PDPageContentStream contentStream = new PDPageContentStream(doc, page);
				contentStream.beginText();
				contentStream.setFont(PDType1Font.HELVETICA, 16);
				contentStream.setLeading(14.5f);
				contentStream.newLineAtOffset(25, 725); 
				for(String line: ReportTextGenerator.getPredavacTestReport(odabranTest)) {
					contentStream.showText(line);
					contentStream.newLine();
					contentStream.newLine();
				}
			    contentStream.endText();
			    contentStream.close();
				
			    String filename = "report\\" + odabranTest.getId() + " " + odabranTest.getNaziv();
			    File file = new File(filename + ".pdf");
				doc.save(file);
				doc.close();
			
				PrinterJob pj = PrinterJob.getPrinterJob();
			    if (pj.printDialog()) {
			        try {pj.print();}
			        catch (PrinterException exc) {
			            JOptionPane.showMessageDialog(null, "Štampanje nije uspelo!", "Greška", JOptionPane.ERROR_MESSAGE);
			         }
			     } 
			    
			    JOptionPane.showMessageDialog(null, "Izveštaj sačuvan!", "Uspeh", JOptionPane.INFORMATION_MESSAGE);			    
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch(NullPointerException e2) {
				JOptionPane.showMessageDialog(null, "Niste odabrali test!", "Greška", JOptionPane.ERROR_MESSAGE);
			}
			
		}
	}
	
	public HashMap<String, Double> getChartData(Test t){
		HashMap<String, Double> retVal = new HashMap<String, Double>();
		for(ResenjeTesta rt : t.getResenja())
			if (rt.isPregledan())
				retVal.put(rt.getUcenik().getIme() + " " + rt.getUcenik().getPrezime(), rt.getBrojBodova());
		return retVal;
	}
	
	public Object[][] getTableContent(){
		int numOfRow = 0;
		Object [][] retVal;
		
		ArrayList<Test> filtriraniTestovi = new ArrayList<Test>();

		for(Test t: dh.getTestovi()) {
			if (t.getAutor().equals(dh.getCurrentlyLogged()))
				filtriraniTestovi.add(t);
		}
		retVal = new Object[filtriraniTestovi.size()][2];
		this.testovi = filtriraniTestovi;
		
		for(Test t: filtriraniTestovi) {
			retVal[numOfRow][0] = t.getId();
			String start = t.getAktivanOd().format(DateTimeFormatter.ofPattern("dd.MM.yyyy."));
			String end = t.getAktivanOd().format(DateTimeFormatter.ofPattern("dd.MM.yyyy."));
			String s = "<html>" + t.getNaziv() + "<br />" +start + " - " + end + "</html>";
			retVal[numOfRow][1] = s;
			numOfRow++;
		}
		
		return retVal;
		
		
	}
	
	class MyTableModel extends AbstractTableModel {
	    private String[] columnNames = {"ID Testa", "Naziv"};
	    private Object[][] data = PredavacRezultatiPanel.this.getTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}	
}
