package gui.predavac;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import entities.managers.DataHandler;
import gui.customcomponents.CustomButton;
import gui.customcomponents.LoggedFrame;
import gui.izvestaji.UceniciPoJeziku;
import gui.izvestaji.UceniciPoStarosti;


@SuppressWarnings("serial")
public class PredavacLoggedFrame extends LoggedFrame implements ActionListener{
	
	JPanel buttonPanel;
	
	CustomButton pregledButton;
	CustomButton kreiranjeButton;
	CustomButton ocenjivanjeButton;
	CustomButton rezultatiButton;
	CustomButton izmenaButton;
	
	JMenu chartMenu;
	JMenuItem statistikaStarost;
	JMenuItem statistikaJezik;
	
	public PredavacLoggedFrame(DataHandler data) {
		super(data);	
		
		chartMenu = new JMenu("Statistika učenika");
		this.menuBar.add(chartMenu);
		
		statistikaStarost = new JMenuItem("Statistika po starosti");
		statistikaStarost.setIcon(barChartIcon);
		statistikaStarost.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
    			Thread t = new Thread(new Runnable() {
    				@Override
    				public void run() {
    	            	UceniciPoStarosti izv = new UceniciPoStarosti(PredavacLoggedFrame.this.getData());
    	    			izv.uceniciIzvestaj(PredavacLoggedFrame.this.getData());
    				}
    			});
    			t.start();
            }
            }));
		chartMenu.add(statistikaStarost);
		
		statistikaJezik = new JMenuItem("Statistika po jezicima");
		statistikaJezik.setIcon(pieChartIcon);
		statistikaJezik.addActionListener((new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
    			Thread t = new Thread(new Runnable() {
    				@Override
    				public void run() {
    	            	UceniciPoJeziku izv = new UceniciPoJeziku(PredavacLoggedFrame.this.getData());
    	    			izv.uceniciIzvestaj(PredavacLoggedFrame.this.getData(), true);
    				}
    			});
    			t.start();
            }
            }));
		chartMenu.add(statistikaJezik);
		
		buttonPanel = new JPanel();
		buttonPanel.setBounds(10, 527, 974, 112);
		buttonPanel.setLayout(null);
		buttonPanel.setBackground(new Color(255, 168, 168));
		getContentPane().add(buttonPanel);
		
		pregledButton = new CustomButton("Pregled kurseva");
		pregledButton.setBounds(15, 25, 183, 63);
		pregledButton.addActionListener(this);
		buttonPanel.add(pregledButton);
	
		kreiranjeButton = new CustomButton("Kreiranje testa");
		kreiranjeButton.setBounds(204, 25, 183, 63);
		kreiranjeButton.addActionListener(this);
		buttonPanel.add(kreiranjeButton);
		
		ocenjivanjeButton = new CustomButton("Ocenjivanje testa");
		ocenjivanjeButton.setBounds(393, 25, 183, 63);
		ocenjivanjeButton.addActionListener(this);
		buttonPanel.add(ocenjivanjeButton);
		
		rezultatiButton = new CustomButton("Pregled Rezultata");
		rezultatiButton.setBounds(771, 25, 183, 63);
		rezultatiButton.addActionListener(this);
		buttonPanel.add(rezultatiButton);
		
		izmenaButton = new CustomButton("Izmena Testa");
		izmenaButton.setBounds(582, 25, 183, 63);
		izmenaButton.addActionListener(this);
		buttonPanel.add(izmenaButton);
		
		buttonPanel.setVisible(true);
		buttonPanel.revalidate();
		buttonPanel.repaint();
		this.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == pregledButton) {
			PredavacLoggedFrame.this.makeButtonsVisible(false);
			PredavacLoggedFrame.this.getContentPane().add(new PredavacPregledPanel(this.getData()));
			PredavacLoggedFrame.this.setLabelVisibility(false);
			PredavacLoggedFrame.this.revalidate();
			PredavacLoggedFrame.this.repaint();
		}
		
		else if (e.getSource() == kreiranjeButton) {
			PredavacLoggedFrame.this.makeButtonsVisible(false);
			PredavacLoggedFrame.this.getContentPane().add(new PredavacCreatePanel(this.getData()));
			PredavacLoggedFrame.this.setLabelVisibility(false);
			PredavacLoggedFrame.this.revalidate();
			PredavacLoggedFrame.this.repaint();
		}
	
		else if (e.getSource() == ocenjivanjeButton) {
			if (this.getData().getBrojNeocenjenih() == 0) {
				JOptionPane.showMessageDialog(null, "Nemate neocenjenih testova!", "Obaveštenje", JOptionPane.WARNING_MESSAGE);
				return;
			}
			PredavacLoggedFrame.this.makeButtonsVisible(false);
			PredavacLoggedFrame.this.getContentPane().add(new PredavacOcenjivanjePanel(this.getData()));
			PredavacLoggedFrame.this.setLabelVisibility(false);
			PredavacLoggedFrame.this.revalidate();
			PredavacLoggedFrame.this.repaint();
		}
	
		else if(e.getSource() == rezultatiButton) {
			PredavacLoggedFrame.this.makeButtonsVisible(false);
			PredavacLoggedFrame.this.getContentPane().add(new PredavacRezultatiPanel(this.getData()));
			PredavacLoggedFrame.this.setLabelVisibility(false);
			PredavacLoggedFrame.this.revalidate();
			PredavacLoggedFrame.this.repaint();
		}
	
		else if (e.getSource() == izmenaButton) {
			if (this.getData().getBrojEditabilnih() == 0) {
				JOptionPane.showMessageDialog(null, "Nemate testova podobnih za izmenu!", "Obaveštenje", JOptionPane.WARNING_MESSAGE);
				return;
			}
			PredavacLoggedFrame.this.makeButtonsVisible(false);
			PredavacLoggedFrame.this.getContentPane().add(new PredavacEditPanel(this.getData()));
			PredavacLoggedFrame.this.logoLabel.setVisible(false);
			PredavacLoggedFrame.this.revalidate();
			PredavacLoggedFrame.this.repaint();
		}
	}
		
	public void makeButtonsVisible(boolean state) {
		PredavacLoggedFrame.this.ocenjivanjeButton.setVisible(state);
		PredavacLoggedFrame.this.pregledButton.setVisible(state);
		PredavacLoggedFrame.this.rezultatiButton.setVisible(state);	
		PredavacLoggedFrame.this.kreiranjeButton.setVisible(state);
		PredavacLoggedFrame.this.izmenaButton.setVisible(state);
	}
}
