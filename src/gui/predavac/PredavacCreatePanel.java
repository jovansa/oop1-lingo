package gui.predavac;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.other.*;
import entities.users.Predavac;
import gui.customcomponents.CustomButton;
import utilities.RegistrationValidator;

@SuppressWarnings("serial")
public class PredavacCreatePanel extends JPanel implements ActionListener {
	DataHandler dh;
	private JTextField nazivField;
	private JTextField startField;
	private JTextField endField;
	private JTextField maxPointField;
	private JTextField pitanjeField;
	
	private CustomButton dodajButton;
	private CustomButton kreirajButton;
	private CustomButton povratakButton;
	
	ArrayList<String> pitanja;
	JLabel errorLabel;
	private JLabel lblDdmmyyyyHhmm;
	private JLabel label;
	
	
	public PredavacCreatePanel(DataHandler dh) {
		this.dh = dh;
		this.pitanja = new ArrayList<String>(0);
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Kreiranje testa");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		titleLabel.setBounds(389, 15, 197, 52);
		add(titleLabel);
		
		JLabel nazivLabel = new JLabel("Unesite naziv testa:");
		nazivLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		nazivLabel.setBounds(216, 115, 212, 52);
		add(nazivLabel);
		
		JLabel startLabel = new JLabel("Unesite datum  i vreme početka:");
		startLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		startLabel.setBounds(73, 167, 355, 52);
		add(startLabel);
		
		JLabel endLabel = new JLabel("Unesite datum  i vreme kraja:");
		endLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		endLabel.setBounds(106, 221, 322, 52);
		add(endLabel);
		
		JLabel maxPointLabel = new JLabel("Unesite maksimalan broj bodova:");
		maxPointLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		maxPointLabel.setBounds(65, 275, 363, 52);
		add(maxPointLabel);
		
		JLabel pitanjeLabel = new JLabel("Unesite pitanje (jedno po jedno):");
		pitanjeLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 24));
		pitanjeLabel.setBounds(70, 329, 358, 52);
		add(pitanjeLabel);
		
		nazivField = new JTextField();
		nazivField.setBounds(438, 129, 218, 37);
		add(nazivField);
		nazivField.setColumns(10);
		
		startField = new JTextField();
		startField.setColumns(10);
		startField.setBounds(438, 181, 218, 37);
		add(startField);
		
		endField = new JTextField();
		endField.setColumns(10);
		endField.setBounds(438, 235, 218, 37);
		add(endField);
		
		maxPointField = new JTextField();
		maxPointField.setColumns(10);
		maxPointField.setBounds(438, 289, 218, 37);
		add(maxPointField);
		
		pitanjeField = new JTextField();
		pitanjeField.setColumns(10);
		pitanjeField.setBounds(438, 343, 218, 37);
		add(pitanjeField);
		
		dodajButton = new CustomButton("Dodaj pitanje");
		dodajButton.setBounds(679, 343, 162, 37);
		dodajButton.setFocusable(false);
		dodajButton.addActionListener(this);
		add(dodajButton);
		
		errorLabel = new JLabel("");
		errorLabel.setForeground(Color.RED);
		errorLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 16));
		errorLabel.setBounds(679, 275, 285, 52);
		add(errorLabel);
		
		kreirajButton = new CustomButton("Kreiraj Test");
		kreirajButton.setFocusable(false);
		kreirajButton.setBounds(285, 432, 162, 37);
		kreirajButton.addActionListener(this);
		add(kreirajButton);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setFocusable(false);
		povratakButton.addActionListener(this);
		povratakButton.setBounds(527, 432, 162, 37);
		add(povratakButton);
		
		lblDdmmyyyyHhmm = new JLabel("(dd.mm.yyyy. hh:mm)");
		lblDdmmyyyyHhmm.setFont(new Font("Segoe UI Semibold", Font.ITALIC, 20));
		lblDdmmyyyyHhmm.setBounds(673, 167, 203, 52);
		add(lblDdmmyyyyHhmm);
		
		label = new JLabel("(dd.mm.yyyy. hh:mm)");
		label.setFont(new Font("Segoe UI Semibold", Font.ITALIC, 20));
		label.setBounds(673, 221, 203, 52);
		add(label);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == dodajButton){
			if(pitanjeField.getText().length() > 5) {
				pitanja.add(pitanjeField.getText());
				pitanjeField.setText("");
				JOptionPane.showMessageDialog(null, "Pitanje dodato! Ukupan broj pitanja do sad: " + pitanja.size() , "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				JOptionPane.showMessageDialog(null, "Pitanje se mora sastojati od bar 6 karaktera!", "Greška", JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
		else if (e.getSource() == povratakButton) {
			PredavacLoggedFrame topFrame = (PredavacLoggedFrame) SwingUtilities.getWindowAncestor(PredavacCreatePanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
		else if (e.getSource() == kreirajButton) {
			if (nazivField.getText().equals("")) {
				errorLabel.setText("Niste uneli naziv testa!");
				return;
			}
			String nazivTesta = nazivField.getText();
			
			if (!RegistrationValidator.isValidDateTimeString(startField.getText())){
				errorLabel.setText("Niste uneli ispravan datum početka!");
				return;
			}
			else if (!RegistrationValidator.isValidDateTimeString(endField.getText())){
				errorLabel.setText("Niste uneli ispravan datum kraja!");
				return;
			}
			LocalDateTime start = LocalDateTime.parse(startField.getText(), DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm"));
			LocalDateTime end = LocalDateTime.parse(endField.getText(), DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm"));
			
			if(start.isBefore(LocalDateTime.now()) || end.isBefore(start)) {
				errorLabel.setText("Niste uneli ispravne datume!");
				return;
			}
			
			double maxPoint;
			try {
				maxPoint = Double.parseDouble(maxPointField.getText());
			}catch(Exception ee) {
				errorLabel.setText("<html><center>Neispravan unos maksimalnog broja bodova!</center><html>");
				return;
			}
			
			if (maxPoint < 0) {
				errorLabel.setText("<html><center>Maksimalan broj bodova mora biti veći od 0!</center></html>");
				return;
			}
			
			if (this.pitanja.size() <= 0) {
				errorLabel.setText("Morate uneti barem jedno pitanje!");
				return;
			}
			
			Test test = new Test(dh.generateTestId(), nazivTesta, start, end,
					(Predavac) dh.getCurrentlyLogged(), pitanja,  maxPoint, new ArrayList<ResenjeTesta>());
		
			dh.addTest(test);
			dh.writeEntityToFile(test, DataLoad.testoviPath);
			JOptionPane.showMessageDialog(null, "Test Kreiran!", "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			PredavacLoggedFrame topFrame = (PredavacLoggedFrame) SwingUtilities.getWindowAncestor(PredavacCreatePanel.this);
			topFrame.getLogoLabel().setVisible(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
		
	}
}
