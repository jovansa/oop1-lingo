package gui.predavac;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import entities.managers.DataHandler;
import entities.managers.DataLoad;
import entities.other.ResenjeTesta;
import entities.other.Test;
import gui.customcomponents.CustomButton;
import gui.customcomponents.CustomTableFixed;
import gui.customcomponents.MultiLineTableCellRenderer;

@SuppressWarnings("serial")
public class PredavacOcenjivanjePanel extends JPanel implements ActionListener{

	DataHandler dh;
	private JTextField bodoviField;
	
	CustomButton oceniButton;
	CustomButton povratakButton;
	
	JScrollPane testoviPanel;
	JScrollPane resenjaPanel;
	
	CustomTableFixed testoviTable;
	CustomTableFixed resenjaTable;
	
	Test selectedTest;
	ResenjeTesta selectedResenje;
	
	JLabel warningLabel;
	
	@SuppressWarnings("rawtypes")
	JComboBox oceneBox;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public PredavacOcenjivanjePanel(DataHandler dh) {
		this.dh = dh;
		
		this.setBackground(new Color(255, 168, 168));
		this.setLayout(null);
		this.setBounds(10, 10, 974, 505);
		
		JLabel titleLabel = new JLabel("Ocenjivanje testova");
		titleLabel.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		titleLabel.setBounds(353, 15, 268, 52);
		add(titleLabel);
		
		bodoviField = new JTextField();
		bodoviField.setBounds(240, 376, 219, 27);
		add(bodoviField);
		bodoviField.setColumns(10);
		
		String[] options = {"1", "2", "3", "4", "5"};
		
		oceneBox = new JComboBox(options);
		oceneBox.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		oceneBox.setBounds(240, 414, 219, 27);
		oceneBox.setAlignmentX(CENTER_ALIGNMENT);
		oceneBox.setAlignmentY(CENTER_ALIGNMENT);
		((JLabel)oceneBox.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
		add(oceneBox);
		
		JLabel bodoviLabel = new JLabel("Unesite broj bodova:");
		bodoviLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		bodoviLabel.setBounds(56, 382, 174, 21);
		add(bodoviLabel);
		
		JLabel ocenaLabel = new JLabel("Odaberite ocenu:");
		ocenaLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		ocenaLabel.setBounds(86, 420, 144, 21);
		add(ocenaLabel);
		
		oceniButton = new CustomButton("Oceni");
		oceniButton.setBounds(515, 374, 219, 77);
		oceniButton.addActionListener(this);
		add(oceniButton);
		
		povratakButton = new CustomButton("Povratak");
		povratakButton.setBounds(745, 374, 219, 77);
		povratakButton.addActionListener(this);
		add(povratakButton);
		
		warningLabel = new JLabel("<html><center>Ocena \"1\" znači da polaznik nije položio test</center></html>");
		warningLabel.setForeground(Color.DARK_GRAY);
		warningLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 14));
		warningLabel.setBounds(240, 442, 219, 52);
		add(warningLabel);
		
		testoviTable = new CustomTableFixed();
		testoviTable.setModel(new MyTestoviTableModel());
		testoviTable.setRowSelectionAllowed(true);
		testoviTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		testoviTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		MultiLineTableCellRenderer.resizeColumnWidth(testoviTable);
		MultiLineTableCellRenderer.resizeColumnHeight(testoviTable);
		testoviTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	selectedTest = dh.getTestFromId((String) testoviTable.getValueAt(testoviTable.getSelectedRow(), 0));
	        	selectedResenje = dh.getResenjeFromId((String) testoviTable.getValueAt(testoviTable.getSelectedRow(), 1));
	    		resenjaTable.setModel(new MyResenjaTableModel());
	    		MultiLineTableCellRenderer.resizeColumnWidth(testoviTable);
	    		MultiLineTableCellRenderer.resizeColumnHeight(testoviTable);
	        	resenjaPanel.revalidate();
	        	resenjaPanel.repaint();
	        	PredavacOcenjivanjePanel.this.revalidate();
	        	PredavacOcenjivanjePanel.this.repaint();
	        }
	    });
		

		resenjaTable = new CustomTableFixed();
		
		testoviPanel = new JScrollPane(testoviTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		testoviPanel.getViewport().setBackground(new Color(255, 209, 209));
		testoviPanel.setBounds(10, 96, 449, 267);
		
		add(testoviPanel);
		
		resenjaPanel = new JScrollPane(resenjaTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		resenjaPanel.setBounds(515, 96, 449, 267);
		resenjaPanel.getViewport().setBackground(new Color(255, 209, 209));
		add(resenjaPanel);
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == povratakButton) {
			PredavacLoggedFrame topFrame = (PredavacLoggedFrame) SwingUtilities.getWindowAncestor(PredavacOcenjivanjePanel.this);
			topFrame.setLabelVisibility(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	
		else if (e.getSource() == oceniButton) {
			warningLabel.setText("");
			float brojBodova;
			try {
				brojBodova = Float.parseFloat(bodoviField.getText());
			}
			catch (Exception ee) {
				warningLabel.setForeground(Color.RED);
				warningLabel.setText("Neispravan Unos!");
				return;
			}
			if(brojBodova > selectedTest.getMaxBrojbodova()) {
				warningLabel.setForeground(Color.RED);
				warningLabel.setText("<html><center>Maksimalan broj bodova za odabrani test je: " + selectedTest.getMaxBrojbodova() + "</center></html>");
				return;
			}
			String ocena = (String) oceneBox.getSelectedItem();
			
			selectedResenje.setOcena(Integer.parseInt(ocena));
			selectedResenje.setBrojBodova(brojBodova);
			selectedResenje.setPregledan(true);
			
			if (ocena.equals("1"))
				selectedResenje.setPolozen(false);
			else
				selectedResenje.setPolozen(true);
		
			dh.writeResenjaToFile(DataLoad.resenjaPath);
			JOptionPane.showMessageDialog(null, "Test Ocenjen!", "Uspeh", JOptionPane.INFORMATION_MESSAGE);
			PredavacLoggedFrame topFrame = (PredavacLoggedFrame) SwingUtilities.getWindowAncestor(PredavacOcenjivanjePanel.this);
			topFrame.getLogoLabel().setVisible(true);
			topFrame.makeButtonsVisible(true);
			this.setVisible(false);
		}
	}
	
	public Object[][] getTestoviTableContent(){
		int numOfRow = 0;
		Object[][] retVal;
		
		ArrayList<Test> testovi = new ArrayList<Test>();
		HashMap<ResenjeTesta, Test> resenja = new HashMap<ResenjeTesta, Test>();
		
		for (Test t : dh.getTestovi()) {
			if (t.getAutor().equals(dh.getCurrentlyLogged())) {
				testovi.add(t);
				if(t.getResenja().size() > 0) {
					for(ResenjeTesta rt: t.getResenja()) {
						if (!rt.isPregledan())
							resenja.put(rt, t);
					}
				}
			}	
		}
		retVal = new Object[resenja.size()][4];
		for (Map.Entry<ResenjeTesta, Test> entry : resenja.entrySet()) {
			retVal[numOfRow][0] = entry.getValue().getId();
			retVal[numOfRow][2] = entry.getValue().getNaziv();
			retVal[numOfRow][3] = entry.getKey().getUcenik().getIme() + " " + entry.getKey().getUcenik().getPrezime();
			retVal[numOfRow][1] = entry.getKey().getId();
			numOfRow++;
		}
		return retVal;
	}
	
	public Object[][] getResenjaTableContent(){
		int numOfRow = 0;
		Object[][] retVal = new Object[selectedTest.getPitanja().size()][2];
		
		for(String pitanje : selectedTest.getPitanja()) {
			retVal[numOfRow][0] = pitanje;
			retVal[numOfRow][1] = selectedResenje.getOdgovori().get(numOfRow);
			numOfRow++;
		}
		return retVal;
		
	}
	
	class MyTestoviTableModel extends AbstractTableModel {
	    private String[] columnNames = {"ID Testa","ID Rešenja" ,"Naziv Testa", "Polaznik"};
	    private Object[][] data = PredavacOcenjivanjePanel.this.getTestoviTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	    
	    
	}
	
	class MyResenjaTableModel extends AbstractTableModel {
	    private String[] columnNames = {"Pitanje", "Odgovor"};
	    private Object[][] data = PredavacOcenjivanjePanel.this.getResenjaTableContent();

	    public int getColumnCount() {
	        return columnNames.length;
	    }

	    public int getRowCount() {
	        return data.length;
	    }

	    public String getColumnName(int col) {
	        return columnNames[col];
	    }

	    public Object getValueAt(int row, int col) {
	        return data[row][col];
	    }
	    
	    public boolean isCellEditable(int rowIndex, int mColIndex) {
	        return false;
	    }
	    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public Class getColumnClass(int c) {
	        return getValueAt(0, c).getClass();
	    }
	}
}
