package utilities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.regex.Pattern;

public class RegistrationValidator {

	public static boolean isValidUsername(String user) {
		if(user.matches("^[a-zA-Z]+[\\w]{5,20}$"))
			return true;
		return false;
	}

	public static boolean isValidName(String name) {
		if (Pattern.matches("[a-zA-Z]+",name))
			return true;
		return false;
	}	

	public static boolean isValidPassword(String password) {
		if (password.length() > 5)
			return true;
		return false;
	}

	public static boolean isValidDateString(String date) {
		try {
			@SuppressWarnings("unused")
			LocalDate datumRodjenja = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.yyyy."));
		}
		catch (Exception err){
			return false;
		}
		return true;
	}
	
	public static boolean isValidDateTimeString(String dateTime) {
		try {
			@SuppressWarnings("unused")
			LocalDateTime datumRodjenja = LocalDateTime.parse(dateTime, DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm"));
		}
		catch (DateTimeParseException err){
			return false;
		}
		return true;
	}

	public static boolean isValidTelefon(String telefon) {
		if (telefon.matches("[+0-9]+")) 
			return true;
		return false;
	}
	
	public static boolean isValidDouble(Double doub) {
		try {
			Double.parseDouble(doub.toString());
			return true;
		}catch(Exception e) {
			return false;
		}
	}
}
