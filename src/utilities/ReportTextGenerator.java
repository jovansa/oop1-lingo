package utilities;

import java.util.ArrayList;

import entities.other.ResenjeTesta;
import entities.other.Test;

public class ReportTextGenerator {

	public static ArrayList<String> getPredavacTestReport(Test t){
		
		ArrayList<String> retVal = new ArrayList<String>();
		
		retVal.add("Izvestaj za test: " + t.getId() + " " + t.getNaziv());
		retVal.add("Autor testa: " + t.getAutor().getIme() + " " + t.getAutor().getPrezime());
		retVal.add("Ukupan broj resenja: " + t.getResenja().size());
		retVal.add("Maksimalan broj bodova: " + t.getMaxBrojbodova());
		
		double avg = .0;
		double ocene = 0;
		for (ResenjeTesta rt : t.getResenja()) {
			avg += rt.getBrojBodova();
			ocene += rt.getOcena();
		}
		retVal.add("Prosecno ostvaren broj bodova: " + Double.toString(avg / t.getResenja().size()));
		retVal.add("Prosecna ocena: " + String.format("%1.2f",ocene / t.getResenja().size()));
		retVal.add("Ucinak po polazniku: ");
		
		for (ResenjeTesta rt : t.getResenja()) {
			retVal.add("Polaznik: " + rt.getUcenik().getIme() + " " + rt.getUcenik().getPrezime() + ", broj bodova: "
						+ rt.getBrojBodova() + ", " + rt.getOcena());
		}
		return retVal;
	}
}
